package com.mlf.syn;

/**
 * @program: Java基础
 * @description: 卖票
 * @author: mlf
 * @date: 2022-07-18 16:51
 **/
// 资源类
class Ticket {
    private int ticketNumber = 30;

    public synchronized void sale() {
        if (ticketNumber > 0) {
            System.out.println(Thread.currentThread().getName() + " : 卖出了编号" + (ticketNumber--) + "，剩" + ticketNumber);
        }
    }
}

// 售票
public class SaleTicket {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();

        new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                ticket.sale();
            }
        }, "AAA").start();

        new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                ticket.sale();
            }
        }, "BBB").start();

        new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                ticket.sale();
            }
        }, "CCC").start();
    }
}
