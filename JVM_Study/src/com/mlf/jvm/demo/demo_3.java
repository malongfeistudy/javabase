package com.mlf.jvm.demo;

import java.util.LinkedList;

/**
 * @program: Java基础
 * @description: OutOfMemoryError Java heap space  堆内存溢出
 * VM Options：-Xmx4m 设置堆内存命令
 * @author: mlf
 * @date: 2022-05-10 13:18
 **/
public class demo_3 {

    public static void main(String[] args) {

        int i = 0;
        try {
            LinkedList<String> linkedList = new LinkedList<>();
            String s = "test";

            while (true) {
                linkedList.add(s);
                s = s + s;
                i++;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(i);
        }
    }

}
