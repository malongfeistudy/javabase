package com.mlf.datastructure.heap;

/**
 * @program: Java基础
 * @description: 堆排序
 * @author: mlf
 * @date: 2022-05-21 18:26
 **/
public class HeapSort {

    // 判断索引i处值是否小于索引j处值
    private static boolean less(Comparable[] heap, int i, int j) {
        return heap[i].compareTo(heap[j]) < 0;
    }

    // 交换索引i和j处的元素
    private static void exch(Comparable[] heap, int i, int j) {
        Comparable temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }


    // 根据原数组source，构造出堆
    public static void createHeap(Comparable[] source, Comparable[] heap) {

        // 将原数组拷贝到无序堆数组中
        System.arraycopy(source, 0, heap, 1, source.length);

        // 使元素下沉 从一半处开始下沉（堆中root结点），往索引处扫描
        for (int i = (heap.length) / 2; i > 0; i--) {
            sink(heap, i, heap.length - 1);
        }
        // ！注意：此时的堆在数据结构中并不是真正的有序

    }

    // 对source数组中元素进行排序
    public static void sort(Comparable[] source) {
        // 构建一个中间变量堆
        Comparable[] heap = new Comparable[source.length + 1];
        // 创建一个堆 ！注意：此时的堆在数据结构中并不是真正的有序
        createHeap(source, heap);
        int N = heap.length - 1;
        // 当未排序（下沉）元素个数大于1时
        while (N != 1) {
            //交换索引1和N处的值
            exch(heap, 1, N);
            // 缩小下沉范围
            N--;
            // 对堆顶元素下沉
            sink(heap, 1, N - 1);
        }

        // 将排好序的堆数组拷贝给原数组
        System.arraycopy(heap, 1, source, 0, source.length);

    }

    // 元素下沉 对target处的元素做下沉 范围是0~range
    private static void sink(Comparable[] heap, int target, int range) {
        while (2 * target <= range) {
            // 找出较大的子结点
            int max;
            if (2 * target + 1 <= range) {  // 存在右子结点
                if (less(heap, 2 * target + 1, 2 * target)) max = 2 * target;
                else max = 2 * target + 1;
            } else max = 2 * target;

            //比较交换max 和 target索引处的元素
            if (less(heap, target, max)) exch(heap, target, max);
            else break;

            target = max;
        }
    }


}
