package com.mlf.test;

import com.mlf.datastructure.linklist.TwoWayLinkList;

public class TwoWayLinkListTest {
    public static void main(String[] args) {
        TwoWayLinkList<String> s1= new TwoWayLinkList<>();
        s1.insert("我是谁");
        s1.insert("我在哪");
        s1.insert("咋办呀");
        s1.insert("干什么");

        System.out.println("-----------isEmpty()----------");
        System.out.println(s1.isEmpty());

        System.out.println("----------length()-----------");
        System.out.println(s1.length());

        System.out.println("----------遍历-----------");
        for (int i=0;i<s1.length();i++){
            System.out.println(s1.get(i));
        }

        System.out.println("----------首尾-----------");
        System.out.println(s1.getFirst());
        System.out.println(s1.getLast());

        System.out.println("----------indexOf-----------");
        int indexOf = s1.indexOf("咋办呀");
        System.out.println(indexOf);//2

        System.out.println("----------insert(i)-----------");
        s1.insert(2,"找不到工作");
        System.out.println(s1.get(2));
        System.out.println(s1.get(3));

        System.out.println("----------remove(i)-----------");
        System.out.println(s1.length());
        String remove = s1.remove(s1.length() - 1);
        System.out.println(remove);
        System.out.println(s1.length());

        System.out.println("-----------clear()----------");
        s1.clear();
        System.out.println(s1.length());

        System.out.println("-----------isEmpty()----------");
        System.out.println(s1.isEmpty());


    }
}
