package com.mlf.tcp;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @program: Java基础
 * @description: 服务端程序
 * @author: mlf
 * @date: 2022-05-11 22:52
 **/

public class Server {
    public static void main(String[] args) {
        new TCPServer().listen();
    }
}

class TCPServer {
    private static final int PORT = 7788; //定义一个端口号

    public void listen() {
        OutputStream os = null;
        Socket client = null;

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);

            client = serverSocket.accept(); //接收数据,没有数据进入阻塞等待

            os = client.getOutputStream(); //获取客户端的输出流

            System.out.println("开始与客户端交互数据····");

            os.write(("欢迎你").getBytes(StandardCharsets.UTF_8));

            Thread.sleep(3000);  //模拟延迟

            System.out.println("结束与客户端交互数据····");

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}

