package com.mlf.thread.sleep;

import java.text.SimpleDateFormat;
import java.util.Date;

//倒计时
public class TestSleep2 {

    public static void main(String[] args) throws InterruptedException {
        //开始时间
        Date date = new Date(System.currentTimeMillis());
        //打印时间
        while (true){
            Thread.sleep(1000);
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(date));
            date = new Date(System.currentTimeMillis());
        }
    }

}
