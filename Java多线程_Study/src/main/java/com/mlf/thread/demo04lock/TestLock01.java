package com.mlf.thread.demo04lock;

import java.util.concurrent.locks.ReentrantLock;

public class TestLock01 {

    public static void main(String[] args) {
        SaleTicket saleTicket = new SaleTicket();
        new Thread(saleTicket,"李四").start();
        new Thread(saleTicket,"张三").start();
        new Thread(saleTicket,"王五").start();
    }
}

class SaleTicket implements Runnable{

    private static Integer num=20;
    private static boolean flag=true;

    //初始化锁
    ReentrantLock lock = new ReentrantLock();
    @Override
    public void run() {
        while (flag){
            buy();
        }
    }

    private void buy() {
        //加锁
        lock.lock();
        try {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (num<=0){
                flag=false;
                return;
            }
            // 业务代码
            System.out.println("恭喜"+Thread.currentThread().getName()+"卖出了第"+(num--)+"张票");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            //必须在finally中释放锁否则会造成线程死锁
            //因为不管是否发生异常 finally 都会执行，因此我们可以在 finally 代码块中执行关闭连接、关闭文件和释放线程的的操作。
            //finally 块中定义的代码，总是在 try 和任何 catch 块之后、方法完成之前运行。
            lock.unlock();
        }

    }

}