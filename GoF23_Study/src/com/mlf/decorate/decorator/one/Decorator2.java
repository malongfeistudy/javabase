package com.mlf.decorate.decorator.one;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 装饰大小杯
 * @author: mlf
 * @date: 2022-06-01 19:05
 **/
public abstract class Decorator2 implements Beverage {
    protected Beverage beverage;
}
