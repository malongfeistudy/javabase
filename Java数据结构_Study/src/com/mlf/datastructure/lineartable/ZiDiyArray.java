package com.mlf.datastructure.lineartable;

public class ZiDiyArray<T> {

    private final T[] array;

    private int N;

    public ZiDiyArray(int size) {
        this.array=(T[]) new Object[size];
        this.N=size;
    }

    public int length(){
        return N;
    }

    public void insert(T t){
        array[N++]=t;
    }

    public void insert(int index,T t){
        for (int i=N;i>index;i--){
            array[i]=array[i-1];
        }
        array[index]=t;
        this.N+=1;
    }

    public void remove(int index){
        for (int i=index;i<N-1;i++){
            array[i]=array[i+1];
        }
        this.N-=1;
    }

    public void clear(){
        for (int i=0;i<N;i++){
            this.array[i]=null;
        }
        this.N=0;
    }




}
