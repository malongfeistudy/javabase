package com.mlf.reflect.t3;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @program: Java基础
 * @description: Method类及其用法
 * @author: mlf
 * @date: 2022-05-25 19:47
 **/
public class MethodTest {

    public static void main(String[] args) throws Exception {

        Class<?> aClass = Class.forName("com.mlf.reflect.t3.Test");
        // 根据参数获取public的Method,包含继承自父类的方法
        Method test1 = aClass.getMethod("test1", int.class);
        System.out.println("Method" + test1);
        System.out.println("===========");
        //获取所有public的方法:包含超类和超接口等
        Method[] methods = aClass.getMethods();
        for (Method method : methods) {
            System.out.println("Method:" + method);
        }

        System.out.println("=========================================");
        //获取当前类的方法包含private,该方法无法获取继承自父类的method
        Method method1 = aClass.getDeclaredMethod("test2");
        System.out.println("method1::" + method1);
        System.out.println();
        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod);
        }

    }

    @org.junit.Test
    public void test() throws Exception {
        Class<?> aClass = Class.forName("com.mlf.reflect.t3.Test");

        // 确定由该对象表示的类或接口实现的接口
        System.out.println(Arrays.toString(aClass.getInterfaces()));

        Test test = (Test) aClass.newInstance();

        Method test1 = aClass.getMethod("test1", int.class);
        //invoke(Object obj,Object... args)第一个参数代表调用的对象，第二个参数传递的调用方法的参数。
        // 这样就完成了类方法的动态调用
        test1.invoke(test, 100);

        //对私有无参方法的操作
        Method test2 = aClass.getDeclaredMethod("test2");
        //修改私有方法的访问标识
        test2.setAccessible(true);  //不加会报 IllegalAccessException
        test2.invoke(test);
        System.out.println("=========");

        //对有返回值得方法操作
        Method test3 = aClass.getDeclaredMethod("test3", String.class);
        String s = (String) test3.invoke(test, "mlf");
        System.out.println(s);

    }


}

class Test1 {
    public void t1() {
    }
}

class Test extends Test1 implements Comparable<Integer> {
    public void test1(int i) {
        System.out.println("我是方法" + "test1() " + i);
    }

    private void test2() {
        System.out.println("我是方法" + "test2方法");
    }

    public String test3(String s) {
        System.out.println("我是方法" + "test3方法");
        return "test3" + s;
    }

    public void test4() {
        System.out.println("我是方法" + this.toString());
    }

    @Override
    public int compareTo(Integer o) {
        return 0;
    }
}