package com.mlf.thread.obj;

import com.mlf.thread.utils.Sleeper;
import lombok.extern.java.Log;

/**
 * @program: Java基础
 * @description: 测试sleep和wait
 * @author: mlf
 * @date: 2022-07-25 13:02
 **/
@Log
public class TestWaitAndSleep {

    private static final Object lock = new Object();  // 模拟锁对象

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {
            synchronized (lock) {
                log.info("t1获得锁");
                try {
                    Thread.sleep(20000);  // 不释放锁
                    //lock.wait();  // 释放锁
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1");
        t1.start();
        t1.setDaemon(true); // 这里不能设置为守护线程

        Sleeper.sleep(1);
        synchronized (lock) {
            log.info("main获得锁");
        }

    }

}
