package com.mlf.demo1;

/**
 * @program: Java基础
 * @description: 泛型上下限: 在使用泛型的时候，我们可以为传入的泛型类型实参进行上下边界的限制，
 * 如：类型实参只准传入某种类型的父类或某种类型的子类。
 * <?> 无限制通配符
 * <? extends E> extends 关键字声明了类型的上界，表示参数化的类型可能是所指定的类型，或者是此类型的子类
 * <? super E> super 关键字声明了类型的下界，表示参数化的类型可能是指定的类型，或者是此类型的父类
 * <p>
 * // 使用原则《Effictive Java》
 * // 为了获得最大限度的灵活性，要在表示 生产者或者消费者 的输入参数上使用通配符，使用的规则就是：生产者有上限、消费者有下限
 * 1. 如果参数化类型表示一个 T 的生产者，使用 < ? extends T>;
 * 2. 如果它表示一个 T 的消费者，就使用 < ? super T>；
 * 3. 如果既是生产又是消费，那使用通配符就没什么意义了，因为你需要的是精确的参数类型。
 * @author: mlf
 * @date: 2022-05-22 10:01
 **/
class Info1<T extends Number> {    // 此处泛型只能是数字类型
    private T var;        // 定义泛型变量

    public void setVar(T var) {
        this.var = var;
    }

    public T getVar() {
        return this.var;
    }

    public String toString() {    // 直接打印
        return this.var.toString();
    }
}

public class Demo_02 {
    public static void main(String args[]) {
        Info1<Integer> i1 = new Info1<Integer>();        // 声明Integer的泛型对象
        i1.setVar(123);
        System.out.println(i1.toString());
    }
}



