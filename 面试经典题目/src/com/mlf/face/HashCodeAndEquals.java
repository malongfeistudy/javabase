package com.mlf.face;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-26 21:08
 **/
public class HashCodeAndEquals {
    // hashcode ：经过hash函数计算得出来的：将实际物理内存地址转换为一个整数传入hash函数计算 此函数自变量不唯一
    // equals方法：比较两个对象的值内容是否一样
    // ==：比较两个对象的值内容和内存地址。
    // 1、 如果两个对象的equals相等，那么两个对象的hashcode一定相等
    // 2、 如果两个对象的HashCode相同，不代表两个对象一定相等 。
    public static void main(String[] args) {

        A a1 = new A();
        //A a2 = a1;
        A a2 = new A();

        System.out.println(a1.hashCode());
        System.out.println(a2.hashCode());
        System.out.println(a1 == a2);
        System.out.println("a1==a2: " + a1.equals(a2));

    }

}

class A {

    @Override
    public boolean equals(Object obj) {
        return obj instanceof A;
    }

}