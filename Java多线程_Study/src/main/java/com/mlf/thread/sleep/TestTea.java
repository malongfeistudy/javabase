package com.mlf.thread.sleep;

import lombok.extern.java.Log;

import static com.mlf.thread.utils.Sleeper.sleep;

/**
 * @program: Java基础
 * @description: 统筹问题
 * @author: mlf
 * @date: 2022-07-23 16:27
 **/

@Log
public class TestTea {

    public static void main(String[] args) {

        // 烧水
        Thread t1 = new Thread(() -> {
            log.info("洗水壶");
            sleep(1);
            log.info("烧开水");
            sleep(5);
        }, "张三");

        // 洗茶具
        Thread t2 = new Thread(() -> {
            log.info("洗茶壶");
            sleep(1);
            log.info("洗茶杯");
            sleep(2);
            log.info("拿茶叶");
            sleep(1);
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("泡茶");
        }, "张三");

        t1.start();
        t2.start();

    }

}


