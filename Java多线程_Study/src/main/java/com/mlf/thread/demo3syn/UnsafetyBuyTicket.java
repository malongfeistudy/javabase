package com.mlf.thread.demo3syn;

//模拟买票线程不安全案例
//由于有三个线程对象都去抢占同一个资源，导致内存不同步，三个人可能同时都拿到相同的资源
public class UnsafetyBuyTicket {

    //主线程
    public static void main(String[] args) {
        BuyTicket buyTicket = new BuyTicket();
        new Thread(buyTicket,"一号").start();
        new Thread(buyTicket,"二号").start();
        new Thread(buyTicket,"三号").start();
        //System.out.println("买票线程运行完毕！！！");
    }

    //买票类
    static class BuyTicket implements Runnable{

        //定义票总量
        private int ticketNums=3;
        //标志 外部停止方式
        boolean flag=true;

        //线程运行方法类
        @Override
        public void run() {
            while (flag){
                try {
                    buy();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //对买票进行添加限制条件
        //给方法体加上synchronize同步锁，当下次访问需等待本次结束被阻塞
        private synchronized void buy() throws InterruptedException {
            if (ticketNums<=0) {flag=false;return;}
            Thread.sleep(1000);
            //这里有可能因为不同的线程对象同时访问的话 内存不能同步 造成ticketNums<=0的情况
            System.out.println(Thread.currentThread().getName()+"买到了到数第"+ticketNums--+"张票");
        }
    }

}
