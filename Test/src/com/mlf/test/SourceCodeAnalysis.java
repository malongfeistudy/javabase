package com.mlf.test;

/**
 * @program: Java基础
 * @description: 源码分析
 * @author: mlf
 * @date: 2022-04-28 10:44
 **/
public class SourceCodeAnalysis {

    public static void main(String[] args) {

        Integer a = 11;
        Integer b = 2;

        //System.out.println(a.compareTo(b));

        Integer[] test = {1, 2, 3, 4};
        int N = 3;
        test[N--] = null;
        System.out.println(N);
        for (Integer integer : test) {
            System.out.println(integer);
        }


    }

    //public void test(BiConsumer<? super K, ? super V> action) {
    //    HashMap.Node<K,V>[] tab;
    //    if (action == null)
    //        throw new NullPointerException();
    //    if (size > 0 && (tab = table) != null) {
    //        int mc = modCount;
    //        for (int i = 0; i < tab.length; ++i) {
    //            for (HashMap.Node<K,V> e = tab[i]; e != null; e = e.next)
    //                action.accept(e.key, e.value);
    //        }
    //        if (modCount != mc)
    //            throw new ConcurrentModificationException();
    //    }
    //}

}
