package com.mlf.demo1;

import java.util.Date;

/**
 * @program: Java基础
 * @description: 泛型类
 * @author: mlf
 * @date: 2022-05-25 15:31
 **/

public class PairTest {

    public static void main(String[] args) throws ClassNotFoundException {
        DateInter dateInter = new DateInter();
        dateInter.setValue(new Date());
        //dateInter.setValue(new Object()); //编译错误 ClassCastException
        // 多态不能体现，方法重载成为了重写，泛型类型擦除后，只能变为了重载
        // 重写：表示方法体、参数等都一样的情况。
    }

}

class Pair<T> {

    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

// 愿意：将父类的泛型类型限定为Date，那么父类里面的两个方法的参数都为Date类型
// 实现继承多态。
class DateInter extends Pair<Date> {

    @Override
    public void setValue(Date value) {
        super.setValue(value);
    }

    @Override
    public Date getValue() {
        return super.getValue();
    }
}


// javap -c className的方式反编译下DateInter子类的字节码

/*
class com.mlf.demo1.DateInter extends com.mlf.demo1.Pair<java.util.Date> {
        com.mlf.demo1.DateInter();
        Code:
        0: aload_0
        1: invokespecial #1                  // Method com/mlf/demo1/Pair."<init>":()V
        4: return

public void setValue(java.util.Date);        //我们重写的setValue方法
        Code:
        0: aload_0
        1: aload_1
        2: invokespecial #2                  // Method com/mlf/demo1/Pair.setValue:(Ljava/lang/Object;)V
        5: return

public java.util.Date getValue();            //我们重写的getValue方法
        Code:
        0: aload_0
        1: invokespecial #3                  // Method com/mlf/demo1/Pair.getValue:()Ljava/lang/Object;
        4: checkcast     #4                  // class java/util/Date
        7: areturn

public void setValue(java.lang.Object);
        Code:
        0: aload_0
        1: aload_1
        2: checkcast     #4                  // class java/util/Date
        5: invokevirtual #5                  // Method setValue:(Ljava/util/Date; 去调用我们重写的setValue方法  )V
        8: return

public java.lang.Object getValue();
        Code:
        0: aload_0
        1: invokevirtual #6                  // Method getValue:()Ljava/util/Date;  去调用我们重写的getValue方法
        4: areturn
}
*/


//其实这在普通的类继承中也是普遍存在的重写，这就是协变。 并且，还有一点也许会有疑问，子类中的巧方法Object getValue()和Date getValue()是同时存在的，
//可是如果是常规的两个方法，他们的方法签名是一样的，也就是说虚拟机根本不能分别这两个方法。
// 如果是我们自己编写Java代码，这样的代码是无法通过编译器的检查的，但是虚拟机却是允许这样做的，因为虚拟机通过参数类型和返回类型来确定一个方法
// ，所以编译器为了实现泛型的多态允许自己做这个看起来“不合法”的事情，然后交给虚拟器去区别







