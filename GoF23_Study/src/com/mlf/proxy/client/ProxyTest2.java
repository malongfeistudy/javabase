package com.mlf.proxy.client;

import com.mlf.proxy.dynamicproxy.DynamicProxy;
import com.mlf.proxy.realfun.impl.RealChar;

/**
 * @program: Java基础
 * @description: jdk动态代理：必须实现接口，代理的实现类也必须实现
 * 为了保持行为的一致性，代理类和委托类通常会实现相同的接口
 * @author: mlf
 * @date: 2022-06-27 11:32
 **/
public class ProxyTest2 {

    public static void main(String[] args) {

        DynamicProxy dynamicProxy = new DynamicProxy();
        dynamicProxy.setTarget(new RealChar());
        Object proxy = dynamicProxy.getProxy();


        //DynamicProxy dynamicProxy1 = new DynamicProxy();
        //dynamicProxy1.setTarget(new RealCharImpl());
        //RealCharInter proxy1 = (RealCharInter) dynamicProxy1.getProxy();
        //
        //proxy1.RealCharFunComm("malongfei");


    }

}





