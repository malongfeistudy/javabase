package com.mlf.thread.votatiles;

import com.mlf.thread.utils.Sleeper;
import lombok.extern.java.Log;

/**
 * @program: Java基础
 * @description: 两阶段终止--volatile
 * @author: mlf
 * @date: 2022-07-27 17:34
 **/

@Log
public class TwoPhaseStop {

    // 监控线程
    private Thread monitorThread;

    // 多线程共享变量 单线程写入（停止线程） 多线程读取 使用 volatile
    private volatile boolean stop = false;

    // 启动监控线程
    public void start() {
        monitorThread = new Thread(() -> {
            log.info("开始监控");
            while (true) {
                log.info("监控中");
                Thread currentThread = Thread.currentThread();
                if (stop) {
                    log.info("正在停止");
                    break;
                }
                try {
                    log.info("正常运行");
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    // sleep出现被打断异常后、被打断后会清除打断标记
                    // 需要重新打断标记
                    currentThread.interrupt();
                }
            }
            log.info("已停止");
        }, "monitor");
        monitorThread.start();
    }

    // 停止监控线程
    public void stop() {
        stop = true;
        monitorThread.interrupt();
    }

}

@Log
class Test {
    public static void main(String[] args) {
        TwoPhaseStop twoPhaseStop = new TwoPhaseStop();
        twoPhaseStop.start();

        Sleeper.sleep(2);
        twoPhaseStop.stop();
        log.info("停止监控");
    }
}
