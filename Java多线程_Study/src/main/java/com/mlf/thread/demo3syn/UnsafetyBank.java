package com.mlf.thread.demo3syn;

//不安全案例2 银行取款
public class UnsafetyBank {

    public static void main(String[] args) {
        Account mlf = new Account("mlf", 100);
        BankGetMoney me = new BankGetMoney(mlf, 100, 0);
        BankGetMoney she = new BankGetMoney(mlf, 100, 0);
        new Thread(me,"1111").start();
        new Thread(she,"2222").start();
    }

}

//个人账户 属性值加上private需要get、set才能访问
class Account{
    String name;
    int money;

    public Account(String name, int money) {
        this.name = name;
        this.money = money;
    }
}

//银行:模拟取款流程
class BankGetMoney implements Runnable {
    //账户
    final Account account;
    //取款金额
    int getMoney;
    //手头现有金额
    int nowMoney;

    public BankGetMoney(Account account, int getMoney, int nowMoney) {
        this.account = account;
        this.getMoney = getMoney;
        this.nowMoney = nowMoney;
    }

    //业务逻辑方法
    public void run() {
        //给需要增删改的地方加把同步锁synchronized ：他可以让线程排队进行达到数据同步安全有序进行
        synchronized(account){
            //判断账户余额是否大于取款金额  账户金额小于取款金额
            if (getMoney > account.money) {
                System.out.println("余额不足！");
                return;
            }
            try {
                Thread.sleep(100);   //sleep可以放大问题的发生性
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            nowMoney += getMoney;
            account.money -= getMoney;
            System.out.println(Thread.currentThread().getName() + "账户余额" + account.money + "手头现有存款：" + nowMoney);
        }

    }
}

