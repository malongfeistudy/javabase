package com.mlf.oop.demo01;

/**
 * @program: Java基础
 * @description: 测试就近原则
 * @author: mlf
 * @date: 2022-05-11 22:30
 **/
public class Demo_2 {

    public static void main(String[] args) {

        Parent p1 = new Parent();

        Parent p2 = new Son();

        p1.setName("父类对象");

        p2.setName("子类对象，父类类型");

        System.out.println(p2.getName());
        System.out.println(p1.getName());

    }

}


class Parent {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Son extends Parent {
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}