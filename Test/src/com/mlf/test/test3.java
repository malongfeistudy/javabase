package com.mlf.test;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-09-28 20:32
 **/
public class test3 {

    public static void main(String[] args) {
        // 对于这样直接赋值的Integer 当值的范围在 [-128,127] 时候 会不生成新的对象
        // 直接把缓存中的对对象拿来用 Integer a=128;Integer b=128 这样的话就是false了
        /**
         * Cache to support the object identity semantics of autoboxing for values between
         * -128 and 127 (inclusive) as required by JLS.
         *
         * The cache is initialized on first usage.  The size of the cache
         * may be controlled by the {@code -XX:AutoBoxCacheMax=<size>} option.
         * During VM initialization, java.lang.Integer.IntegerCache.high property
         * may be set and saved in the private system properties in the
         * sun.misc.VM class.
         */
        Integer i = 128;
        Integer j = 128;
        System.out.println(i == j); //false

        Integer i1 = new Integer(100);
        Integer j1 = new Integer(100);
        System.out.println(i1 == j1);//false

        Integer i2 = 100;
        Integer j2 = 100;
        System.out.println(i2 == j2);//true

        Integer i3 = Integer.valueOf(100);
        Integer j3 = 100;
        System.out.println(i3 == j3);//true

    }

}

class test4 {


    static int n;

    final int var = 1;


    public static void main(String[] args) {

        n = 1;
        n = 2;
        System.out.println(n);
        Integer i = new Integer(1);

        i = 2;
        //System.out.println(i);


        class t {
            final int n = 1;

        }


    }

    static class tt {
        static int i = 1;

    }

    class ttt {
        //static int i = 1;

    }

    public static void test() {

    }


}

// static 可修饰范围总结：
// 1、类、方法、成员变量

// 2、静态内部类可以使用

// 3、静态内部类的成员变量

// 4、静态内部类和静态变量可无限套娃
class T1 {
    static int n;

    public static void t() {
    }

    static class T2 {
        static int n;

        static class T3 {
            static int n;
        }
    }

    public static void main(String[] args) {
        System.out.println(T1.n);
        System.out.println(T1.T2.n);
        System.out.println(T1.T2.T3.n);
    }

}


class C1 {
    static int n;

    class C2 {
        private final static int i = 1;
        private int id;
        private String name;

        public void test() {
        }
    }

    // 非静态类 cannot be referenced from a static context
    public static void main(String[] args) {

        C1 c1 = new C1();
        c1.test();
        // 编译不通过， java: 无法从静态上下文中引用非静态 变量 this
        // 解析：由于 this（main函数）为静态方法，相当于一定存在，但是 类C2 非静态（说明他为类C1的成员）
        //      静态方法main() 一定存在，编译器也需要方法内的实例化对象或者说引用的对象一定存在，但是类C2存在的前提是类C1也存在，。
        //C2 c2 = new C2();
    }

    public void test() {
        C2 c2 = new C2();
        c2.test();
    }

    class a {

    }
}


class B {
    final static int b = 1;
    static int c = 1;
    final StringBuilder d = new StringBuilder("1");

    public static void main(String[] args) {
        int b = B.b;
        c = 2;

        B b1 = new B();

        b1.test(b1.d);
        System.out.println(b1.d);

    }


    public void test(final StringBuilder a) {
        a.append("1");
    }


}







