package com.mlf.thread.print;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description: 先后打印
 * @author: mlf
 * @date: 2022-11-03 11:14
 **/
@Slf4j
public class OrderPrint {

    private final static Object ONE_LOCK = new Object();
    private final static Object TWO_LOCK = new Object();

    public static void main(String[] args) {

        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                3,
                4,
                1,
                TimeUnit.DAYS,
                new ArrayBlockingQueue<>(3),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );

        // 使用线程池创建线程
        executor.execute(new DiyThread(1, ONE_LOCK, TWO_LOCK));
        executor.execute(new DiyThread(2, TWO_LOCK, ONE_LOCK));

        synchronized (ONE_LOCK) {
            ONE_LOCK.notify();
        }

    }

}

/**
 * 自定义线程类，需要使用同一把锁进行控制线程的打印顺序，因此以参数的方式传入即可
 **/
@Slf4j
class DiyThread implements Runnable {

    private final int num;
    private final Object own;
    private final Object other;

    public DiyThread(int num, Object own, Object other) {
        this.num = num;
        this.own = own;
        this.other = other;
    }

    /**
     * 每个线程进来首先需要阻塞自己，然后唤醒其他线程
     * 模拟运行过程：
     * 假如有两个线程，第一个线程，传入两把锁A、B；第二个线程，传入两把锁B、A
     **/
    @Override
    public void run() {
        System.out.println(this.num + "线程开始运行");
        synchronized (own) {
            try {
                own.wait();
                System.out.println(num);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (other) {
            other.notify();
        }

    }

}
