package com.mlf.uuid;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * @program: Java基础
 * @description: Java实现UUID
 * @author: mlf
 * @date: 2022-04-21 14:53
 **/
public final class UUIDTest {

    public static void main(String[] args) {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.nameUUIDFromBytes("ghaergrgssdfferrtr3322332".getBytes(StandardCharsets.UTF_8));
        UUID uuid3 = UUID.fromString("095c64fe-c5bf-11ea-aec6-a402b9e2b04d");

        System.out.println(uuid1.toString());
        System.out.println(uuid2.toString());
        System.out.println(uuid3.toString());


        new Thread("mlf").start();

        //大数类
        BigDecimal bigDecimal = new BigDecimal(10000000);

    }

}
