package com.mlf.thread.demo3syn;

import java.util.ArrayList;

//线程不安全 集合
//有可能造成两个线程同时往一个位置添加 被覆盖 数组长度不够100000

//synchronized中文意思是同步，也称之为”同步锁“。
//synchronized的作用是保证在同一时刻， 被修饰的代码块或方法只会有一个线程执行，以达到保证并发安全的效果。
//synchronized是Java中解决并发问题的一种最常用的方法，也是最简单的一种方法。
public class UnsafetyList {
    public static void main(String[] args) throws InterruptedException {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            new Thread(()->{
                //同步锁 锁住代码块 在当前线程未进行完，其他线程访问时被阻塞 排队进行
                synchronized (list) {
                    list.add(Thread.currentThread().getName());
                }
            }).start();
        }
        Thread.sleep(3000);
        System.out.println(list.size());
    }
}
