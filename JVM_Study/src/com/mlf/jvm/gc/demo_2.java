package com.mlf.jvm.gc;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;

/**
 * @program: Java基础
 * @description: 演示软引用：  软引用可用来实现内存敏感的高速缓存。
 * vm options: -Xmx20m -XX:+PrintGCDetails -verbose:gc
 * @author: mlf
 * @date: 2022-05-16 11:21
 **/
public class demo_2 {

    private static final int _4MB = 4 * 1024 * 1024;

    public static void main(String[] args) throws IOException {

/*        //在这种强引用（此处的list泛型类型）情况下报堆内存不足错误
        // （比如在需要图片缓存时使用强引用会造成内存浪费，容易引发内容空间不足）
        ArrayList<byte[]> list = new ArrayList<>();
        for (int i=0;i<5;i++){
            list.add(new byte[_4MB]);  //-Xmx20m
        }
        System.in.read();*/
        soft();

    }

    //软引用可用来实现内存敏感的高速缓存 生存时间：内存不足时终止

    public static void soft() {

        //此处的ArrayList泛型类型为弱引用（使用SoftReference队列）
        ArrayList<SoftReference<byte[]>> list = new ArrayList<>();
        ReferenceQueue<byte[]> queue = new ReferenceQueue<>();
        for (int i = 0; i < 5; i++) {
            //SoftReference<byte[]> softReference = new SoftReference<>(new byte[_4MB]);
            //关联了引用队列，当软引用所关联的 byte[] 被回收（让ref == null）时，软引用自己加入引用队列中去
            SoftReference<byte[]> softReference = new SoftReference<>(new byte[_4MB], queue);
            System.out.println(softReference.get());
            list.add(softReference);
            System.out.println(list.size());
        }

        //从引用队列中获取、移出软引用对象
        Reference<? extends byte[]> poll = queue.poll();
        while (poll != null) {
            list.remove(poll);
            poll = queue.poll();
        }


        System.out.println("循环结束：" + list.size());
        for (SoftReference<byte[]> ref : list) {
            System.out.println(ref.get());
        }
    }

}
