package com.mlf.syn;

import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-07-18 18:07
 **/
public class OneProblem {

    public static void main(String[] args) throws InterruptedException {

        Phone phone = new Phone();
        new Thread(() -> {
            try {
                phone.sendMsg();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        //TimeUnit.SECONDS.sleep(1);

        new Thread(phone::call).start();

        new Thread(() -> {
            try {
                phone.sendMsg();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

}

class Phone {
    // synchronized方法 锁的是当前调用者本身对象
    public synchronized void sendMsg() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        System.out.println("发短信");
    }

    public void call() {
        System.out.println("打电话");
    }

}
