package com.mlf.jvm.demo;

/**
 * @program: Java基础
 * @description: 常量池  javap -v out/production/JVM_Study/com/mlf/jvm/demo/demo_5.class 查看字节码常量池
 * @author: mlf
 * @date: 2022-05-10 16:01
 **/

//串池：StringTable [ "a", "b", "ab" ]
public class demo_5 {

    public static void main(String[] args) {
        String s1 = "a";
        String s2 = "b";

        String s3 = "ab";  // 6: ldc     #4  ldc 把常量池中a符号变为 “a” 字符串对象
        String s4 = s1 + s2;    //编译期间 s1 ,s2 为变量 不能确定s5 运行期间 new StringBuilder.append("a").append("b") new String("ab")
        String s5 = "a" + "b";  //javac 在编译期间的优化 ， 两个常量（不会变化）拼接直接确定为 "ab"

        String s6 = s4.intern();//先去串池中找，有则不会放入，没有创建一个新对象放入串池，，返回常量池中的对象  29: ldc     #4                  // String ab

        System.out.println(s6 == s3); //true  同一常量池中对象

        System.out.println(s4 == s3); //false s4为新建的StringBuilder.toString: 该对象在堆内存中 s3为常量池中的字符串对象
        System.out.println(s3 == s5); //true  同一常量池中对象

        System.out.println("ab".equals(s3));  //true
        System.out.println("ab".equals(s4));  //true
        System.out.println("ab".equals(s5));  //true
        System.out.println("ab".equals(s6));  //true

    }

}
