package com.mlf.proxy.client;

import com.mlf.proxy.dynamicproxy.DynamicProxy;
import com.mlf.proxy.realfun.UserDao;
import com.mlf.proxy.realfun.impl.UserDaoImpl;

/**
 * @program: Java基础
 * @description: 静态代理测试
 * @author: mlf
 * @date: 2022-04-23 17:09
 **/
public class ProxyTest {

    public static void main(String[] args) {

        //真是角色
        UserDaoImpl userDao = new UserDaoImpl();

        //静态代理  代理角色
        //生成代理对象
        //StaticProxy staticProxy = new StaticProxy();
        //staticProxy.setUserService(userDao);
        //staticProxy.add();

        //动态代理  代理角色
        DynamicProxy dynamicProxy = new DynamicProxy();
        dynamicProxy.setTarget(userDao);
        //动态生成代理类
        UserDao proxy = (UserDao) dynamicProxy.getProxy();
        proxy.add("malongfei");

    }

}
