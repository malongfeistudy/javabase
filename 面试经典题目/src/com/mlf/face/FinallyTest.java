package com.mlf.face;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: Java基础
 * @description: 测试Finally代码块
 * 结论一：  finally语句在return语句执行之后return返回之前执行的
 * 结论二：  finally块中的return语句会覆盖try块中的return返回，但是变量不会覆盖
 * @author: mlf
 * @date: 2022-05-12 22:33
 **/
public class FinallyTest {

    private static final boolean flag = false;

    public static void main(String[] args) {

        System.out.println("test1: " + test1());  //结论一：  finally语句在return语句执行之后return返回之前执行的
        System.out.println("test2: " + test2());  //结论二：  finally块中的return语句会覆盖try块中的return返回
        System.out.println("test3: " + test3());  //结论三：  如果finally语句中没有return语句覆盖返回值，那么原来的返回值:  1、变量不会覆盖
        System.out.println("test4: " + test4());  // 2、Map等集合会被覆盖(因为对象类类型存在堆内存中，返回的对象地址，finally改变地址中的值被调用)
        System.out.println("test5: " + test5());

    }

    private static int test5() {

        int a = 1;

        try {
            int b = 1 / 0;
            return a;
        } catch (Exception e) {
            a += 1;
        } finally {
            a += 1;
        }
        return a;
    }


    private static List<String> test4() {

        ArrayList<String> list = new ArrayList<>();

        try {
            list.add("try");
        } catch (Exception e) {
            list.add("catch");
        } finally {
            list.add("finally");
        }

        return list;
    }

    private static Map<String, String> test3() {

        HashMap<String, String> map = new HashMap<>();

        try {
            System.out.println();
            map.put("key", "valueTry");
        } catch (Exception e) {
            map.put("key", "valueCatch");
        } finally {
            map.put("key", "finally");
            map = null;
        }

        return map;
    }

    public static int test1() {

        int tag = 0;

        try {
            System.out.println("try 代码块:");
            return tag += 1;  //
        } catch (Exception e) {
            System.out.println("catch 代码块:");
        } finally {
            System.out.println("finally 代码块:");
            if (tag != 0)
                tag += 1;
        }

        return tag;
    }


    public static int test2() {

        int tag = 0;

        try {
            System.out.println("try 代码块:");
            return tag += 1;  //
        } catch (Exception e) {
            System.out.println("catch 代码块:");
        } finally {
            System.out.println("finally 代码块:");
            if (tag != 0)
                tag += 1;
            return tag;
        }

        //return tag;
    }

}
