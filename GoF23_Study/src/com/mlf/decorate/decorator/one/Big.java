package com.mlf.decorate.decorator.one;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 大杯
 * @author: mlf
 * @date: 2022-06-01 19:07
 **/
public class Big extends Decorator2 {

    public Big(Beverage beverage) {
        super.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost() + 1;
    }
}
