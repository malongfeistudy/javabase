package com.mlf.test;

import com.mlf.datastructure.tree.UF;

import java.util.Scanner;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-01 15:16
 **/
public class UFTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int N = sc.nextInt();
        UF uf = new UF(N);

        while (uf.getCount() > 1) {
            System.out.println("第一个：");
            int p = sc.nextInt();
            System.out.println("第二个：");
            int q = sc.nextInt();
            //判断p和q是否在同一个组
            if (uf.connected(p, q)) {
                System.out.println("结点：" + p + "结点" + q + "已经在同一个组");
                continue;
            }

            uf.union(p, q);
            System.out.println("总共还有" + uf.getCount() + "个分组");
        }
    }

}
