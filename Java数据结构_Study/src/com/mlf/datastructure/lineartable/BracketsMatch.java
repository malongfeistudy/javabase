package com.mlf.datastructure.lineartable;

/**
 * @program: Java基础
 * @description: 括号匹配问题
 * @author: mlf
 * @date: 2022-05-26 16:49
 **/
public class BracketsMatch {

    public static void main(String[] args) {

        String str = "(上海(长安)(()))";
        boolean match = isMatch(str);
        System.out.println(str + "中的括号是否匹配：" + match);

    }

    private static boolean isMatch(String str) {

        Stack<String> stack = new Stack<>();

        char[] chars = str.toCharArray();
        for (char c : chars) {
            // 遍历str遇到左括号就把str中的左括号逐个放入栈中
            if (c == '(') {
                stack.push(String.valueOf(c));
            } else if (c == ')') {            // 左括号应该对应一个右括号，当遇到右括号时弹出左括号
                // 判断栈否为空，若为空说明栈中没有左括号，return FALSE
                if (stack.isEmpty()) return false;
                    // 若不为空则弹出左括号
                else stack.pop();
            }
        }

        // 遍历结束，判断栈是否为空，若不为空，说明左括号冗余，return FALSE。
        return stack.isEmpty();
    }

}
