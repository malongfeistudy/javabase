package com.mlf.demo1;

import java.util.ArrayList;

/**
 * @program: Java基础
 * @description: 泛型不可引用传递
 * @author: mlf
 * @date: 2022-05-25 15:26
 **/
public class Demo_06 {

    //当我们使用list2引用用get()方法取值的时候，返回的都是String类型的对象（类型检测是根据引用来决定的）
    // ，可是它里面实际上已经被我们存放了Object类型的对象，这样就会有ClassCastException了。
    public static void main(String[] args) {
        ArrayList<Object> list1 = new ArrayList<Object>();
        list1.add(new Object());
        list1.add(new Object());
        //ArrayList<String> list2 = list1; //编译错误，这里的类型检测：list2为String、list1为Object向下转型失败


        ArrayList<String> list3 = new ArrayList<String>();
        list1.add(new String());
        list1.add(new String());

        //ArrayList<Object> list4 = list3; //编译错误，我们使用了泛型，到头来，还是要自己强转，违背了泛型设计的初衷
        //再说，你如果又用list2往里面add()新的对象，那么到时候取得时候，我怎么知道我取出来的到底是String类型的，还是Object类型的呢？ 。

    }

}
