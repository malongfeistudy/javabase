package com.mlf.jvm.demo;

/**
 * @program: Java基础
 * @description: 控制台命令 jps：查看当前进程id  jmap -heap 15864 ：查看15864进程的详细内存使用
 * @author: mlf
 * @date: 2022-05-10 13:37
 **/
public class demo_4 {

    public static void main(String[] args) throws InterruptedException {

        System.out.println("1...");
        Thread.sleep(20000);
        byte[] array = new byte[1024 * 1024 * 10]; //10M

        System.out.println("2...");
        Thread.sleep(20000);

        array = null;

        System.gc();

        System.out.println("3...");
        Thread.sleep(100000L);
    }

}
