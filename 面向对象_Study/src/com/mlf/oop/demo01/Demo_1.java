package com.mlf.oop.demo01;

import org.junit.Test;

import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

/**
 * @program: Java基础
 * @description: USB测试程序
 * @author: mlf
 * @date: 2022-05-11 14:06
 **/
public class Demo_1 {

    private USB[] usbArr = new USB[4];

    public static void main(String[] args) {

        Properties properties = System.getProperties();

        Set<String> propertyNames = properties.stringPropertyNames();

        //propertyNames.forEach(System.out::println);

        for (String key : propertyNames) {
            System.out.println(System.getProperty(key));
        }

    }

    @Test
    public void test() {

        Runtime runtime = Runtime.getRuntime();
        System.out.println(runtime.availableProcessors());
        System.out.println((runtime.freeMemory()) / 1024 / 1024);
        System.out.println(runtime.maxMemory() / 1024 / 1024);

        try {
            runtime.exec("notepad.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test1() {

        System.out.println("==============start=============");

        try {
            int a = 1 / 0;
        } catch (Exception e) {
            System.out.println("catch=======");
            e.printStackTrace();
        }

        System.out.println("==========end============");

    }


    @Test
    public void test3() {

        //Collection
        //HashMap
        //FilterInputStream
        //ByteArrayInputStream
        //
        //OutputStream;
        //FileOutputStream
        //BufferedInputStream
        //Reader

        //Thread.sleep();

        Random random = new Random(2);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt(1000));
        }

    }

}









