package com.mlf.listset;

import org.junit.Test;

import java.util.*;

/**
 * @program: Java基础
 * @description: list 和 set 区别
 * 按元素存取顺序来说，List是有序的，Set是无序的。
 * 按照元素和元素之间的关系来说（内部存储有排序），List是无序的，TreeSet是有序的。
 * 而HashSet怎么说都是无序的
 * @author: mlf
 * @date: 2022-04-28 13:43
 **/
public class List_SetDifferent {

    //list测试
    public static void main(String[] args) {

        List<Object> list = new ArrayList<>();

        list.add("roger");
        list.add(5);
        list.add(6234);
        list.add(62341);
        list.add(1423);
        list.add(134);

        list.forEach(System.out::println);
        //list有序指的是 存取顺序一致
        //输出结果：
        //1
        //5
        //6234
        //1423
        //134

        System.out.println("============================");
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(432);
        linkedList.add(42);
        linkedList.add(43);
        linkedList.add(41);
        linkedList.add(854);

        linkedList.forEach(System.out::println);
        //输出结果
        //432
        //42
        //43
        //41
        //854



    }

    //Set测试  实现类  Comparator接口会进行排序，将形成有序存储
    // 无法存储两种不同的基本类型 若需要两种类型可自定义一个封装类，将两种类型设置为属性
    //向TreeSet中添加的数据，要求是相同类的对象
    //可以按照添加对象的指定属性进行排序
    //注意点1：添加元素所在类，需要实现Comparable接口
    //注意点2：所在类需要重写compareTo方法（可以二级排序比较）
    @Test
    public void test(){
        TreeSet<Object> treeSet1 = new TreeSet<>();
        treeSet1.add(1);
        treeSet1.add(39);
        TreeSet<Object> treeSet = new TreeSet<>();
        treeSet.add(2);
        treeSet.addAll(treeSet1);
        treeSet.add(123);
        treeSet.add(92);
        treeSet.forEach(System.out::println);

        System.out.println("=======================");
        HashSet<Object> hashSet = new HashSet<>();
        hashSet.add("2");
        hashSet.add("432");
        hashSet.add("12");
        hashSet.add("5");
        hashSet.forEach(System.out::println);

        System.out.println("=======================");

        TreeSet<Set> treeSet2 = new TreeSet<>();
        Set mlf1 = new Set("mlf", 173);
        Set mlf2 = new Set("mlf", 17);
        treeSet2.add(mlf1);
        treeSet2.add(mlf2);
        for (Set set : treeSet2) {
            System.out.println(set.toString());
        }
        System.out.println("=======================");
        HashSet<Set> hashSet1 = new HashSet<>();
        hashSet1.add(mlf1);
        hashSet1.add(mlf2);
        for (Set set : hashSet1) {
            System.out.println(set);
        }

        

    }
}

class Set implements Comparable<Set> {

    private String name;
    private int age;

    public Set() {
    }

    public Set(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Set{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Set o) {
        return this.age-o.age;
    }

}










