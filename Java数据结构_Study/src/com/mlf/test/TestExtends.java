package com.mlf.test;

/**
 * @program: 数据结构与算法
 * @description: 面向对象三大特性之一继承测试
 * @author: mlf
 * @date: 2022-04-18 13:58
 **/
public class TestExtends {

    //继承：子承父业 子类可用父类的一切 反之不行
    public static void main(String[] args) {
        Son son = new Son();
        son.setHeight(110);
        son.setName("我是儿子");
        System.out.println(son.getClass().getName()+son.test());
        System.out.println(son.getClass().getName()+son.toString());
        Parents parents = new Parents();
        parents.setName("我是父");
        System.out.println(parents.getClass().getName()+parents.toString());
        System.out.println(parents.getClass().getName()+parents.test());
    }

}

class Son extends Parents {
    int height;

    public Son() {
    }

    public Son(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Son{" +
                "height=" + height +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height > 100) {
            this.height = height;
        }
    }
}

class Parents {
    String name;
    int age;

    @Override
    public String toString() {
        return "Parents{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    public String test() {
        return "我是父类方法！！！";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age>0&&age<150){
            this.age = age;
        }
    }
}
