package com.mlf.jvm.gc;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * @program: Java基础
 * @description: 弱引用：弱引用和软引用的区别在于：弱引用拥有更短暂的生命周期，
 * 不管内存够不够，都会回收，都会回收它的内存。
 * @author: mlf
 * @date: 2022-05-16 12:29
 **/
public class demo_3 {

    private static final int _4MB = 4 * 1024 * 1024;

    public static void main(String[] args) {

        //list->weakReference->byte[]
        ArrayList<WeakReference<byte[]>> list = new ArrayList<>();
        ReferenceQueue<byte[]> queue = new ReferenceQueue<>();
        for (int i = 0; i < 10; i++) {
            WeakReference<byte[]> ref = new WeakReference<byte[]>(new byte[_4MB], queue);
            list.add(ref);
            for (WeakReference<byte[]> weakReference : list) {
                System.out.print(weakReference.get() + " ");
            }
            System.out.println();
        }

        System.out.println("end======" + list.size());
        //从引用队列中获取、移出弱引用对象
        Reference<? extends byte[]> poll = queue.poll();
        while (poll != null) {
            list.remove(poll);
            poll = queue.poll();
        }

    }

}
