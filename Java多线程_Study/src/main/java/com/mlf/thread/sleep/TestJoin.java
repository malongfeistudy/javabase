package com.mlf.thread.sleep;

public class TestJoin implements Runnable{
    @Override
    public void run() {
        for (int i=0;i<10;i++){
            System.out.println("VIP来了"+i);
        }
    }

    //多线程调度看cpu心情调度 没有规律性而言
    public static void main(String[] args) throws InterruptedException {

        TestJoin testJoin = new TestJoin();
        Thread thread = new Thread(testJoin);

        Thread.State state = thread.getState(); //NEW
        System.out.println(state);

        thread.start();//线程开启

        state = thread.getState();//        RUNNABLE
        System.out.println(state);


        for (int i = 0; i < 20; i++) {//主线程
            if (i==10) {
                thread.join();//thread插队 阻塞main线程 自己单独执行
            }
            System.out.println("main线程"+i);
        }
        //while (state!=Thread.State.TERMINATED){
        //    state = thread.getState();// 不终止一直输出
        //    System.out.println(state);
        //}

    }
}
