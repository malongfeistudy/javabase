package com.mlf.proxy.realfun.impl;

import com.mlf.proxy.realfun.UserDao;

/**
 * @program: Java基础
 * @description: 被代理业务类
 * @author: mlf
 * @date: 2022-04-23 16:56
 **/
public class UserDaoImpl implements UserDao {

    public void add(String s) {
        System.out.println("我是DAO实现类add方法！！！" + s);
    }

    public void del() {
        System.out.println("我是DAO实现类del方法！！！");
    }

    public void update() {
        System.out.println("我是DAO实现类update方法！！！");
    }

    public void query() {
        System.out.println("我是DAO实现类query方法！！！");
    }

}
