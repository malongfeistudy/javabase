package com.mlf.thread.obj;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: Java基础
 * @description: 自定义线程池
 * @author: mlf
 * @date: 2022-07-29 18:07
 **/
public class TestThreadPool {
    public static void main(String[] args) {

        ThreadPool threadPool = new ThreadPool(1, 1000,
                TimeUnit.MILLISECONDS, 1, (queue, task) -> {
            //queue.put(task); // 死等
            //queue.offer(task, 500, TimeUnit.MILLISECONDS);// 超时等待
            // 让调用者放弃任务执行
            //System.out.println("放弃任务·····");
            // 让调用者抛异常
            //throw new RuntimeException("tttt");
            // 让调用者自己执行任务
            task.run();
        });

        for (int i = 0; i < 5; i++) {
            int j = i + 1;
            threadPool.execute(() -> {
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程池执行任务" + j);
            });
        }


    }
}

@FunctionalInterface
        // 拒绝策略
interface RejectPolicy<T> {
    void reject(BlockingQueue<T> queue, T task);
}

class ThreadPool {
    // 任务队列
    private BlockingQueue<Runnable> taskQueue;

    // 线程集合
    private HashSet<Worker> workers = new HashSet<>();

    // 核心线程数
    private int coreSize;

    // 获取任务的超时时间
    private long timeout;

    // 时间单位
    private TimeUnit timeUnit;

    // 拒绝策略
    private RejectPolicy<Runnable> rejectPolicy;

    // 执行任务
    public void execute(Runnable task) {
        synchronized (workers) {
            // 如果线程集合未满，则将新任务加入workers
            // 否则就加入到 阻塞队列中 阻塞等待
            if (workers.size() < coreSize) {
                Worker worker = new Worker(task);
                System.out.println("加入任务" + task);
                workers.add(worker);
                worker.start();
            } else {
                //taskQueue.put(task);
                // 死等、带超时等待、让调用者放弃任务执行、让调用者抛异常、让调用者自己执行任务。
                taskQueue.tryPut(rejectPolicy, task);
            }
        }
    }

    public ThreadPool(int coreSize, long timeout, TimeUnit timeUnit, int queueCapacity, RejectPolicy<Runnable> rejectPolicy) {
        this.coreSize = coreSize;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        this.taskQueue = new BlockingQueue<>(queueCapacity);
        this.rejectPolicy = rejectPolicy;
    }

    class Worker extends Thread {

        private Runnable task;

        public Worker(Runnable task) {
            this.task = task;
        }

        @Override
        public void run() {
            // 执行任务
            // 1、当 task 不为空，执行任务
            // 2、当 task 执行完毕，再接着从任务队列获取任务并执行
            //while (task != null || (task = taskQueue.take()) != null) {
            while (task != null || (task = taskQueue.poll(timeout, timeUnit)) != null) {
                try {
                    task.run();
                    System.out.println("线程池 任务运行" + task);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    task = null;
                }
            }
            System.out.println("移除任务列表" + workers);
            synchronized (workers) {
                workers.remove(this);
            }
        }
    }
}


class BlockingQueue<T> {
    // 1、任务队列
    private Deque<T> deque = new ArrayDeque<>();

    // 2、锁
    private ReentrantLock lock = new ReentrantLock();

    // 3、生产者条件变量
    private Condition fullWaitSet = lock.newCondition();

    // 4、消费者条件变量
    private Condition emptyWaitSet = lock.newCondition();

    // 5、队列容量
    private int capacity;

    public BlockingQueue(int capacity) {
        this.capacity = capacity;
    }

    // 带超时的获阻塞取
    public T poll(long timeout, TimeUnit unit) {
        lock.lock();
        try {
            long nanos = unit.toNanos(timeout);
            while (deque.isEmpty()) {
                try {
                    // 等待超时，返回 null
                    if (nanos <= 0) return null;
                    // awaitNanos() 返回的是剩余时间循环
                    nanos = emptyWaitSet.awaitNanos(nanos);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            T t = deque.removeFirst();
            fullWaitSet.signal();
            return t;
        } finally {
            lock.unlock();
        }
    }

    // 阻塞获取方法
    public T take() {
        lock.lock();
        try {
            while (deque.isEmpty()) {
                try {
                    emptyWaitSet.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 若不为空
            T res = deque.removeFirst();
            // 出队后唤醒插入
            fullWaitSet.signal();
            // 将队列元素出队
            return res;
        } finally {
            // ReentrantLock 锁需要在 finally 块中释放
            lock.unlock();
        }
    }

    // 增强 put 方法，加入超时等待
    public boolean offer(T task, long timeout, TimeUnit unit) {
        // 队列已满时需要阻塞插入
        lock.lock();
        try {
            long nanos = unit.toNanos(timeout);
            while (deque.size() == capacity) {
                try {
                    // 若超时，则返回 FALSE
                    if (nanos <= 0) return false;
                    System.out.println("队列已满超时等待" + nanos);
                    nanos = fullWaitSet.awaitNanos(nanos);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("任务集合已满，放入阻塞队列" + task);
            deque.addLast(task);
            emptyWaitSet.signal();
            return true;
        } finally {
            lock.unlock();
        }
    }

    // 阻塞插入方法
    public void put(T t) {
        // 队列已满时需要阻塞插入
        lock.lock();
        try {
            while (deque.size() == capacity) {
                try {
                    System.out.println("等待被获取加入任务集合");
                    fullWaitSet.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("任务集合已满，放入阻塞队列" + t);
            deque.addLast(t);
            emptyWaitSet.signal();
        } finally {
            lock.unlock();
        }
    }

    // 获取阻塞队列的大小
    public int size() {
        lock.lock();
        try {
            return deque.size();
        } finally {
            lock.unlock();
        }
    }

    public void tryPut(RejectPolicy<T> rejectPolicy, T task) {
        lock.lock();
        try {
            if (deque.size() == capacity) {
                // 权利下方，让具体的 rejectPolicy 去实现策略。。。
                rejectPolicy.reject(this, task);
            } else {
                // 有空闲
                System.out.println("加入任务队列" + task);
                deque.addLast(task);
                emptyWaitSet.signal();
            }
        } finally {
            lock.unlock();
        }
    }
}









