package com.mlf.test;

import com.mlf.datastructure.Student;

public class TestCompare {

    public static void main(String[] args) {
        Student s1 = new Student("马云", 40);
        Student s2 = new Student("马化腾", 41);
        Comparable max = getMax(s1, s2);
        System.out.println(max);
    }

    public static Comparable getMax(Comparable c1, Comparable c2){
        int res=c1.compareTo(c2);
        if (res>=0){
            return c1;
        }else {
            return c2;
        }
    }

}
