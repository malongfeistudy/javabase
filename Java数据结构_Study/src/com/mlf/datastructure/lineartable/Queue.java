package com.mlf.datastructure.lineartable;

import java.util.Iterator;

/**
 * @program: Java基础
 * @description: 队列
 * @author: mlf
 * @date: 2022-05-26 18:27
 **/
public class Queue<T> implements Iterable<T> {

    private Node head;
    private Node last;
    private int N;

    public Queue() {
        head = new Node(null, null);
        last = null;
        N = 0;
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public int size() {
        return N;
    }

    // 队列：头删尾插
    public void enqueue(T t) {
        if (last == null) {
            last = new Node(t, null);
            head.next = last;
        } else {
            Node oldLast = this.last;
            last = new Node(t, null);
            oldLast.next = last;
        }
        N++;
    }

    public T dequeue() {
        if (isEmpty()) return null;
        Node oldHead = head.next;
        head.next = oldHead.next;
        N--;
        if (isEmpty()) last = null;
        return oldHead.item;
    }

    @Override
    public Iterator<T> iterator() {
        return new QIterator();
    }

    private class QIterator implements Iterator<T> {
        private Node n = head;

        @Override
        public boolean hasNext() {
            return n.next != null;
        }

        @Override
        public T next() {
            Node node = n.next;
            n = n.next;
            return node.item;
        }
    }

    private class Node {
        public T item;
        public Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
