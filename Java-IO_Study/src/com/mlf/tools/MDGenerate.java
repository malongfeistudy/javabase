package com.mlf.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @program: Java基础
 * @description: 生成MD文档
 * @author: mlf
 * @date: 2022-07-10 11:27
 **/
public class MDGenerate {

    public static void main(String[] args) throws IOException {
        String fileName = "C:\\Users\\马龙飞\\Desktop\\LeetCode-十一月.md";
        StringBuilder content = new StringBuilder();
        String code = "";
        String head = "";
        outDate(0, 30, content, code, head);
        docGenerate(fileName, content);
    }

    // 输出日期 ， 从 start 天后的日期开始，输出 end - start 天的日期
    public static void outDate(int start, int end, StringBuilder content, String code, String head) {
        for (int i = start; i < end; i++) {
            content.append("\n## `Date-`")
                    .append(new SimpleDateFormat("yyyy-MM-dd").format(getNextDays(i)))
                    .append("\n### 题目：").append(head)
                    .append("\n```java").append(code).append("\n```")
                    .append("\n ###");
        }
    }

    // 获取 days 天后的日期
    public static Date getNextDays(int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    // 追加文件内容
    public static void docGenerate(String fileName, StringBuilder content) {
        // 在文件夹目录下新建文件
        File file = new File(fileName);

        FileOutputStream fos = null;
        OutputStreamWriter osw = null;

        try {
            if (!file.exists()) {
                boolean hasFile = file.createNewFile();
                if (hasFile) {
                    System.out.println("file not exists, create new file");
                }
                fos = new FileOutputStream(file);
            } else {
                fos = new FileOutputStream(file, true);
            }
            osw = new OutputStreamWriter(fos, "utf-8");
            // 写入内容
            osw.write(String.valueOf(content));
            // 换行
            osw.write("\r\n");
        } catch (Exception e) {
            System.out.println("写入文件发生异常" + e);
        } finally {
            // 关闭流
            try {
                if (osw != null) {
                    osw.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.out.println("关闭流异常" + e);
            }
        }
    }

}
