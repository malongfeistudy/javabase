package com.mlf.test;

import org.junit.Test;

public class FastSlowTest {



    //寻找有环链表的入口测试
    @Test
    public void testGetEntrance(){
        //创建结点
        Node<String> aa = new Node<>("aa", null);
        Node<String> bb = new Node<>("bb", null);
        Node<String> cc = new Node<>("cc", null);
        Node<String> dd = new Node<>("dd", null);
        Node<String> ee = new Node<>("ee", null);
        Node<String> ff = new Node<>("ff", null);
        Node<String> gg = new Node<>("gg", null);
        //完成结点之间的指向
        aa.next=bb;
        bb.next=cc;
        cc.next=dd;
        dd.next=ee;
        ee.next=ff;
        ff.next=gg;
        //造成链表有环
        gg.next=cc;
        Node entrance = getEntrance(aa);
        assert entrance != null;
        System.out.println(entrance.item);
    }

    //判断是否有环测试
    @Test
    public void testIsCircle(){
        //创建结点
        Node<String> aa = new Node<>("aa", null);
        Node<String> bb = new Node<>("bb", null);
        Node<String> cc = new Node<>("cc", null);
        Node<String> dd = new Node<>("dd", null);
        Node<String> ee = new Node<>("ee", null);
        Node<String> ff = new Node<>("ff", null);
        Node<String> gg = new Node<>("gg", null);
        //完成结点之间的指向
        aa.next=bb;
        bb.next=cc;
        cc.next=dd;
        dd.next=ee;
        ee.next=ff;
        ff.next=gg;
        //造成链表有环
        gg.next=cc;
        System.out.println(isCircle(aa));
    }

    //getMidNum中间值测试
    @Test
    public void testGetMidNum(){
        //创建结点
        Node<String> aa = new Node<>("aa", null);
        Node<String> bb = new Node<>("bb", null);
        Node<String> cc = new Node<>("cc", null);
        Node<String> dd = new Node<>("dd", null);
        Node<String> ee = new Node<>("ee", null);
        Node<String> ff = new Node<>("ff", null);
        Node<String> gg = new Node<>("gg", null);
        //完成结点之间的指向
        aa.next=bb;
        bb.next=cc;
        cc.next=dd;
        dd.next=ee;
        ee.next=ff;
        ff.next=gg;
        String s = getMidNum(aa);
        System.out.println(s);
    }

    //寻找有环链表的入口
    public static Node<String> getEntrance(Node<String> first){
        Node<String> slow= first;
        Node<String> fast= first;
        Node<String> temp = first; //新增临时指针
        while (fast!=null && fast.next != null){
            slow=slow.next;
            fast=fast.next.next;
            //若有环 新增临时指针赋值
            if (slow.equals(fast)){
                //temp和slow相遇的结点为入口（数论知识）
                while (true){
                    temp=temp.next;
                    slow=slow.next;
                    if (temp.equals(slow)) break;
                }
                break;
            }
        }
        return temp;
    }

    //判断是否有环
    public static boolean isCircle(Node first){
        Node<String> slow=first;
        Node<String> fast=first;
        while (fast!=null&&fast.next!=null){
            fast=fast.next.next;
            slow=slow.next;
            if (fast==slow) return true;
        }
        return false;
    }

    //获取中间值
    public static String getMidNum(Node first){

        Node<String> slow=first;
        Node<String> fast=first;

        //改变步长进行遍历  结点数n为奇数 中间值在(n+1)/2处； 偶数为n/2处
        while (fast!=null&&fast.next!=null){
            fast=fast.next.next;
            slow=slow.next;
        }
        return slow.item;
    }

    //结点类
   private static class Node<T>{
        T item;
        Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

}
