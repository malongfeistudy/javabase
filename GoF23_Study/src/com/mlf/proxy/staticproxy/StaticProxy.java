package com.mlf.proxy.staticproxy;

import com.mlf.proxy.realfun.UserDao;
import com.mlf.proxy.realfun.impl.UserDaoImpl;

/**
 * @program: Java基础
 * @description:
 * 静态代理类在不改变实现类的情况下，对实现类进行功能的增加，由此而产生了代理类，生成代理对象
 * 而所谓的静态就是在创建代理类的时候，接口和被代理类都已经被固定了，无法改变，代理类就只能这一种类
 * 静态代理类被调用里边的方法时 重新编译类生成.class文件
 * 缺点：会造成资源浪费 太鸡肋了
 * @author: mlf
 * @date: 2022-04-23 16:54
 **/
public class StaticProxy implements UserDao {

    // 将接口和被代理类固定（ UserDao 和 StaticProxy）
    private UserDaoImpl userDao;

    public void setUserService(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    @Override
    public void add(String s) {
        log("add");
        userDao.add("static");
    }

    @Override
    public void del() {
        log("del");
        userDao.del();
    }

    @Override
    public void update() {
        log("update");
        userDao.update();
    }

    @Override
    public void query() {
        log("query");
        userDao.query();
    }

    //代理类对功能的增强
    private void log(String msg){
        System.out.println("我是代理类添加的消息，使用了"+msg+"方法！！！");
    }

}
