package com.mlf.face;

/**
 * @program: Java基础
 * @description: 面试题：将用户输入的￥1101转换为大写输出：一千一百零一元整
 * @author: mlf
 * @date: 2022-06-15 10:31
 **/
public class MoneyTransform {

    // 存储中文单位和数字
    private static final String[] bitArr_ZH = {"元整", "拾", "百", "千", "万", "十", "百", "千"};
    private static final String[] numArr_ZH = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"};


    public static void main(String[] args) {

        String num = "￥498465";
        //Scanner sc = new Scanner(System.in);
        //String num = sc.nextLine();

        StringBuffer res = getRes(num);

        System.out.println(res);
    }

    // 获取单位
    public static StringBuffer getRes(String s) {

        // 1、首先将字符串中的金额数截取出来
        String numString = s.substring(1);
        // 3、转为字符数组，逐个进行判断
        char[] numCharArr = numString.toCharArray();
        int len = numCharArr.length;
        // 4、定义一个可变长度的字符串进行存储大写结果
        StringBuffer res = new StringBuffer();

        // 5、从高位往低位开始转换
        for (int i = len - 1, j = 0; i >= 0; i--, j++) {
            // 获取当前位大写中文数字，numCharArr的i处即为需要的中文大小，numCharArr[i]为当前数字的大小
            String currNum = numArr_ZH[numCharArr[j] - 48];
            // 大写单位索引，当前i表示为多少位，千：3 个：0
            String currBit = bitArr_ZH[i];

            // 当不为零时，先追加数字，再追加单位
            if (!currNum.equals("零")) {
                res.append(currNum).append(currBit);
            } else {
                // 需要解决两个零同时出现，首先获取上一位(i+1，即上一次循环的数字)的数值是否为零
                String preNum = numArr_ZH[numCharArr[i + 1] - 48];
                // 当出现第一个零时，需要处理
                if (!preNum.equals("零")) {
                    res.append(currNum);
                }
            }
            //System.out.println(res);
        }
        return res;
    }

}

//substring(int beginIndex, int endIndex)  截取范围：beginIndex~endIndex-1