package com.mlf.reflect.t4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @program: Java基础
 * @description: 测试类加载流程
 * @author: mlf
 * @date: 2022-05-27 20:38
 **/
public class TestLoad {

    public static void main(String[] args) throws Exception {
        //new A();
        //Class<?> aClass = Class.forName("com.mlf.reflect.t4.A");
        Class<A> a = A.class;
        A a1 = a.newInstance();
        Field name = a.getDeclaredField("name");
        name.setAccessible(true);
        System.out.println("a1.getName(): " + a1.getName());
        name.set(a1, "malongfei");
        Method getName = a.getMethod("getName");
        Object invoke = getName.invoke(a1);
        System.out.println(invoke);
        System.out.println("a1.getName(): " + a1.getName());

    }

}

class A {
    private String name = "hellokitty";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}



