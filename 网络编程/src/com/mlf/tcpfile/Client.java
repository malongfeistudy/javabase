package com.mlf.tcpfile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @program: Java基础
 * @description: 客户端程序
 * @author: mlf
 * @date: 2022-05-11 23:00
 **/
public class Client {
    public static void main(String[] args) throws IOException {

        Socket socket = new Socket("127.0.0.1", 10001);  //指定连接的服务端IP和端口

        //给服务端发送图片
        OutputStream out = socket.getOutputStream();  //获取服务端Socket输出流对象
        FileInputStream fis = new FileInputStream("A:\\Java开发学习\\Learning\\Java基础\\网络编程\\clientImages\\20201230133209393.png");

        byte[] buf = new byte[1024];
        int len;
        while ((len = fis.read(buf)) != -1) {
            out.write(buf, 0, len);
        }
        socket.shutdownOutput(); //关闭客户端输出流


        InputStream in = socket.getInputStream();  //获取Socket的输入流对象
        //将服务端响应回来的数据显示
        byte[] bufMsg = new byte[1024];
        int num = in.read(bufMsg);
        String Msg = new String(bufMsg, 0, num);
        System.out.println(Msg);

        fis.close();
        socket.close();

    }
}
