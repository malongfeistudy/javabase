package com.mlf.thread.demo2;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.*;

public class TestCallable implements Callable<Boolean> {

    private final String url;
    private final String name;

    public TestCallable(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public Boolean call() {
        WebDownloader webDownloader=new WebDownloader();
        try {
            webDownloader.downloader(url,name);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
        System.out.println("下载的文件名为："+name);
        return true;
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        TestCallable t1 = new TestCallable("https://www.cnblogs.com/images/aggsite/myblog.svg",
                "1.svg");
        TestCallable t2 = new TestCallable("https://www.cnblogs.com/images/aggsite/newpost.svg",
                "2.svg");
        TestCallable t3 = new TestCallable("https://www.cnblogs.com/images/aggsite/search.svg",
                "3.svg");

        //创建执行服务：
        ExecutorService service = Executors.newFixedThreadPool(3);
        Future<Boolean> r1 = service.submit(t1);
        Future<Boolean> r2 = service.submit(t2);
        Future<Boolean> r3 = service.submit(t3);
        //获取结果
        Boolean res1 = r1.get();
        Boolean res2 = r2.get();
        Boolean res3 = r3.get();
        System.out.println(res1);
        System.out.println(res2);
        System.out.println(res3);
        //关闭服务
        service.shutdownNow();
    }
    
}

class WebDownloader{
    public void downloader(String url,String name) throws IOException {
        FileUtils.copyURLToFile(new URL(url),new File(name));
    }
}
