package com.mlf.thread.safe;

import java.io.Serializable;

/**
 * @program: Java基础
 * @description: 单例模式----单例模式有很多实现的方法，饿汉、懒汉、静态内部类、枚举类
 * @author: mlf
 * @date: 2022-07-28 10:28
 **/

// 实现一：饿汉式：类加载就会导致该单例对象被创建

// 问题1：为什么加 final
// 答：防止子类继承 行为方法覆盖破坏单例
// 问题2：如果实现类序列化接口，还要做什么来防止反序列化破坏单例。
//  答：加入一个方法 如下：public Object readResolve() { return INSTANCE; }
//    反序列化过程中 发下有这个方法 会返回该方法的返回的结果
public final class Singleton implements Serializable {
    // 问题3：为什么构造私有？是否能防止反射创建新的实例？
    //      答：不让外部创建该类对象；不能，反射可以暴力 setAccessible = true
    private Singleton() {
    }

    // 问题4：这样初始化是否能够保证单例对象创建时的线程安全？
    //      答：可以。final、static 可以保证 静态成员变量类加载时由JVM来保证
    private static final Singleton INSTANCE = new Singleton();

    // 问题5：为什么提供静态方法而不直接将 INSTANCE 设置为 public，说出知道的理由？
    //      答：可以实现对泛型的支持、体现方法封装、添加更多的的控制条件
    public static Singleton getInstance() {
        return INSTANCE;
    }

    public Object readResolve() {
        return INSTANCE;
    }

}

// 实现二：枚举类
//  问题1：枚举单例是如何限制实例个数的？
//        答：枚举类内部是静态单实例变量
//  问题2：枚举单例在创建时是否有并发问题？
//        答：没有
//  问题3：枚举单例能否被反射破坏单例？
//        答：不能
//  问题4：枚举单例能否被反序列化破坏单例？
//        答：Enum继承Serializable 可以
//  问题5：枚举单例属于懒汉式还是饿汉式？
//        答：饿汉式
//  问题6：枚举单例如果希望加入一些单例创建时的初始化逻辑该如何做？
//        答：给枚举类加入构造方法，在其中加入相应的逻辑
enum SingletonEnum {
    INSTANCE;

    SingletonEnum() {
    }
}

// 实现三： 懒汉式：类加载不会导致该对象被创建、而是首次使用该对象时才会创建
final class SingletonLazy {
    private SingletonLazy() {
    }

    private static SingletonLazy INSTANCE = null;

    // 分析这里的线程安全，并说明有什么缺点？
    // 答：可以保证线程安全，因为是加在静态成员变量上，相当于加在.class上。
    //    缺点：锁的范围太大，性能低
    public static synchronized SingletonLazy getInstance() {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        INSTANCE = new SingletonLazy();
        return INSTANCE;
    }
}

// 实现四：DCL（double checked）：加入 volatile 线程安全
final class SingletonLazyVolatile {
    private SingletonLazyVolatile() {
    }

    // 问题1：为什么加入 volatile 关键字
    // 答：   防止指令重排序 造成返回对象不完整。 如 TODO
    private static volatile SingletonLazyVolatile INSTANCE = null;

    // 问题2：对比 实现3 说出这样做的意义
    // 答：没有锁进行判断、效率较高
    public static SingletonLazyVolatile getInstance() {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        // 问题3：为什么要在这里加空判断，之前不是判断过了吗？
        // 答：加入t1 先进入判断空成立，先拿到锁， 然后到实例化对象这一步（未执行）
        //    同时 线程 t2 进入阻塞状态，若 t1 完成创建对象后，t2 没有在同步块这进行判空，t2 会再新创建一个对象，
        //    导致 t1 的对象被覆盖 造成线程不安全。
        synchronized (SingletonLazyVolatile.class) {  // t1
            if (INSTANCE != null) {
                return INSTANCE;
            }
            INSTANCE = new SingletonLazyVolatile();   // t1  这行代码会发生指令重排序，需要加入 volatile
            // 如：先赋值指令INSTANCE = new SingletonLazyVolatile，导致 实例不为空， 下一个线程会判空失败 返回该对象。
            // 但是构造方法()指令还没执行，返回的就是一个不完整的对象。
            return INSTANCE;
        }
    }
}

// 实现5：静态内部类 （推荐使用）
final class SingletonStaticInnerClass {
    private SingletonStaticInnerClass() {
    }

    // 问题1：属于懒汉式还是饿汉式？ 答：懒汉式，静态内部类，调用时才会加载
    private static class LazyHolder {
        static final SingletonStaticInnerClass INSTANCE = new SingletonStaticInnerClass();
    }

    // 问题2：在创建时是否有并发问题?  答：是线程安全的，静态的只会加载一次，并且由JVM保证。
    public static SingletonStaticInnerClass getInstance() {
        return LazyHolder.INSTANCE;
    }
}
