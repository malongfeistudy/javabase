package com.mlf.datastructure.lineartable;

/**
 * @program: Java基础
 * @description: 给定一个只包含加减乘除四种运算的逆波兰表达式的数组表示方式，求出该逆波兰表达式的结果。
 * @author: mlf
 * @date: 2022-05-26 17:07
 **/
public class ReversePolishNotation {

    public static void main(String[] args) {
        //中缀表达式3*（17-15）+18/6的逆波兰表达式如下
        String[] notation = {"3", "17", "15", "-", "*", "18", "6", "/", "+"};
        int result = caculate(notation);
        System.out.println("逆波兰表达式的结果为：" + result);
    }

    private static int caculate(String[] notation) {
        String s = "+-*/"; //运算符
        Stack<Integer> stack = new Stack<>(); // 存放操作数
        for (String curr : notation) {
            Integer o1;
            Integer o2;  // 先进后出，o2是运算符前面的操作数
            if (s.contains(curr)) { // 如果是运算符，则需要计算结果值
                o1 = stack.pop();
                o2 = stack.pop(); //取出两个操作数
                int o3; //存放计算结果
                switch (curr) {
                    case "+":
                        o3 = o2 + o1;
                        stack.push(o3); //将计算结果入栈
                        break;
                    case "-":
                        o3 = o2 - o1;
                        stack.push(o3);
                        break;
                    case "*":
                        o3 = o2 * o1;
                        stack.push(o3);
                        break;
                    case "/":
                        o3 = o2 / o1;
                        stack.push(o3);
                        break;
                    default:
                        System.out.println("发生异常····");
                        break;
                }
            } else {
                stack.push(Integer.valueOf(curr)); //将操作数放入栈中
            }
        }
        // 最终返回栈中的结果
        return stack.pop();
    }

}
