package com.mlf.reflect.t3;

import java.lang.reflect.Field;

/**
 * @program: Java基础
 * @description: 測試Filed操作: Field 提供有关类或接口的单个字段的信息，以及对它的动态访问权限。
 * 反射的字段可能是一个类（静态）字段或实例字段。
 * @author: mlf
 * @date: 2022-05-25 18:03
 **/
public class FiledTest {

    //上述方法需要注意的是，如果我们不期望获取其父类的字段，
    // 则需使用Class类的getDeclaredField/getDeclaredFields方法来获取字段即可，
    // 倘若需要连带获取到父类的字段，那么请使用Class类的getField/getFields，
    // 但是也只能获取到public修饰的的字段，无法获取父类的私有字段
    public static void main(String[] args) throws Exception {

        Class<?> clazz = Class.forName("com.mlf.reflect.t1.User");

        System.out.println("getDeclaredFields不包含继承字段，但包含私有");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        System.out.println("========");
        Field name = clazz.getDeclaredField("age");
        System.out.println(name);

        System.out.println("====Field包含继承字段但只能获取公共字段=========");
        Field[] fields1 = clazz.getFields();
        for (Field field : fields1) {
            System.out.println(field);
        }

        System.out.println("======");
        Field id = clazz.getField("id");
        System.out.println(id);


    }

}
