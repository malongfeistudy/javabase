package com.mlf.test;

import com.mlf.datastructure.tree.RedBlackTree;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-2-24 22:09
 **/
public class RedBlackTreeTest {

    public static void main(String[] args) {
        RedBlackTree<String, String> tree = new RedBlackTree<>();
        tree.put("1", "111");
        tree.put("2", "222");
        tree.put("3", "333");

        tree.get("1");
        tree.get("2");
        System.out.println(tree.get("1"));
        System.out.println(tree.get("2"));
        System.out.println(tree.get("3"));
    }

}
