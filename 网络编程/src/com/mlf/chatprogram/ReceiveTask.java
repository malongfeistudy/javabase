package com.mlf.chatprogram;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @program: Java基础
 * @description: 接收数据任务类
 * @author: mlf
 * @date: 2022-05-11 21:40
 **/
public class ReceiveTask implements Runnable {

    private int receivePort;

    public ReceiveTask(int receivePort) {
        this.receivePort = receivePort;
    }

    @Override
    public void run() {
        try {
            DatagramSocket datagramSocket = new DatagramSocket(receivePort);

            byte[] data = new byte[1024];

            DatagramPacket datagramPacket = new DatagramPacket(data, data.length);

            while (true) {
                datagramSocket.receive(datagramPacket);  //接收数据并给datagramPacket封装
                String info = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                System.out.println("收到" + datagramPacket.getAddress().getHostAddress() + ":" + datagramPacket.getPort() + "发送的数据" + info);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




