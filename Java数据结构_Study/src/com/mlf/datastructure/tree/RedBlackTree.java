package com.mlf.datastructure.tree;

/**
 * @program: Java基础
 * @description: 红黑树API设计
 * @author: mlf
 * @date: 2022-05-24 20:16
 **/
// 定义泛型Key的上界为Comparable
public class RedBlackTree<Key extends Comparable<Key>, Value> {
    // 结点类
    private class Node {
        // 存储键
        public Key key;
        // 存储值
        private Value value;
        // 记录左子结点
        public Node left;
        // 记录右子结点
        public Node right;
        public boolean color;

        public Node(Key key, Value value, Node left, Node right, boolean color) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
            this.color = color;
        }
    }

    // 根节点
    private Node root;
    private int N;
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    // 判断当前结点的父指向链接是否为红色
    private boolean isRed(Node x) {
        if (x == null) return false;
        return x.color == RED;
    }

    // 左旋调整
    private Node rotateLeft(Node h) {
        // 获取h结点的右子结点，表示为x
        Node x = h.right;
        // 让x的左子结点成为h的右子结点
        h.right = x.left;
        // 让h成为x结点的左子结点
        x.left = h;
        // 让x结点的color属性等于h的color属性
        x.color = h.color;
        // 让h结点的color属性变为红色RED
        h.color = RED;
        return x;
    }

    // 右旋调整
    private Node rotateRight(Node h) {
        // 获取结点h的左子结点
        Node x = h.left;
        // 将x的右子结点变为h的左子结点
        h.left = x.right;
        // 将h变为x的右子结点
        x.right = h;
        // 标记x结点的color属性为h结点的color属性
        x.color = h.color;
        // 将h结点的color属性变为RED
        h.color = RED;

        return x;
    }

    // 颜色反转，相当于完成拆分4-结点
    private void flipColors(Node h) {
        // 将h结点的子结点变为黑色
        h.left.color = BLACK;
        h.right.color = BLACK;
        // 将h结点变为红色
        h.color = RED;
    }

    // 在整个树上完成插入操作
    public void put(Key key, Value val) {
        // 在root整个树上插入key-val
        root = put(root, key, val);
        // 修改根节点为黑色
        root.color = BLACK;

    }

    // 在指定树中，完成插入操作，并返回添加元素后新的树
    private Node put(Node h, Key key, Value val) {
        // 如果插入的当前结点h为空时，新建一个红色结点
        if (h == null) {
            N++;
            return new Node(key, val, null, null, RED);
        }
        // h不为空时，比较插入的key和当前结点key，有三种情况 递归寻找
        int cmp = key.compareTo(h.key);
        // 1、小于，继续寻找左子树插入，
        if (cmp < 0) {
            h.left = put(h.left, key, val);
        } else if (cmp > 0) {// 2、大于，继续寻找右子树插入
            h.right = put(h.right, key, val);
        } else h.value = val;// 3、等于，覆盖更新结点value

        // 旋转调整和颜色反转
        // 1、右旋转：该结点的左子结点及左孙子结点都为红色时
        if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
        // 2、左旋转：该结点的左子结点为黑色，右子结点为红色时
        if (!isRed(h.left) && isRed(h.right)) h = rotateLeft(h);
        // 3、颜色反转：当左右子结点都为红色时
        if (isRed(h.left) && isRed(h.right)) flipColors(h);

        return h;
    }

    // 根据key，从树中找出对应的值
    public Value get(Key key) {
        return get(root, key);
    }

    // 从指定树x中找出key对应的值
    public Value get(Node h, Key key) {

        if (h == null) return null;

        int cmp = key.compareTo(h.key);
        if (cmp > 0) return get(h.right, key);
        else if (cmp < 0) return get(h.left, key);
        else return h.value;
    }

    // 获取树中元素的个数。
    public int size() {
        return N;
    }


}

