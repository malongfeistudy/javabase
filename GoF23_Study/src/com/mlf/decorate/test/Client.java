package com.mlf.decorate.test;

import com.mlf.decorate.component.Beverage;
import com.mlf.decorate.concretecompoent.HouseBlend;
import com.mlf.decorate.decorator.one.Big;
import com.mlf.decorate.decorator.two.Ice;
import com.mlf.decorate.decorator.two.Milk;
import com.mlf.decorate.decorator.two.Sugar;

/**
 * @program: Java基础
 * @description: 消费者
 * @author: mlf
 * @date: 2022-06-01 18:35
 **/
public class Client {

    public static void main(String[] args) {
        Beverage beverage = new HouseBlend();
        beverage = new Ice(beverage);
        beverage = new Milk(beverage);
        beverage = new Sugar(beverage);
        beverage = new Big(beverage);
        System.out.println(beverage.cost());
    }

}




