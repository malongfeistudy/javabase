package com.mlf.test;


import java.util.Random;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-28 17:00
 **/
public class TestRandom {

    public static void main(String[] args) {

        test test = new test();
        System.out.println(test.getA());
        int t = test.getB();
        System.out.println(t);

    }

    public final static class test {
        final int a = 1;
        Random r = new Random();
        final int b = r.nextInt();

        public int getA() {
            return a;
        }

        public Random getR() {
            return r;
        }

        public void setR(Random r) {
            this.r = r;
        }

        public int getB() {
            return b;
        }
    }
}

