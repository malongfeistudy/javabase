package com.mlf.decorate.decorator.two;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 需要加入的配料：牛奶
 * @author: mlf
 * @date: 2022-06-01 18:26
 **/
public class Milk extends CondimentDecorator {

    public Milk(Beverage beverage) {
        super.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost() + 1;
    }

}
