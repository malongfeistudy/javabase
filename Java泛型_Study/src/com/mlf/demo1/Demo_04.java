package com.mlf.demo1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: Java基础
 * @description: 泛型和数组：1.数组是协变的，泛型是不可变的。
 * 2.数组是具化的，而泛型是通过类型擦除来实现的。
 * @author: mlf
 * @date: 2022-05-22 11:33
 **/
public class Demo_04 {

    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        //List<Integer>[] list11 = new ArrayList<Integer>[10]; //编译错误，非法创建
        //List<String>[] list12 = new ArrayList<?>[10]; //编译错误，需要强转类型
        List<String>[] list13 = (List<String>[]) new ArrayList<?>[10]; //OK，但是会有警告
        //List<?>[] list14 = new ArrayList<String>[10]; //编译错误，非法创建
        List<?>[] list15 = new ArrayList<?>[10]; //OK
        List<String>[] list6 = new ArrayList[10]; //OK，但是会有警告


    }

    public <T> void ArrayWithTypeToken(Class<T> type, int size) {
        T[] array = (T[]) Array.newInstance(type, size);
    }

    @Test
    public void test() {
        Integer[] integers = new Integer[10];
        Object[] obj = integers;
        obj[0] = "123";//警告会产生数组存储异常ArrayStoreException。
    }

}
