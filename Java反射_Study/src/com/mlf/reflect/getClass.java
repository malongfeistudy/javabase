package com.mlf.reflect;

import org.junit.Test;

import java.lang.reflect.Field;

/*
 * @Description:
    getFields() 获得某个类的所有的公共（public）的字段，包括继承自父类的所有公共字段。
    类似的还有getMethods和getConstructors。
*   getDeclaredFields 获得某个类的自己声明的字段，即包括public、private和proteced，默认但是不包括父类声明的任何字段。
*   类似的还有getDeclaredMethods和getDeclaredConstructors。
 * @Param:
 * @return:
 * @Author: mlf
 * @Date: 2022/5/21
 */
//jvm 中一个类只有一个hashcode值
public class getClass {
    public static void main(String[] args) throws ClassNotFoundException {

        Person person=new Student();

        Class c1 = person.getClass();

        Class c2 = Class.forName("com.mlf.reflect.Student");

        Class c3 = Student.class;

        Class c4 = Integer.TYPE;

        Class c5 = c1.getSuperclass();

        Person person1 = new Person();

        Class c6 = person1.getClass();

        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());
        System.out.println(c4.hashCode());
        System.out.println(c5.hashCode());
        System.out.println(c6.hashCode());
    }

    @Test
    public void test() throws IllegalAccessException, InstantiationException {
        Class<Person> person = Person.class;
        System.out.println(person.getName());//com.mlf.reflect.Person
        System.out.println(person.getCanonicalName());//com.mlf.reflect.Person
        System.out.println(person.getSimpleName());//Person
        System.out.println(person.isInterface());//false
        System.out.println("===================");
        for (Field field : person.getFields()) {
            System.out.println(field.getName());  //name   只打印public字段
        }

        System.out.println("===================");

        // 父类的父类的公共字段也打印出来了
        Class<Student> student = Student.class;
        Student stu = student.newInstance();//创建实例
        for (Field field : stu.getClass().getFields()) {
            System.out.println(field.getName());//age  name
        }
        System.out.println("===================");
        // 自己类声明的字段
        for (Field declaredField : stu.getClass().getDeclaredFields()) {
            System.out.println(declaredField.getName()); //age
        }

    }

}


class Person{
    public String name;
    private int id;
    public Person() {
        id = 123;
    }

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}


class Student extends Person{
    public int age;
    public Student() {
        this.name = "stu";
        this.age = 18;
    }

    public Student(String name,int age) {
    }
}

class Teacher extends Person{
    int age;
    public Teacher() {
        this.name = "tea";
        this.age = 18;
    }


    public Teacher(String name) {

    }
}
