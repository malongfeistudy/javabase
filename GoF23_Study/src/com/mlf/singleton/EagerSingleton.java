package com.mlf.singleton;

/**
 * @program: Java基础
 * @description:
 *    饿汉式单例这种方式比较常用，但容易产生垃圾对象。
 *  * 优点：没有加锁，执行效率会提高。
 *  * 缺点：类加载时就初始化，浪费内存
 * @author: mlf
 * @date: 2022-04-25 20:15
 **/
public class EagerSingleton {

    //私有构造函数
    private EagerSingleton(){}

    private static EagerSingleton eagerSingleton = new EagerSingleton();

    public static EagerSingleton getEagerSingleton() {
        return eagerSingleton;
    }

}
