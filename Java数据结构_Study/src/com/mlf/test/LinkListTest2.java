package com.mlf.test;

import com.mlf.datastructure.linklist.LinkList;

public class LinkListTest2 {
    public static void main(String[] args) {
        LinkList<String> s1=new LinkList<>();
        s1.insert("我是谁");
        s1.insert("我在哪");
        s1.insert("咋办呀");
        s1.insert("干什么");
        System.out.println("---------------------");
        for (int i=0;i<s1.length();i++){
            System.out.println(s1.get(i));
        }

        System.out.println("---------------------");
        s1.reverse();
        for (int i=0;i<s1.length();i++){
            System.out.println(s1.get(i));
        }

    }
}
