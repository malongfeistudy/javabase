package com.mlf.pojo;

import java.io.Serializable;

/**
 * @program: Java基础
 * @description: 实体类person实现序列化接口实验
 * @author: mlf
 * @date: 2022-04-21 18:04
 **/
public class Person extends Parent implements Serializable {

    private static final long serialVersionUID = 1L;

    String name;
    int age;

    public Person(String name, int age, int num) {
        this.name = name;
        this.age = age;
        this.num = num;
    }

    public String toString() {
        return "name:" + name + "\tage:" + age + "\num:" + num;
    }

}
