package com.mlf.interview;

/**
 * @program: Java基础
 * @description: 测试一个父类parent，两个子类son1、son2之间可以实例化引用的关系。
 * @author: mlf
 * @date: 2022-09-15 09:52
 **/
public class ProblemTest {
    public static void main(String[] args) {
        Parent p1 = new Son1();
        Parent p2 = new Son2();
        //Son1 son2 = (Son1)new Son2(); // 报错
        Son1 son1 = null; // 父类强转为子类，会报异常：.ClassCastException
        try {
            son1 = (Son1) new Parent();
            son1.test1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


class Parent {
    public void test1() {
    }

    public String toString1() {
        return "test";
    }
}

class Son1 extends Parent {
    @Override
    public void test1() {
    }
}

class Son2 extends Parent {
}
