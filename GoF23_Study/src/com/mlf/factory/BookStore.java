package com.mlf.factory;

/**
 * @program: Java基础
 * @description: 实体书店
 * @author: mlf
 * @date: 2022-04-23 13:52
 **/
public interface BookStore {

    void sellingBooks();

}
