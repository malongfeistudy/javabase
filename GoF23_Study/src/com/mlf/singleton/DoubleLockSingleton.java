package com.mlf.singleton;

/**
 * @program: Java基础
 * @description: 双检锁单例：双检锁/双重校验锁（DCL，即 double-checked locking）  *
 *               这种方式采用双锁机制，安全且在多线程情况下能保持高性能。
 * @author: mlf
 * @date: 2022-04-25 20:30
 **/
public class DoubleLockSingleton {

    //私有构造函数 只包含静态成员
    private DoubleLockSingleton() {
    }

    private static DoubleLockSingleton doubleLockSingleton = null;

    public static DoubleLockSingleton getDoubleLockSingleton() {

        //第一次检查实例是否存在，若不存在进入下面同步块
        if (doubleLockSingleton == null) {

            //同步块，线程安全的创建实例
            synchronized (DoubleLockSingleton.class) {
                //再次检查实例是否存在，不存在才创建真正的实例
                if (doubleLockSingleton == null) {
                    doubleLockSingleton = new DoubleLockSingleton();
                }
            }
        }

        return doubleLockSingleton;
    }
}
