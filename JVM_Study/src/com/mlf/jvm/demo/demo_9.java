package com.mlf.jvm.demo;

/**
 * @program: Java基础
 * @description: 测试StringTable 被gc回收
 * VM options:-Xmx10m -XX:+PrintStringTableStatistics -verbose:gc
 * @author: mlf
 * @date: 2022-05-10 21:57
 **/
public class demo_9 {

    //串池字符串数量为：23318 存入的为100000条数据 其余的被回收了
    //[GC (Allocation Failure)  2048K->758K(9728K), 0.0009500 secs]
    //[GC (Allocation Failure)  2806K->809K(9728K), 0.0010294 secs]
    //[GC (Allocation Failure)  2857K->901K(9728K), 0.0010053 secs]
    //Number of buckets       :     60013 =    480104 bytes, avg   8.000
    //Number of entries       :     23318 =    559632 bytes, avg  24.000
    public static void main(String[] args) {

        int j = 0;

        try {
            for (int i = 0; i < 100000; i++) {
                String.valueOf(i).intern(); //放入串池
                j++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println(j);
        }

    }

}
