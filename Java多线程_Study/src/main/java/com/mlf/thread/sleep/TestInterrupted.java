package com.mlf.thread.sleep;

import lombok.extern.java.Log;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-07-23 10:02
 **/

@Log
public class TestInterrupted {
    @Test
    public void test() throws InterruptedException {


        Thread t1 = new Thread(() -> {
            while (true) {
                Thread current = Thread.currentThread();
                boolean interrupted = current.isInterrupted();
                if (interrupted) {

                    break;
                }
            }
        }, "t1");

        t1.start();

        TimeUnit.SECONDS.sleep(1);

        t1.interrupt();

    }

}










