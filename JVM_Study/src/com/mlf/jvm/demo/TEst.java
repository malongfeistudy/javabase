package com.mlf.jvm.demo;

import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-08-08 23:30
 **/
public class TEst {

    public static void main(String[] args) {


        // new String("mlf");  此时 b 在堆内存中
        String b = new String("m") + new String("lf");
        // b为堆内存中对象, "mlf" 为串池中对象
        System.out.println("mlf" == b);  // FALSE

    }

    @Test
    public void test1() {
        // new String("mlf");  此时 b 在堆内存中
        String b = new String("m") + new String("lf");
        // 尝试将 b 放入串池
        b.intern();


        // 都为串池中对象
        System.out.println("mlf" == b); // true
    }

    @Test
    public void test2() {
        // new String("m")
        String a = "m";
        // vm调用StringBuild.append("m").append("lf").toString()
        // 最终相当于变成 new String("mlf")
        String b = a + "lf";
        // 将 b 移动放入串池中  最终返回串池中的对象  jdk 1.6 是复制放入，1.8 是
        String c = b.intern();

        // b c 都为串池中的对象
        System.out.println(b == c);
    }


    @Test
    public void test() {
        // 尾插法
        LinkedList<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Integer integer = list.removeLast();
        System.out.println(integer);
    }

    @Test
    public void test11() {

        LinkedList<Object> objects = new LinkedList<>();
        objects.getLast();
        objects.isEmpty();

        HashMap<Object, Object> map = new HashMap<>();
        //Object aDefault = map.getOrDefault(1);
        int[] nums = new int[1];
        List<int[]> objects1 = new LinkedList<>();
        //objects.contains()
        objects1.remove(1);

    }


}
