package com.mlf.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: Java基础
 * @description: 动态代理：当实际开发中有很多这样的主题接口和类需要功能增强时，就需要更多的代理类，即每一个主题接口都得创建一个代理类，
 *                       会造成代码的繁多和冗余，因此就产生了我们的动态代理技术
 *                       动态代理是在运行时动态生成的，即编译完成后没有实际的class文件，而是在运行时动态生成类字节码，并加载到JVM中
 * @author: mlf
 * @date: 2022-04-23 17:21
 **/
//动态生成代理类
public class DynamicProxy implements InvocationHandler {  // 实现调用处理接口

    //被代理的接口对象
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    //创建动态代理对象用Proxy类中的newProxyInstance()方法
    // 参数信息：
    //参数一：通过反射得到类加载器
    //参数二：需要实现的接口
    //参数三：处理者  这个参数需要一个InvocationHandler(接口)的对象，
    // 我们这个自定义代理类实现 了InvocationHandler接口，所以用this调用自己
    // 返回指定接口的代理类的实例，该接口将方法调用分派给指定的调用处理程序。
    //Proxy.newProxyInstance因为与IllegalArgumentException相同的原因而Proxy.getProxyClass
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),
                target.getClass().getInterfaces(),this);
    }

    // 处理代理实例，并返回结果
    // method这个参数其实就是我们要增强的方法，也就是需要代理类去调用的方法，
    // 通过这个参数调用invoke()方法 invoke翻译：调用
    // method.invoke()通过反射去调用我们target接口里的方法 动态之所以就在这
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log(method.getName());

        return method.invoke(target, args);
    }

    private void log(String msg){
        System.out.println("我是代理类添加的消息，使用了"+msg+"方法！！！");
    }

}















