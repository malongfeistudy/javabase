package com.mlf.singleton;

/**
 * @program: Java基础
 * @description: 枚举单例模式
 * @author: mlf
 * @date: 2022-04-25 22:31
 **/


enum EnumSingleton{
    EnumSingleton;
}


/*public final class EnumSingleton extends Enum{
    //私有构造方法，这里调用了父类的构造方法，其中参数s对应了常量名，参数i代表枚举的一个顺序(这个顺                       序与枚举的声明顺序对应，用于oridinal()方法返回顺序值)
    private EnumSingleton()
    {
        super();
    }
    //定义的枚举在这里声明了SINGLETON5 Singleton5的常量对象引用
    public static final EnumSingleton $VALUES;

    //将所有枚举的实例存放在数组中
    private static final EnumSingleton $VALUES[];

    //对象的实例化在static静态块中
    static
    {
        EnumSingleton = new EnumSingleton("EnumSingleton", 0);
        //将所有枚举的实例存放在数组中
        $VALUES = (new EnumSingleton[] {
                EnumSingleton
        });
    }
}*/
