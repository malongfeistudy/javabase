package com.mlf.test;

import com.mlf.datastructure.priority.IndexMinPriorityQueue;

/**
 * @program: Java基础
 * @description: 最xiao优先队列测试
 * @author: mlf
 * @date: 2022-05-23 17:22
 **/
public class IndexMinPriorityQueueTest<T extends Comparable<T>> {

    public static void main(String[] args) {

        //创建索引最小优先队列对象
        IndexMinPriorityQueue<String> queue = new IndexMinPriorityQueue<>(10);

        //往队列中添加元素
        queue.insert(0, "A");
        queue.insert(1, "C");
        queue.insert(2, "F");

        //测试修改
        //queue.changeItem(2, "B");
        queue.delete(1);

        //测试删除
        while (!queue.isEmpty()) {
            int index = queue.delMin();
            System.out.print(index + " ");

        }


    }
}









