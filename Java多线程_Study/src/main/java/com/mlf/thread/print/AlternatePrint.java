package com.mlf.thread.print;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description: 交替打印
 * @author: mlf
 * @date: 2022-11-02 13:48
 **/
public class AlternatePrint {

    /**
     * 每个使用对应唯一的对象作为监视器对象锁。
     */
    public static final Object A_O = new Object();
    public static final Object B_O = new Object();
    public static final Object C_O = new Object();

    public static void main(String[] args) throws InterruptedException {

        /** 参数：
         * int corePoolSize,                     核心线程数
         * int maximumPoolSize,                  最大线程数
         * long keepAliveTime,                   救急存活时间
         * TimeUnit unit,                        单时间位
         * BlockingQueue<Runnable> workQueue,    阻塞队列
         * ThreadFactory threadFactory,          线程工厂
         * RejectedExecutionHandler handler      拒绝策略
         **/
        // 使用阿里巴巴推荐的创建线程池的方式
        // 通过ThreadPoolExecutor构造函数自定义参数创建
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                3,
                5,
                1,
                TimeUnit.DAYS,
                new ArrayBlockingQueue<>(2),
                new ThreadPoolExecutor.AbortPolicy());


        // ExecutorService executor = Executors.newFixedThreadPool(3) ;
        executor.execute(new AlternateThread("1", A_O, B_O));
        executor.execute(new AlternateThread("2", B_O, C_O));
        executor.execute(new AlternateThread("3", C_O, A_O));
        // 等待线程创建完成
        Thread.sleep(1000);
        // 此时类似于死锁····每个线程都会将自己阻塞，需要其他线程来唤醒
        // 也就是循环等待锁住了对方需要的资源，因此需要一个线程进来打破僵局，那就是主线程
        // 再来复习一下死锁的必要条件：1、循环等待
        // 2、请求保持 3、不可抢占 4、互斥

        // 每个线程进入首先都会调用自己的waitFor对象的wait()方法阻塞自己，如果
        synchronized (A_O) {
            A_O.notify();
        }

    }

}


/**
 * 模拟执行流程
 * 打印名（name）    等待标记（waitFor）   下一个标记
 * 1                 A                 B
 * 2                 B                 C
 * 3                 C                 A
 * <p>
 * 像不像Spring的循环依赖：确实很像，Spring中的循环依赖就是 BeanA 依赖 BeanB，BeanB 依赖 BeanA；
 * 他们实例化过程中都需要先属性注入对方的实例，倘若刚开始的时候都没有实例化，初始化就会死等。类似于死锁。
 **/
class AlternateThread implements Runnable {

    private final String name;

    /**
     * 阻塞锁对象  等待标记
     **/
    private final Object waitFor;

    /**
     * 执行锁对象  下一个标记
     **/
    private final Object next;

    /**
     * 注意：使用 wait、notify 的时候需要加 synchronized 锁。
     **/
    public AlternateThread(String name, Object waitFor, Object next) {
        this.name = name;
        this.waitFor = waitFor;
        this.next = next;
    }

    @Override
    public void run() {

        while (true) {
            // 一个线程一旦调用了任意对象的wait()方法，它就释放了所持有的监视器对象上的锁，并转为非运行状态。
            // 每个线程首先会调用 waitFor对象的 wait()方法，随后该线程进入阻塞状态，等待其他线程执行自己 waitFor对象的 notify()方法
            synchronized (waitFor) {
                try {
                    waitFor.wait();
                    System.out.println(name + " 开始执行");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // 唤醒下一个对象
            synchronized (next) {
                next.notify();
            }
        }

    }

}
