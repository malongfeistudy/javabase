package com.mlf.jvm.demo;

import java.util.Arrays;
import java.util.List;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-10 12:36
 **/
public class demo_2 {

    public static void main(String[] args) {

        Dept d = new Dept();
        d.setName("deptddd");

        Emp emp1 = new Emp();
        emp1.setName("aaaaa");
        emp1.setDept(d);
        Emp emp2 = new Emp();
        emp2.setName("bbbbb");
        emp2.setDept(d);

        d.setEmp(Arrays.asList(emp1, emp2));


        System.out.println(d.toString());


    }


}

class Emp {
    private String name;
    private Dept dept;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", dept=" + dept +
                '}';
    }
}

class Dept {
    private String name;
    private List<Emp> emp;


    public List<Emp> getEmp() {
        return emp;
    }

    public void setEmp(List<Emp> emp) {
        this.emp = emp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "name='" + name + '\'' +
                ", emp=" + emp +
                '}';
    }
}
