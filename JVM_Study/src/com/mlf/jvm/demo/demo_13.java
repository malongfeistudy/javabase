package com.mlf.jvm.demo;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @program: Java基础
 * @description: 内存分配  VM -Options：  -XX:+DisableExplicitCC 禁用显式的垃圾回收
 * @author: mlf
 * @date: 2022-05-13 10:24
 **/
public class demo_13 {

    static int _1GB = 1024 * 1024 * 1024;

    public static void main(String[] args) throws IOException {

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(_1GB);
        System.out.println("分配完毕···");
        System.in.read();
        System.out.println();
        System.out.println("开始释放···");
        byteBuffer = null; //让其指向null 等待gc来回收
        System.gc();  //禁用后无效
        System.in.read();

        //ByteBuffer.allocateDirect(_1GB);
        //ByteBuffer.allocateDirect(_1GB);

    }

}
