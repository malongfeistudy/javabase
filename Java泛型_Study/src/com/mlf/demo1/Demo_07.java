package com.mlf.demo1;

import java.lang.reflect.Array;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-25 16:43
 **/
public class Demo_07 {

    // 如果我们确实需要实例化一个泛型，应该如何做呢？可以通过反射实现：
    static <T> T newTclass(Class<T> clazz) throws InstantiationException, IllegalAccessException {
        T obj = clazz.newInstance();
        return obj;
    }

    static Test test(Class<Test> testClass) throws IllegalAccessException, InstantiationException {
        Test test = testClass.newInstance();
        return test;
    }

}

class Test {

    //我们在使用到泛型数组的场景下应该尽量使用列表集合替换，
    // 此外也可以通过使用 java.lang.reflect.Array.newInstance(Class<T> componentType, int length)
    // 方法来创建一个具有指定类型和维度的数组，如下：
    public static class ArrayWithTypeToken<T> {
        private final T[] array;

        public ArrayWithTypeToken(Class<T> type, int size) {
            array = (T[]) Array.newInstance(type, size);
        }

        public void put(int index, T item) {
            array[index] = item;
        }

        public T get(int index) {
            return array[index];
        }

        public T[] create() {
            return array;
        }
    }

    // 所以使用反射来初始化泛型数组算是优雅实现，因为泛型类型 T在运行时才能被确定下来，
    // 我们能创建泛型数组也必然是在 Java 运行时想办法，而运行时能起作用的技术最好的就是反射了
    public static void main(String[] args) {
        ArrayWithTypeToken<Integer> arrayToken = new ArrayWithTypeToken<>(Integer.class, 100);
        Integer[] array = arrayToken.create();
    }


}