package com.mlf.test;

//约瑟夫问题
public class JosephTest {

    public static void main(String[] args) {
        //1、构建循环链表
        //首结点
        Node<Integer> first=null;
        Node<Integer> pre=null;
        for (int i=1;i<=41;i++){
            if (i==1){
                first=new Node<>(i,null);
                pre=first;
                continue;
            }
            Node<Integer> node = new Node<>(i, null);
            pre.next=node;
            pre=node;//更新pre
            if (i==41){
                pre.next=first;
            }
        }
        //2、遍历循环链表
        int count=0;//计数
        Node n=first;
        Node before=null;//记录当前结点的上一个结点
        while (n!=n.next){//只剩最后一个结点时停止
            count++;
            if (count==3){
                //让前一个结点 指向下一个结点 当前结点kill
                before.next=n.next;
                System.out.print(n.item+",");
                count=0;
            }else {
                //让before成为当前结点n。
                before=n;
            }
            //n++
            n=n.next;
        }
        //输出最后一个n
        System.out.println(n.item);
    }

    //结点类
    private static class Node<T>{
        T item;
        Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

}
