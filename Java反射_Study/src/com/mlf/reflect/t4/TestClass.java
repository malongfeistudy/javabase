package com.mlf.reflect.t4;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-09-20 21:26
 **/
public class TestClass {
    public static void main(String[] args) {
        Class<?> factoryClass = getSpringFactoriesLoaderFactoryClass();
        System.out.println(factoryClass.getName());  // com.mlf.reflect.t4.EnableAutoConfiguration
    }

    public static Class<?> getSpringFactoriesLoaderFactoryClass() {
        return EnableAutoConfiguration.class;
    }
}

@EnableAutoConfiguration
class testConfig {

}

@interface EnableAutoConfiguration {

}
