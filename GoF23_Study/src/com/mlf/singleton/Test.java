package com.mlf.singleton;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-04-25 20:18
 **/
public class Test {

    public static void main(String[] args) {

        System.out.println(LazySingleton1.getSingleton().hashCode());
        System.out.println(LazySingleton1.getSingleton().hashCode());

        System.out.println(LazySingleton2.getSingleton().hashCode());
        System.out.println(LazySingleton2.getSingleton().hashCode());

        System.out.println(EagerSingleton.getEagerSingleton().hashCode());
        System.out.println(EagerSingleton.getEagerSingleton().hashCode());

        System.out.println(DoubleLockSingleton.getDoubleLockSingleton().hashCode());
        System.out.println(DoubleLockSingleton.getDoubleLockSingleton().hashCode());

        System.out.println(InternalSingleton.getSingleton().hashCode());
        System.out.println(InternalSingleton.getSingleton().hashCode());

    }

}
