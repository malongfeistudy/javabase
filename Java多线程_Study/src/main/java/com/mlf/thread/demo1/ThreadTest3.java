package com.mlf.thread.demo1;

public class ThreadTest3 implements Runnable{
    @Override
    public void run() {
        //run方法线程体
        for (int i=0;i<10;i++){
            System.out.println("我正在学习run----"+i);
        }
    }

    public static void main(String[] args) {

        ThreadTest3 threadTest3 = new ThreadTest3();
        new Thread(threadTest3).start();

        //main线程，主线程
        for (int i=0;i<10;i++){
            System.out.println("我在看main代码----"+i);
        }
    }
}
