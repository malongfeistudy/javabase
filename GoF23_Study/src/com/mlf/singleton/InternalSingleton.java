package com.mlf.singleton;

/**
 * @program: Java基础
 * @description:
 * 静态(类级)内部类：这种方式能达到双检锁方式一样的功效，但实现更简单。
 * 对静态域使用延迟初始化，应使用这种方式而不是双检锁方式。
 * * 这种方式只适用于静态域的情况，双检锁方式可在实例域需要延迟初始化时使用。
 * @author: mlf
 * @date: 2022-04-25 22:18
 **/

public class InternalSingleton {

    private InternalSingleton(){}

    /*类级内部类，也就是静态的成员式内部类，该内部类的实例与外部类的实例
      没有绑定关系，而且只有被调用到时才会装载，从而实现了延迟加载
     */
    private static class InternalSingletonHolder {
        //静态初始化器，由JVM来保证线程安全
        private static InternalSingleton internalSingleton = new InternalSingleton();
    }

    public static InternalSingleton getSingleton(){
        return InternalSingletonHolder.internalSingleton;
    }

}
