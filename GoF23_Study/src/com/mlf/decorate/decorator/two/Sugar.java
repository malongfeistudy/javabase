package com.mlf.decorate.decorator.two;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 加入糖配料
 * @author: mlf
 * @date: 2022-06-01 19:02
 **/
public class Sugar extends CondimentDecorator {
    public Sugar(Beverage beverage) {
        super.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost() + 2;
    }
}
