package com.mlf.singleton;

/**
 * @program: Java基础
 * @description: 懒汉式单例（在第一次调用的时候实例化自己）
 *  * 这种方式是最基本的实现方式，这种实现最大的问题就是不支持多线程。因为没有加锁 synchronized，所以严格意义上它并不算单例模式
 * @author: mlf
 * @date: 2022-04-25 20:10
 **/
class LazySingleton1 {

    //构造方法私有化
    private LazySingleton1(){}

    private static LazySingleton1 singleton=null;

    //构造实例化方法
    public static LazySingleton1 getSingleton() {

        if (singleton==null){
            singleton = new  LazySingleton1();
        }

        return singleton;
    }
}


//线程安全
class LazySingleton2 {

    //构造方法私有化
    private LazySingleton2() {
    }

    private static LazySingleton2 singleton = null;

    //构造实例化方法
    public static synchronized LazySingleton2 getSingleton() {

        if (singleton == null) {
            singleton = new LazySingleton2();
        }

        return singleton;
    }
}