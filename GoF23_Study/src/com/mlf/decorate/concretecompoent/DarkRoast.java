package com.mlf.decorate.concretecompoent;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-01 18:11
 **/
public class DarkRoast implements Beverage {
    @Override
    public double cost() {
        return 1;
    }
}
