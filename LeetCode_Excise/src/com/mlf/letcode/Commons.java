package com.mlf.letcode;

import org.junit.Test;

import java.util.Arrays;

public class Commons {

    @Test
    public void test(){
        int[][] ints = generateMatrix1(5);
        for (int[] anInt : ints) {
            System.out.println(Arrays.toString(anInt));
        }
        System.out.println();
        int[][] ints2 = generateMatrix2(5);
        for (int[] anInt : ints2) {
            System.out.println(Arrays.toString(anInt));
        }
    }


    //59. 螺旋矩阵 II
    public int[][] generateMatrix1(int n) {
        int[][] sq = new int[n][n];

        int x = 0, y = 0; //循环每一个圈的起始位置 索引
        int loop = n / 2; //循环几圈的次数
        int mid = n / 2; //矩阵中心 当n为奇数时 中心数需单独输出
        int count = 1; //赋值元素
        int offset = 1; //控制输出的四个边长

        int i, j;//控制行列下标
        while (loop-- > 0) {
            i=x;j=y;

            //1、从左到右
            for (j = y; j < y + n - offset; j++) {
                sq[x][j] = count++;
            }

            //2、从上到下 j为上次赋值最后一个数组元素的j 下同
            for (i = x; i < x + n - offset; i++) {
                sq[i][j] = count++;
            }

            //3、从右到左
            for (; j > y; j--) {
                sq[i][j] = count++;
            }

            //4、从下到上
            for (; i > x; i--) {
                sq[i][j] = count++;
            }
            x ++;
            y ++; //改变起点索引

            offset+=2;

        }

        //给中心元素赋值
        if (n % 2 != 0) {
            sq[mid][mid] = count;
        }

        return sq;
    }
    //59. 螺旋矩阵 II
    public int[][] generateMatrix2(int n) {
        int[][] ans = new int[n][n];
        int index=1;

        // 外层i控制圈数 内层j负责赋值
        for (int i = 0; i < (n+1) / 2; i++) {
            //上
            for (int j = i; j < n - i; j++) {
                if (n/2==i) {//中心数在这
                    ans[i][j]=index;
                    break;
                }
                ans[i][j] = index++;
            }
            //右
            for (int j = i + 1; j < n - i; j++) {
                ans[j][n - i - 1] = index++;
            }
            //下
            for (int j = n - i - 2; j >= i; j--) {
                ans[n - i - 1][j] = index++;
            }
            //左
            for (int j = n - i - 2; j > i; j--) {
                ans[j][i] = index++;
            }
        }
        //System.out.println(index);
        return ans;
    }

}
