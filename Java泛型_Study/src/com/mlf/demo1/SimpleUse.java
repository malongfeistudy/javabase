package com.mlf.demo1;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-22 09:13
 **/
public class SimpleUse {

    public static void main(String[] args) {

        Vehicle<String> car = new Car<>("小汽车");
        System.out.println(car.getInfo());

    }

}


// 定义泛型类接口
interface Vehicle<T> {
    public T getInfo(); //定义抽象方法，抽象方法的返回值就是泛型类型
}

// 实现泛型接口
class Car<T> implements Vehicle<T> {

    private T info;

    public Car(T info) {
        this.info = info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    @Override
    public T getInfo() {
        return this.info;
    }
}


