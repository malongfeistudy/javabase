package com.mlf.datastructure.priority;

/**
 * @program: Java基础
 * @description: 最大优先队列
 * @author: mlf
 * @date: 2022-05-23 17:22
 **/
public class MaxPriorityQueue<T extends Comparable<T>> {

    private T[] item;
    private int N;

    public MaxPriorityQueue(int capacity) {
        this.item = (T[]) new Comparable[capacity + 1];
        this.N = 0;
    }

    // 判断堆中索引i<j
    private boolean less(int i, int j) {
        return item[i].compareTo(item[j]) < 0;
    }

    public int size() {
        return N;
    }

    public boolean isEmpty() {
        return N == 0;
    }

    // 交换i和 j处元素
    private void exch(int i, int j) {
        T temp = item[i];
        item[i] = item[j];
        item[j] = temp;
    }

    // 往队列中插入一个元素
    public void insert(T t) {
        item[++N] = t;
        swim(N);
    }

    // 上浮算法
    private void swim(int k) {

        while (k > 1) {
            if (less(k / 2, k)) exch(k / 2, k);
            k /= 2;
        }

    }

    // 删除最大元素并且返回，相当于出队
    public T delMax() {
        T max = item[1];
        exch(1, N);     //先交换，再移除，最后下沉
        item[N--] = null;
        sink(1);
        return max;
    }

    // 下沉算法
    public void sink(int k) {
        // 比较当前结点与它的两个子结点2*k和2*k+1 小于则下沉 条件：2*k<=N(保证其有子结点)
        int max;
        while (2 * k <= N) {        // 有子结点

            // 比较子结点，与其较大的交换
            if (2 * k + 1 <= N) {  // 有右子结点
                if (less(2 * k, 2 * k + 1)) max = 2 * k + 1;         // 找到较大结点max
                else max = 2 * k;
            } else max = 2 * k;

            if (!less(k, max)) break;        // 比较 若满足则交换

            exch(k, max);

            k = max;
        }
    }

}









