package com.mlf.writen;


import java.util.Arrays;
import java.util.Scanner;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-09-27 15:47
 **/
public class Main {

    /**
     * 小明很喜欢打字，今天小红给了小明一个字符串。
     * <p>
     * 这个字符串只包含大写和小写字母。
     * <p>
     * 我们知道，按下CapsLock键，可以切换大小写模式。
     * <p>
     * 我们在小写模式时候，同时按下shift+字母键，就能写出大写字母。
     * <p>
     * 在大写模式的时候，按下shift+字母键，就能写出小写字母。
     * <p>
     * 现在问题来了，给你一个字符串，问你最少使用多少个按键，就可以写出这个字符串呢？
     * <p>
     * 注意，按shift和字母键，算两次按键。开始时均为小写状态。
     */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        int[] res = new int[t];
        for (int i = 0; i < t; i++) {
            String s = scanner.next();
            //res[i] = leastNums(s);
            res[i] = getCount(s);
        }
        for (int re : res) {
            System.out.println(re);
        }
    }

    private static int getCount(String s) {
        // 键入当前字符后并保持输入状态为小写字母需要small次，保持输入状态为大写字母需要big次。
        // 初始状态下，small = 0, big = 1
        int small = 0, big = 1;
        for (char c : s.toCharArray()) {
            // 当前需要输入大写字符
            if (c >= 'A' && c <= 'Z') {
                int smallTmp = Math.min(small + 3, big + 2);  // smallTmp记录最终转为小写所需的最小次数
                int bigTmp = Math.min(small + 2, big + 1);   // bigTmp记录最终转为大写所需的最小次数
                small = smallTmp;  // 更新
                big = bigTmp;
            } else {
                // 当前需要输入小写字符
                int smallTmp = Math.min(small + 1, big + 2);  // smallTmp记录最终转为小写所需的最小次数
                int bigTmp = Math.min(small + 2, big + 2);   // bigTmp记录最终转为大写所需的最小次数
                small = smallTmp;  // 更新
                big = bigTmp;
            }
        }

        return Math.min(small, big);
    }

    private static int leastNums(String s) {
        int count = 0;
        // 1、首先算入按压每个字符的次数
        // 2、然后需要计算输入过程中的大小写转换次数。
        // 思路：进行遍历字符串，如果首字符为小写，则不转换，继续往后遍历，
        // 小写遇到大写，需要转换；大写遇到小写，需要转换；一直不变则不需要转换。
        // a-z：97-122，A-Z：65-90，0-9：48-57
        boolean flag = false; // 默认FALSE为小写，TRUE表大写
        for (char c : s.toCharArray()) {
            // 1、在小写模式下
            if (flag == false) {
                // 小写字符 只需+1
                if (c >= 'a' && c <= 'z') count += 1;
                // 大写字符 计数且转换+2
                if (c >= 'A' && c <= 'Z') {
                    count += 2;
                    flag = true; // 转换为大写
                }
            } else { // 2、大写模式下
                // 小写字符 只需+1
                if (c >= 'A' && c <= 'Z') count += 1;
                // 小写字符 计数且转换 + 2
                if (c >= 'a' && c <= 'z') {
                    count += 2;
                    flag = false; // 转换为小写
                }
            }
        }
        // BfbsSD  9
        return count;
    }

}

/**
 * 小树在生日那天收到一个特别的礼物，是一个长度为 n 的数列。
 * <p>
 * 这个数列有一个神奇的性质，数列中任意两个奇偶性不同的数的位置可以被交换。
 * <p>
 * 小树想知道经过任意次变换后（或者不变）可以得到的字典序最小的数列是什么。
 * <p>
 * 对于两个数列 a、b，第 i 个数分别为 ai ,bi，
 * a 的字典序小于 b 的字典序当且仅当存在一个位置 k，ak < bk，且当 i 小于 k 时 ai = bi。
 * <p>
 * 输入描述
 * 第一行有一个整数 n（1<=n<=105），代表数列长度。
 * <p>
 * 第二行有 n 个整数a1,a2,...,an（1<=ai<=109），代表数列中的元素。
 * <p>
 * 输出描述
 * 输出 n 个整数，代表能得到的字典序最小的数列。
 * <p>
 * <p>
 * 样例输入
 * 3
 * 4 1 7
 * 样例输出
 * 1 4 7
 */
class Main01 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Integer[] nums = new Integer[n];
        for (int i = 0; i < n; i++) {
            nums[i] = Integer.valueOf(scanner.next());
        }
        Integer[] seq = getSeq(nums);
        for (int i = 0; i < seq.length; i++) {
            if (i != seq.length - 1) System.out.print(seq[i] + " ");
            else System.out.print(seq[i]);
        }
    }

    public static Integer[] getSeq(Integer[] nums) {
        Integer[] res = new Integer[nums.length];
        boolean flag1 = false, flag2 = false;
        for (Integer num : nums) {
            if (flag1 && flag2) break;
            if (num % 2 == 0) {
                flag1 = true;
            } else {
                flag2 = true;
            }
        }
        if (flag1 && flag2) {
            Arrays.sort(nums);
        }

        return nums;
    }

}

/**
 * 小A有一个由 0 到 9 组成的幸运数字串 S ，他每次在游戏里抽卡之前都会用这个串进行占卜。
 * 在占卜时，他会先随机出一个幸运数 k ，然后随机选出数字串 S 中的一些位置并将这些位置上的数按顺序拼起来。
 * 如果最终所得的数字可以被幸运数 k 整除，那么他会觉得自己这次抽卡出货概率很大，就会下血本进行抽卡。
 * 在数次尝试之后，他开始怀疑占卜流程的可靠性。
 * 现在他给了你幸运数 k 和幸运串 S ，他想知道有多少种挑选位置的方法可以使其得出抽中概率大的结论。
 * 即，串 S 中有多少个非空子序列组成的十进制数能够被 k 整除。
 * <p>
 * 第一行有两个正数n,k(2<=n<=1000,1<=k<=10000)，分别代表串 S的长度和幸运数 k 。
 * <p>
 * 第二行有一个长度为 n 的字符串 S ，仅由字符 0 到 9 组成。
 * <p>
 * 输出一个非负整数，代表所求的答案。因为答案可能很大，你只需要输出答案除以1000000007所得的余数。
 * <p>
 * 样例输入
 * 4 3
 * 1423
 * 样例输出
 * 5
 * 提示
 * 12 123 42 423 3为五个满足条件的子序列。
 */
class Main02 {

    // 动态规划 背包求可以被幸运数K整除的数组和
    public static void main(String[] args) {

        String s = "abc";
        boolean b = s.startsWith("ab");
        System.out.println(b);
        s.substring(1);

    }


    public static int getCount(int[] nums, int K) {
        int count = 0, sum = 0;

        for (int num : nums) {
            sum += num;
            if (K / sum == 0) count++;
        }

        return count;
    }

}

class Main03 {
    // n、m、k、x、y
    // 2 2 3
    // 0 0
    // 0 1
    // 1 0
    // res = 3.
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int m = sc.nextInt();
        int k = sc.nextInt();

        int[][] flag = new int[n][m];
        int[] row = new int[n], col = new int[m];

        for (int i = 0; i < k; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();
            flag[x][y] = 1;
            row[x] += 1;
            col[y] += 1;
        }

        int maxRow = 0, maxCol = 0, ans = 0;
        for (int i = 0; i < n; i++) {
            if (row[i] == 0) continue;
            maxRow = Math.max(maxRow, row[i]);
            for (int j = 0; j < m; j++) {
                if (col[j] == 0) continue;
                maxCol = Math.max(maxCol, col[j]);
                if (flag[i][j] != 0) {
                    ans = Math.max(ans, row[i] + col[j]);
                }
            }
        }
        System.out.println(ans - 1);
    }
}


class Solution {
    /**
     * 将一个节点数为 size 链表 m 位置到 n 位置之间的区间反转，要求时间复杂度 O(n)O(n)，空间复杂度 O(1)O(1)。
     * {1,2,3,4,5},2,4
     * {1,4,3,2,5}
     *
     * @param head ListNode类
     * @param m    int整型
     * @param n    int整型
     * @return ListNode类
     */
    public ListNode reverseBetween(ListNode head, int m, int n) {
        if (head.next == null) {
            return head;
        }
        // 先找到开始和结束的指针位置记作 star、end
        ListNode startPre = null, start = head, end = head;
        int max = Math.max(m, n);
        int min = Math.min(m, n);
        // 比如 max 位置为4，只需要向后移动三次即可
        while (--min > 0) {
            startPre = start;
            start = start.next;
        }
        while (--max > 0) {
            end = end.next;
        }

        // 记录当前指针位置，开始反转链表即可
        ListNode cur = start, pre = null, tmp;
        while (cur != end.next) {
            tmp = cur.next;
            cur.next = pre;
            pre = cur;
            cur = tmp;
        }

        // 最终只需将 start 指向 end.next，start.pre 指向 end 即可
        start.next = end.next;
        assert startPre != null;
        startPre.next = end;

        return null;
    }

    /**
     * @param head ListNode类
     * @param k    int整型
     * @return ListNode类
     */
    public ListNode reverseKGroup(ListNode head, int k) {
        ListNode cur = head;
        int count = 0;
        while (cur.next != null) {
            count++;
            // 更新start、end指针
            if (count % k != 0) {

            }


        }


        return head;
    }

}

class ListNode {
    int val;
    ListNode next = null;
}











