package com.mlf.proxy.realfun;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-27 11:40
 **/
public interface Test {

    static void test() {
        System.out.println("我是谁！我是接口的静态方法");
    }

    void test1();

    //Illegal combination of modifiers: 'final' and 'abstract': 非法的组合修饰符
    //final void test2();

}


// 接口的静态方法不能被 继承和重写！
class test implements Test {

    @Override
    public void test1() {

    }

    public static void main(String[] args) {

    }

}