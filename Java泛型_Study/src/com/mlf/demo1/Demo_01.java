package com.mlf.demo1;

/**
 * @program: Java基础
 * @description: 泛型方法
 * @author: mlf
 * @date: 2022-05-22 09:45
 **/
public class Demo_01 implements Comparable<Integer> {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        CreateObj createObj = new CreateObj();
        Demo_01 classObj = createObj.getClassObj(Class.forName("com.mlf.demo1.Demo_01"));
        System.out.println(classObj.getClass());

    }

    @Override
    public int compareTo(Integer o) {
        return 0;
    }
}

class CreateObj {

    public <T extends Object & Comparable<?>> T getClassObj(Class<?> c) throws IllegalAccessException, InstantiationException {
        return (T) c.newInstance();
    }

}