package com.mlf.decorate.decorator.two;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 装饰者抽象类:新增配料功能
 * @author: mlf
 * @date: 2022-06-01 18:12
 **/
public abstract class CondimentDecorator implements Beverage {
    protected Beverage beverage;
}
