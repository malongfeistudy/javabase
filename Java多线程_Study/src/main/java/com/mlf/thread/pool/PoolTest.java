package com.mlf.thread.pool;

import com.mlf.thread.utils.Sleeper;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * @program: Java基础
 * @description: 线程池测试
 * @author: mlf
 * @date: 2022-07-30 17:22
 **/

public class PoolTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool
                = Executors.newFixedThreadPool(3);

        // submit() 提交任务，用返回值 future 获取任务执行结果
        for (int i = 0; i < 30; i++) {
            int j = i + 1;
            Future<String> future = pool.submit(() -> {
                System.out.println("running···" + j);
                Sleeper.sleep(1);
                return "success";
            });
            System.out.println(future.get());
        }
    }


}

class invokeAllTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService pool
                = Executors.newFixedThreadPool(2);
        List<Future<String>> futureList = pool.invokeAll(Arrays.asList(
                () -> {
                    System.out.println("running···");
                    Sleeper.sleep(1);
                    return "1";
                },
                () -> {
                    System.out.println("running···");
                    Sleeper.sleep(1);
                    return "2";
                },
                () -> {
                    System.out.println("running···");
                    Sleeper.sleep(1);
                    return "3";
                },
                () -> {
                    System.out.println("running···");
                    Sleeper.sleep(1);
                    return "4";
                }
        ), 1000, TimeUnit.MILLISECONDS);

        futureList.forEach((f) -> {
            try {
                f.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        //for (Future<String> future : futureList) {
        //    Sleeper.sleep(4);
        //    System.out.println(future.get());
        //}

    }
}

class invokeAnyTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        Object res = pool.invokeAny(Arrays.asList(
                () -> {
                    System.out.println("running···");
                    Thread.sleep(1);
                    return "1";
                }, () -> {
                    System.out.println("running···");
                    Thread.sleep(2);
                    return "2";
                }, () -> {
                    System.out.println("running···");
                    Thread.sleep(3);
                    return "3";
                }), 7000, TimeUnit.MILLISECONDS);

        Thread.sleep(2);
        System.out.println(res);
    }
}

class shutDownTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        List<Runnable> runnableList = null;
        // submit() 提交任务，用返回值 future 获取任务执行结果
        for (int i = 1; i < 4; i++) {
            int j = i;
            Future<String> future = pool.submit(() -> {
                System.out.println("running···" + j);
                Sleeper.sleep(1);
                return "success";
            });
            System.out.println(future.get());
            if (i == 3) {
                //runnableList = pool.shutdownNow();
                pool.shutdown();
                // 线程等待终止时间 6s为总时长

                System.out.println("shutdown不会阻塞main线程，不会等线程池后面线程执行，直接kill");
            }
        }
        //System.out.println("shutdown不会阻塞main线程，不会等线程池后面线程执行，直接kill");
        //for (Runnable runnable : runnableList) {
        //    System.out.println(runnable);
        //}
        //pool.shutdownNow();

        boolean b = pool.awaitTermination(8, TimeUnit.SECONDS);
        System.out.println(b);

    }
}















