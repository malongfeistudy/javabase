package com.mlf.reentrantlock.rw;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-08-02 22:17
 **/
public class RWLockTest {

    public static void main(String[] args) {
        DataContains contains = new DataContains();

        new Thread(() -> {
            //System.out.println("调用读锁");
            //contains.write();
            contains.read();
            contains.write();
        }).start();
    }

}

class DataContains {
    private Object data;
    private ReentrantReadWriteLock rw = new ReentrantReadWriteLock(); // 读写锁
    private ReentrantReadWriteLock.ReadLock r = rw.readLock(); // 读锁
    private ReentrantReadWriteLock.WriteLock w = rw.writeLock(); // 写锁

    // 读操作
    public Object read() {
        r.lock();
        try {
            System.out.println("进入读取锁");
            Thread.sleep(1000);
            //write(); // 不支持锁升级
            return data;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("释放读锁");
            r.unlock();
        }
        return null;
    }

    // 写操作
    public boolean write() {
        w.lock();
        try {
            System.out.println("进入写锁");
            read();  // 支持锁降级
            Thread.sleep(1000);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("释放写锁");
            w.unlock();
        }
        return false;
    }

}



















