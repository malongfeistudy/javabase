package com.mlf.datastructure.heap;

/**
 * @program: Java基础
 * @description: 堆API设计
 * @author: mlf
 * @date: 2022-05-21 16:00
 **/

public class Heap<T extends Comparable<T>> {

    // 存储堆的元素
    private T[] items;
    // 记录堆中元素个数
    private int N;

    public Heap(int capacity) {
        this.items = (T[]) new Comparable[capacity + 1];
        this.N = 0;
    }

    // 判断堆中索引i处的元素是否小于索引j处的元素
    private boolean less(int i, int j) {
        return items[i].compareTo(items[j]) < 0;
    }

    // 交换堆中i索引和j索引处的值
    private void exch(int i, int j) {
        T temp = items[i];
        items[i] = items[j];
        items[j] = temp;
    }

    // 往堆中插入一个元素
    public void insert(T t) {

        items[++N] = t;  // 废弃items[0]
        swim(N);

    }

    // 上浮算法
    private void swim(int k) {
        //1、（循环）比较当前插入的结点元素与父节点
        while (k > 1 && less(k / 2, k)) {  // k>1 条件在前 ，需要先保证k>1 否则 会进入less判断item[0]==null 报空指针异常
            exch(k, k / 2);
            k = k / 2;
        }
    }

    // 删除堆中最大元素并且返回
    public T delMax() {
        T max = items[1];

        // 1、交换堆中最后一个元素和最大元素max
        exch(1, N);
        // 删除max并且元素个数-1
        items[N--] = null;
        // 2、下沉max
        sink(1);

        return max;
    }

    // 下沉算法
    private void sink(int k) {
        // 比较当前结点与它的两个子结点2*k和2*k+1 小于则下沉 条件：2*k<=N(保证其有子结点)
        while (2 * k <= N) {
            // 比较子结点，与其较大的交换
            // 找到较大结点max
            int max;
            if (2 * k + 1 <= N) {  // 有右子结点
                if (less(2 * k + 1, 2 * k)) max = 2 * k;
                else max = 2 * k + 1;
            } else max = 2 * k;

            // 比较 若满足则交换
            if (!less(k, max)) break;

            exch(k, max);

            //更新k值
            k = max;
        }
    }

}
