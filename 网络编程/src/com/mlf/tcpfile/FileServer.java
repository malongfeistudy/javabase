package com.mlf.tcpfile;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @program: Java基础
 * @description: 服务端程序
 * @author: mlf
 * @date: 2022-05-11 22:52
 **/
public class FileServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(10001);
        while (true) {
            Socket s = serverSocket.accept();
            new Thread(new ServerThread(s)).start();
        }
    }
}


class ServerThread implements Runnable {

    private Socket socket;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        String ip = socket.getInetAddress().getHostAddress(); //获取客户端IP地址
        int count = 1;
        try {
            InputStream in = socket.getInputStream();
            //父目录
            File imgFile = new File("A:\\Java开发学习\\Learning\\Java基础\\网络编程\\img");
            if (!imgFile.exists()) {
                imgFile.mkdir();
            }

            //子文件
            File file = new File(imgFile, ip + "(" + count + ").jpg");
            if (file.exists()) {
                file = new File(imgFile, ip + "(" + (count++) + ").jpg");  //如果存在文件则count++表示该IP发送的第二个图片
            }
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) != -1) {  //循环读取数据
                fos.write(buf, 0, len);   //存入服务端
            }

            //响应客户端
            OutputStream out = socket.getOutputStream();
            out.write("上传成功".getBytes());
            fos.close();
            socket.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}




