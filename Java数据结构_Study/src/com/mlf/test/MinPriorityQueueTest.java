package com.mlf.test;

import com.mlf.datastructure.priority.MinPriorityQueue;

/**
 * @program: Java基础
 * @description: 最xiao优先队列测试
 * @author: mlf
 * @date: 2022-05-23 17:22
 **/
public class MinPriorityQueueTest<T extends Comparable<T>> {

    public static void main(String[] args) {

        MinPriorityQueue<String> queue = new MinPriorityQueue<>(10);


        queue.insert("B");
        queue.insert("Z");
        queue.insert("X");
        queue.insert("Y");
        queue.insert("C");
        queue.insert("H");
        queue.insert("T");
        queue.insert("H");

        while (!queue.isEmpty()) {
            String s = queue.delMin();
            System.out.print(s + " ");
        }

    }

}









