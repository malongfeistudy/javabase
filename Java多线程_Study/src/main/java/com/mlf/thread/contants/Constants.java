package com.mlf.thread.contants;

/**
 * @program: Java基础
 * @description: 常量类：标志线程是否为阻塞状态
 * @author: mlf
 * @date: 2022-11-04 12:22
 **/
public class Constants {

    public static class WaitStatus {
        public static boolean flag = false;

        public WaitStatus(boolean flag) {
            WaitStatus.flag = flag;
        }

        public boolean getFlag() {
            return flag;
        }

        public void setFlag(boolean flag) {
            WaitStatus.flag = flag;
        }
    }

    public static enum WaitOrNoWait {
        /**
         * 阻塞状态为true
         */
        WAIT(true, "阻塞"),
        /**
         * 非阻塞状态为FALSE
         */
        NO_WAIT(false, "非阻塞");
        private boolean flag;
        private String msg;

        public boolean getFlag() {
            return flag;
        }

        public void setFlag(boolean flag) {
            this.flag = flag;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        WaitOrNoWait(boolean flag, String msg) {
            this.flag = flag;
            this.msg = msg;
        }
    }

}
