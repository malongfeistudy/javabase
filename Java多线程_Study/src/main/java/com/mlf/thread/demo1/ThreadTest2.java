package com.mlf.thread.demo1;

import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ThreadTest2 implements Runnable{

    private final String url;
    private final String name;

    public ThreadTest2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        WebDownloader webDownloader=new WebDownloader();
        try {
            webDownloader.downloader(url,name);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
        System.out.println("下载的文件名为："+name);
    }


    public static void main(String[] args) {
        ThreadTest2 t1 = new ThreadTest2("https://www.cnblogs.com/images/aggsite/myblog.svg",
                "myblog.svg");
        ThreadTest2 t2 = new ThreadTest2("https://www.cnblogs.com/images/aggsite/newpost.svg",
                "newpost.svg");
        ThreadTest2 t3 = new ThreadTest2("https://www.cnblogs.com/images/aggsite/search.svg",
                "search.svg");
        new Thread(t1).start();
        new Thread(t2).start();
        new Thread(t3).start();
    }
}

class WebDownloader{
    public void downloader(String url,String name) throws IOException {
        FileUtils.copyURLToFile(new URL(url),new File(name));
    }
}
