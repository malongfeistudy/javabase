package com.mlf.jvm.demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-12 20:44
 **/
public class demo_11 {

    static final String FROM = "A:\\视频教程\\【狂神说Java】JavaWeb入门到实战\\javaweb-40：邮件发送原理及实现_高清 1080P.mp4";
    static final String TO = "A:\\视频教程\\test.mp4";
    static final int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        io();  //普通IO字节流  603.4046  558.9288
        directBuffer(); //直接内存缓冲区字节流读取  293.491   302.6445
    }

    private static void directBuffer() {

        long start = System.nanoTime();
        //获取输入输出流通道
        try (FileChannel from = new FileInputStream(FROM).getChannel();
             FileChannel to = new FileOutputStream(TO).getChannel();
        ) {
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(_1MB); //设置缓冲区容量大小
            while (true) {
                int len = from.read(byteBuffer);
                if (len == -1) {
                    break;
                }
                byteBuffer.flip();
                to.write(byteBuffer);
                byteBuffer.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        long end = System.nanoTime();

        System.out.println("directBuffer用时：" + (end - start) / 1000_000.0);

    }

    private static void io() {

        long start = System.nanoTime();
        //获取输入输出流通道
        try (FileInputStream from = new FileInputStream(FROM);
             FileOutputStream to = new FileOutputStream(TO);
        ) {
            byte[] buf = new byte[_1MB];//设置缓冲区容量大小
            while (true) {
                int len = from.read(buf);
                if (len == -1) {
                    break;
                }
                to.write(buf, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        long end = System.nanoTime();

        System.out.println("IO用时：" + (end - start) / 1000_000.0);

    }

}