package com.mlf.demo1.finaltest;

/**
 * @program: Java基础
 * @description: 用户线程和守护线程：
 * 守护线程：JVM执行完当前线程会自动停止  用户线程：JVM会等待用户自定义线程执行结束后再关闭。
 * @author: mlf
 * @date: 2022-07-18 16:29
 **/
public class Test1 {

    public static void main(String[] args) {

        Thread thread = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "：" + Thread.currentThread().isDaemon());
            while (true) {

            }
        });
        thread.setDaemon(true); // 设置为守护线程
        thread.start();

        System.out.println(Thread.currentThread().getName() + "over");

    }
}
