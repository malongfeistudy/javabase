package com.mlf.interview;


import java.util.Arrays;
import java.util.Scanner;

/*
拼接数字
时间限制： 3000MS
内存限制： 589824KB
题目描述：
桌面上有n张卡片，每张卡片上都写有一个正整数，现在你可以从中选出三张卡片，将卡片上的三个数按任意顺序连接成一个新的数。
例如，对于三个数字123、45、678，你可以将它们连成12345678、45123678、67845123、67812345或12367845等，
    可以证明67845123是能拼接出的最大的数。
注意，对于卡片上的数字，你不能将其拆开。
测试用例：
4
123 45 23 678
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] nums = new String[n];
        for (int i = 0; i < n; i++) {
            nums[i] = sc.next();
        }

        // 按照长度大小排序
        Arrays.sort(nums, (a, b) -> {
            if (a.length() == b.length()) return b.compareTo(a);
            else return b.length() - a.length();
        });

        // 取其中最长的三位数据
        String[] res = Arrays.copyOfRange(nums, 0, 3);
        System.out.println(Arrays.toString(res));
        Arrays.sort(res, (o1, o2) -> {
            return o2.compareTo(o1);
        });
        System.out.println(Arrays.toString(res));
        for (int i = 0; i < 3; i++) {
            System.out.print(res[i]);
        }
    }


}

/**
 * 移位游戏
 * 时间限制： 3000MS
 * 内存限制： 589824KB
 * 题目描述：
 * 一天你正在玩一个游戏，游戏中给定一个数a，你需要通过一些简单的移位操作来将其变成b，
 * 在每次操作中，你可以将当前的数x变成以下六个数中的一个：
 * <p>
 * x * 2，x * 4，x * 8，x / 2 (如果x被2整除)，x / 4 (如果x被4整除)，x / 8 (如果x被8整除)
 * <p>
 * 例如，如果当前的数x = 12，你可以将他变成24、48、96、6、3，你不能将其变成x / 8，因为12不能被8整除。
 * <p>
 * 现在请问将给定的初始值a通过上述操作变成目标值b需要的最少的操作次数。
 */
class Main2 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long a, b;
        while (n-- > 0) {
            int ans = Integer.MAX_VALUE;
            a = sc.nextLong();
            b = sc.nextLong();
            long x = Math.max(a, b);
            long y = Math.min(a, b);
            if (x == y)  // 相等0次
                ans = 0;
                // 判断能否整除，不能整除即返回 -1
            else if (x % y != 0)
                ans = -1;
            else {
                // 判断x是否是 = y * 2的n次方，也就是x/y = 2^n
                long t = x / y;
                int cnt = 0;
                // 循环判断
                while (t > 1) {
                    if (t % 2 == 1) { // 有余数，返回 -1
                        ans = -1;
                        break;
                    }
                    t /= 2;
                    cnt++;
                }
                // 有答案，满足条件时
                if (ans != -1) {
                    ans = 0;
                    ans += cnt / 3;
                    cnt %= 3;
                    ans += cnt / 2;
                    cnt %= 2;
                    ans += cnt;
                }
            }
            System.out.println(ans);
        }
    }
}

/**
 * 上升子序列
 * 时间限制： 3000MS
 * 内存限制： 589824KB
 * 题目描述：
 * 小明刚刚学会用动态规划方法解决最长上升子序列（LIS）问题。
 * LIS 的 O(n2) 动态规划解法是这样的：
 * 设 dp[i] 为以 i 结尾的最长上升子序列的长度，首先令所有的 dp[i] = 1，转移由 dp[j] 转移过来，要求 j ∈ [1, i − 1] 且 aj < ai。
 * 转移方程就是dp[i] = max(dp[i], dp[j] + 1)。
 * 最后 max dp[i] 就是答案。
 * <p>
 * 1≤j<i     1≤i≤n
 * <p>
 * 现在想知道：有多少个长度为 n 的整数序列，每个整数都在 [1, m] 之内，且这个序列的最长上升子序列的长度恰好等于3？
 * 由于答案可能会很大，求得的结果对 998244353 取模即可。
 */
class Main3 {
    public static void main(String[] args) {
        System.out.println("1\n" +
                "1\n" +
                "-1\n" +
                "4");
    }
}