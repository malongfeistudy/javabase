package com.mlf.test;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-10 11:04
 **/
public class BackPack01 {

    static int N = 6;//物品有五件
    static int W = 21;//背包容量为20
    static int[] weight = {0, 2, 3, 4, 5, 9};//重量 2 3 4 5 9
    static int[] value = {0, 3, 4, 5, 8, 10};//价值3 4 5 8 10

    public static void getValue() {
        int[][] sum = new int[N][W];//sum[i][j]意思是：背包容量为j时，在前i件物品中取小于等于i件物品，此时取得的物品的价值最大

        for (int i = 1; i < N; i++) {
            for (int j = 1; j < W; j++) {
                if (weight[i] > j) {//太重了，拿不了
                    sum[i][j] = sum[i - 1][j];
                } else {
                    sum[i][j] = Math.max(sum[i - 1][j - weight[i]] + value[i], sum[i - 1][j]);
                }
            }
        }

        System.out.println(sum[5][20]);
    }

    public static void getValue1() {
        int[] sum = new int[W];

        for (int i = 1; i < N; i++) {
            for (int j = W - 1; j >= 1; j--) {
                if (weight[i] <= j) {
                    sum[j] = Math.max(sum[j], sum[j - weight[i]] + value[i]);
                }
            }
        }

        System.out.println(sum[20]);
    }

    public static void main(String[] args) {
        getValue();
    }
}
