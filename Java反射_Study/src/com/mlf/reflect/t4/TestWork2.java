package com.mlf.reflect.t4;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * @program: Java基础
 * @description: File测试2
 * @author: mlf
 * @date: 2022-05-27 21:07
 **/
public class TestWork2 {

    public static void main(String[] args) throws Exception {
        Class<?> cls = Class.forName("com.mlf.reflect.t4.FileClass");
        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
        }
        FileClass file = (FileClass) cls.newInstance();
        Method creatFile = cls.getMethod("creatFile", String.class);
        creatFile.invoke(file, "test.txt");

    }

}

class FileClass {
    public FileClass() {
    }

    public void creatFile(String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            //file.mkdir();
            file.createNewFile();
        }
    }
}
