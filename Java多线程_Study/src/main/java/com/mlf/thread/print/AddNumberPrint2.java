package com.mlf.thread.print;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: Java基础
 * @description: 交替打印
 * @author: mlf
 * @date: 2022-11-02 13:48
 **/
public class AddNumberPrint2 {

    private final static Lock LOCK = new ReentrantLock(false);
    /**
     * Condition是一个 LOCK 实例出来的，他们获取的都是一个 LOCK 的锁，
     * 而如果要调用 object的 wait和notify 方法，首先要获取对应的object的锁
     * 如果要调用Condition 的await、signal方法，必须先获取Lock锁（Lock.lock）。
     * <p>
     * 相当于实例化出来不同线程的条件变量（休息室），对应的条件可以去操作属于他的线程（await、signal）
     **/
    private final static Condition A_C = LOCK.newCondition();
    private final static Condition B_C = LOCK.newCondition();
    private final static Condition C_C = LOCK.newCondition();
    private static final AtomicInteger VAR = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {

        /**
         * 参数：
         * int corePoolSize,                     核心线程数
         * int maximumPoolSize,                  最大线程数
         * long keepAliveTime,                   救急存活时间
         * TimeUnit unit,                        单时间位
         * BlockingQueue<Runnable> workQueue,    阻塞队列
         * ThreadFactory threadFactory,          线程工厂
         * RejectedExecutionHandler handler      拒绝策略
         */
        // 使用阿里巴巴推荐的创建线程池的方式
        // 通过ThreadPoolExecutor构造函数自定义参数创建
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                3,
                5,
                1,
                TimeUnit.DAYS,
                new ArrayBlockingQueue<>(2),
                new ThreadPoolExecutor.AbortPolicy());

        // 解释：
        // C线程启动后首先将自己阻塞，由(等待)A唤醒它
        // B线程启动后首先将自己阻塞，由(等待)C唤醒它
        // A线程启动后首先将自己阻塞，由(等待)B唤醒它
        executor.execute(new NumThread("A", A_C, B_C, VAR));
        executor.execute(new NumThread("B", B_C, C_C, VAR));
        executor.execute(new NumThread("C", C_C, A_C, VAR));

        Thread.sleep(1000);

        // 纯手动，需要手动加锁、解锁处理。可定制化程度较高，相比于synchronized重量级锁来说
        // 可以使用条件变量操作线程阻塞和唤醒，并且可以实现自定义等待时长。
        LOCK.lock();
        try {
            // 调用condition的 signal方法可以将等待队列中等待时间最长的节点移动到同步队列中
            // 唤醒C_C休息室中的线程C，打破僵局
            A_C.signal();
        } finally {
            LOCK.unlock();
        }

    }

    static class NumThread implements Runnable {
        private final String name;
        private final Condition next;
        private final Condition waitFor;
        private final AtomicInteger var;

        /**
         * 初始化时候需要传入两个对象：一个是当前对象，另一个是需要阻塞的对象。从而实现进程循环执行
         * 注意：使用 wait、notify 的时候需要加 synchronized 锁。
         */
        public NumThread(String name, Condition next, Condition waitFor, AtomicInteger var) {
            this.name = name;
            this.next = next;
            this.waitFor = waitFor;
            this.var = var;
        }

        @Override
        public void run() {
            while (true) {
                // await，需要
                LOCK.lock();
                try {
                    waitFor.await(); // 将当前线程
                    System.out.println(name + " : " + var.getAndIncrement());
                    next.signal();   // 唤醒C
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    LOCK.unlock();
                }
            }
        }
    }

}

