package com.mlf.oop.demo01;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-11 14:06
 **/
public interface USB {

    void turnOn();  //启动

    void turnOff(); //关闭

}
