package com.mlf.thread.demo_wait_notify;

import com.mlf.thread.contants.Constants;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description: 测试唤醒丢失和唤醒假唤醒
 * @author: mlf
 * @date: 2022-11-04 10:44
 **/
public class WaitNotifyTest {

    private final static Object LOCK1 = new Object();
    private final static Object LOCK2 = new Object();
    private final static Constants.WaitStatus FLAG = new Constants.WaitStatus(false);

    public static void main(String[] args) throws InterruptedException {

        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 5, 1, TimeUnit.DAYS,
                new ArrayBlockingQueue<>(4), new ThreadPoolExecutor.AbortPolicy());

        // 线程一使用LOCK1对象调用wait方法阻塞自己
        executor.execute(new ThreadTest("线程一", LOCK1, LOCK2, FLAG));

        System.out.println("main睡一会，让线程一先执行wait方法进入阻塞状态");
        Thread.sleep(1000);
        System.out.println("main醒来了，线程一执行完wait方法进入阻塞状态");
        // TODO 唤醒丢失问题：notify在wait之前执行，
        // 调用LOCK1对象的notify方法，可能主线程已经执行完了，上面线程还没创建完成，也就是没有进入wait状态

        synchronized (LOCK1) {
            while (true) {
                if (FLAG.getFlag()) {
                    System.out.println("main马上执行notify方法让线程一醒过来" + "flag = " + FLAG.getFlag());
                    LOCK1.notify();
                    // 将标志位变为FALSE
                    FLAG.setFlag(Constants.WaitOrNoWait.NO_WAIT.getFlag());
                    System.out.println("main执行notify方法完毕" + "flag = " + FLAG.getFlag());
                    break;
                }
            }
        }


        // TODO 假唤醒问题 :假唤醒就是我们的notify由于操作系统CPU调度的原因没有起效，因此可以尝试自旋锁 上面已经实现while循环


    }


}


class ThreadTest implements Runnable {

    private final String name;

    private final Object waitName;

    private final Object NotifyName;

    private final Constants.WaitStatus flag;

    public ThreadTest(String name, Object waitName, Object notifyName, Constants.WaitStatus flag) {
        this.name = name;
        this.waitName = waitName;
        this.NotifyName = notifyName;
        this.flag = flag;
    }

    @Override
    public void run() {

        synchronized (waitName) {
            // 多个线程执行时，防止notifyAll全部唤醒之后就结束运行，我们的需求是只能唤醒一个线程，当其他线程被唤醒之后需要重新判断标志位是否为FALSE
            while (!flag.getFlag()) {
                try {
                    // 将标志位设置为TRUE
                    flag.setFlag(Constants.WaitOrNoWait.WAIT.getFlag());
                    System.out.println("name；" + name + " 我睡着了进入阻塞状态" + "flag = " + flag.getFlag());
                    waitName.wait();
                    System.out.println("name；" + name + " 我醒来了" + "flag = " + flag.getFlag());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}





