package com.mlf.thread.obj;

import com.mlf.thread.utils.Sleeper;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

/**
 * @program: Java基础
 * @description: 测试  模式 之 邮寄案例
 * @author: mlf
 * @date: 2022-07-25 18:19
 **/

public class TestModelWithMail {

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new People().start();
        }

        Sleeper.sleep(2);

        for (Integer id : Mailboxes.getIds()) {
            new Postman(id, "内容" + id).start();
        }
    }

}

@Log
class People extends Thread {
    @SneakyThrows
    public void run() {
        // 收信
        GuardObject guardObject = Mailboxes.createGuardObject();

        log.info("开始收信 id： " + guardObject.getId());
        Object mail = guardObject.getResp(3000);
        log.info("收到信id：" + guardObject.getId() + "内容：" + mail);
    }
}

@Log
class Postman extends Thread {
    private int id;
    private String mail;

    public Postman(int id, String mail) {
        this.id = id;
        this.mail = mail;
    }

    @Override
    public void run() {
        GuardObject guardObject = Mailboxes.getGuardObject(id);
        log.info("送信id：" + guardObject.getId() + "内容：" + mail);
        guardObject.setResp(mail);
    }
}

// 工具类：解耦
class Mailboxes {
    private static Map<Integer, GuardObject> boxes = new Hashtable<>();

    private static int id = 1;

    // 产生唯一id
    private static synchronized int generateId() {
        return id++;
    }

    public static GuardObject createGuardObject() {
        GuardObject guardObject = new GuardObject(generateId());
        boxes.put(guardObject.getId(), guardObject);
        return guardObject;
    }

    public static Set<Integer> getIds() {
        return boxes.keySet();
    }

    public static GuardObject getGuardObject(int id) {
        return boxes.get(id);
    }
}

// 工具类：等待和生产
class GuardObject {
    private final int id;

    public GuardObject(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // 结果
    private Object response;

    // 获取结果
    public Object getResp(long millis) throws InterruptedException {
        synchronized (this) {
            long base = System.currentTimeMillis(); // 开始时间
            long passedTime = 0; // 经历时间
            while (isAlive()) {
                long delay = millis - passedTime; // 更新 实时剩余延迟时间
                if (delay <= 0) {
                    break;
                }
                this.wait(delay);
                passedTime = System.currentTimeMillis() - base; // 经历时间=求差值
            }
        }
        return response;
    }

    // 产生结果
    public void setResp(Object response) {
        synchronized (this) {
            this.response = response;
            this.notifyAll();
        }
    }

    // 判断是否满足条件
    public boolean isAlive() {
        return response == null;
    }
}
