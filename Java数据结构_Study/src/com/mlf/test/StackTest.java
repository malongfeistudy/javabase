package com.mlf.test;

import com.mlf.datastructure.lineartable.Stack;

import java.util.Iterator;

/**
 * @program: Java基础
 * @description: 栈测试
 * @author: mlf
 * @date: 2022-05-26 16:40
 **/
public class StackTest {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");
        stack.push("E");
        System.out.println(stack.isEmpty());
        System.out.println("foreach遍历");
        for (String s : stack) {
            System.out.println(s);
        }
        System.out.println("迭代器遍历");
        Iterator<String> iterator = stack.iterator();
        while (iterator.hasNext()) {
            String s = iterator.next();
            System.out.println(s);
            stack.pop();
        }
        System.out.println(stack.isEmpty());
    }

}
