package com.mlf.demo1;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-25 15:00
 **/
public class GenericsDemo30 {
    public static void main(String[] args) {
        Integer[] i = fun1(1, 2, 3, 4, 5, 6);   // 返回泛型数组
        fun2(i);
    }

    @SafeVarargs
    public static <T> T[] fun1(T... arg) {  // 接收可变参数
        return arg;            // 返回泛型数组
    }

    public static <T> void fun2(T[] param) {   // 输出
        System.out.print("接收泛型数组：");
        for (T t : param) {
            System.out.print(t + "、");
        }
    }

    @Test
    public void test() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<String> list1 = new ArrayList<String>();
        list1.add("abc");

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(123);

        //list1.add(123); 编译报错报错
        //通过反射添加其它类型元素，不会报错，反射获取该类、调用add方法
        // 说明在编译期间进行了泛型擦除，只保留了原始类型（有限定使用其限定类型（无限定的变量用Object）替换）
        list1.getClass().getMethod("add", Object.class).invoke(list1, 123);

        System.out.println(list1.getClass() == list2.getClass()); // true

    }


}
