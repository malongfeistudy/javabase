package com.mlf.enum1;

/**
 * @program: Java基础
 * @description: \
 * @author: mlf
 * @date: 2022-06-05 11:16
 **/
public class EnumTest {

    public static void main(String[] args) {
        MyEnum e = MyEnum.A;
        e.info();
        System.out.println();
        System.out.println(e.toString());
        System.out.println(e.name());
        System.out.println(e.ordinal());
        System.out.println(e.compareTo(MyEnum.B));
        System.out.println(e.compareTo(MyEnum.A));
    }

}


