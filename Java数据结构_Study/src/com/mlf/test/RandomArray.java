package com.mlf.test;

public class RandomArray {

    public static void main(String[] args) {
        int[] nums=new int[10];
        int target=1;
        for (int i=0;i<10;i++){
            nums[i]=i;
        }
        search(nums, target);
        //search1(nums,target);
    }


    //704、二分查找
    private static int search(int[] nums, int target) {
        // 避免当 target 小于nums[0] 大于nums[nums.length - 1]时多次循环运算
        if (target < nums[0] || target > nums[nums.length - 1]) {
            return -1;
        }
        int l=0;
        int r=nums.length;  //左闭右开[l,r)
        // 因为left == right的时候，在[left, right)是无效的空间，所以使用 <
        while(l<r){
            int mid=l+((r-l)>>1);
            //target在左区间 在[left，right)中
            if(nums[mid]>target) r=mid;
                // target在右区间，在[mid+1，right)中
            else if(nums[mid]<target) l=mid+1;
                // nums[mid]=target，返回下标
            else return mid;
            System.out.print("nums.length: " + nums.length+" ");
            System.out.print("mid: "+mid+" ");
            System.out.print("r: "+r+" ");
            System.out.println("l: "+l+" ");
        }
        return -1;
    }
    //704、二分查找
    private static int search1(int[] nums, int target) {
        // 避免当 target 小于nums[0] 大于nums[nums.length - 1]时多次循环运算
        if (target < nums[0] || target > nums[nums.length - 1]) {
            return -1;
        }
        int l=0;
        int r=nums.length-1; //左闭右闭[l,r]
        while(l<=r){//有可能取值到l=r
            // 中间值
            int mid = l + ((r-l)>>1);
            // target==nums[mid] 刚好找到
            if(target==nums[mid]) return mid;
                // target在右半区间 闭区间跳过边界值 所以l=mid+1
            else if(target<nums[mid]) r=mid-1;
                // target在左半区间 同理
            else l=mid+1;
            System.out.print("nums.length:" + nums.length+" ");
            System.out.print("mid: "+mid+" ");
            System.out.print("r: "+r+" ");
            System.out.println("l: "+l+" ");
        }
        return -1;
    }
}
