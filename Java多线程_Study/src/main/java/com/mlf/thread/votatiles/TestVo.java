package com.mlf.thread.votatiles;

import com.mlf.thread.utils.Sleeper;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: Java基础
 * @description: 测试 volatile
 * @author: mlf
 * @date: 2022-07-27 15:31
 **/

public class TestVo {
    // volatile 不能解决原子性问题
    // 使用该变量时，会从主存去读取，但是在多线程操作同一volatile变量还是不安全的
    // 因为他不能保证原子性（除非原子性操作）、底层字节码指令可以分为好多条
    // 比如 i++ 就是四条指令：getstatic、icount_1、iadd、putstatic
    private static final Object lock = new Object();

    // 不可多线程 进行修改 只可以用在单线程修改、多线程读取操作
    private volatile static int num = 0;

    // AtomicInteger具有原子性、可见性、有序性
    //private static final AtomicInteger num = new AtomicInteger();

    // 可重入锁有 synchronized、ReentrantLock
    private static final ReentrantLock REENTRANT_LOCK = new ReentrantLock();

    public static void main(String[] args) {

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                REENTRANT_LOCK.lock();
                //num.set(num.get() + 1);
                num++;
                REENTRANT_LOCK.unlock();
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                REENTRANT_LOCK.lock();
                //num.set(num.get() + 1);
                num++;
                REENTRANT_LOCK.unlock();
            }
        }).start();

        Sleeper.sleep(1);

        System.out.println(num);
    }


}
