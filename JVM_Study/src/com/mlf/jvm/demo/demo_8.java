package com.mlf.jvm.demo;

import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * @program: Java基础
 * @description: StringTable调优  设置底层的HashMap桶的个数
 * VM options:-Xms500m -Xmx500m -XX:+PrintStringTableStatistics -XX:StringTableSize=100
 * @author: mlf
 * @date: 2022-05-10 21:20
 **/
public class demo_8 {

    //StringTable statistics: 串池底层数据结构为HashMap实现 默认桶子数量为60013  执行时长：253ms
    //Number of buckets       :     60013 =    480104 bytes, avg   8.000
    //Number of entries       :    401777 =   9642648 bytes, avg  24.000
    //Number of literals      :    401777 =  41778408 bytes, avg 103.984

    //设置大小后 -XX:StringTableSize=1009 规定最小设置  执行时长：5032ms
    //StringTable statistics:
    //Number of buckets       :      1009 =      8072 bytes, avg   8.000
    //Number of entries       :    403171 =   9676104 bytes, avg  24.000
    //Number of literals      :    403171 =  41879680 bytes, avg 103.876

    public static void main(String[] args) throws IOException {

        //输入流读取文件file.txt
        try (BufferedReader reader =
                     new BufferedReader(
                             new InputStreamReader(
                                     new FileInputStream("JVM_Study/file.txt"),
                                     StandardCharsets.UTF_8))) {

            String line = null;
            long start = System.nanoTime();
            while (true) {
                line = reader.readLine();  //读取每一行内容
                if (line == null) {
                    break;
                }
                line.intern();  //写入串池
            }
            System.out.println("cost:" + (System.nanoTime() - start) / 1000000 + "ms");
        }

    }


    //生成400000万行uuid文本
    @Test
    public void test() {
        File file = new File("A:\\Java开发学习\\Learning\\Java基础\\JVM_Study\\fileComm.txt");

        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileWriter fw = null;
        try {
            fw = new FileWriter(file, true);
            for (int i = 0; i < 400000; i++) {
                fw.write(UUID.randomUUID().toString().replace("-", "").toLowerCase() + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}










