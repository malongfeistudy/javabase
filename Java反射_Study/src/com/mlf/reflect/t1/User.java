package com.mlf.reflect.t1;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-21 23:15
 **/

public class User {
    public int id;
    public String addr;
    private Integer age;
    private String name;

    public User() {
        super();
    }


    /**
     * 私有构造
     *
     * @param age
     * @param name
     */
    public User(Integer age, String name) {
        super();
        this.age = age;
        this.name = name;
    }


    private User(String name) {
        super();
        this.name = name;
    }

    public User(Integer age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
