package com.mlf.thread.demo1;

public class Race implements Runnable{
    private static String winner;

    @Override
    public void run() {
        int i1 = Thread.activeCount();
        System.out.println(i1);
        for (int i=0;i<=10;i++){
                try {
                    Thread.sleep(10,0); //毫秒 纳秒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            boolean gameOver = isGameOver(i);
            if (gameOver) break;
            System.out.println(Thread.currentThread().getName()+"跑了"+i+"步");
        }


    }

    private boolean isGameOver(int step) {
        if (winner!=null) return true;
        else if (step==10){
            winner=Thread.currentThread().getName();
            System.out.println("winner is "+winner);
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Race race = new Race();
        new Thread(race,"rabbit").start();
        new Thread(race,"tortoise").start();
    }
}
