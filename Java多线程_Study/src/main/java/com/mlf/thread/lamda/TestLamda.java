package com.mlf.thread.lamda;

public class TestLamda {

    //3、静态内部类
    static class MyClass2 implements MyFace{
        @Override
        public void myFunction(int a) {
            System.out.println("我是实现类静态内部类！！！"+getClass().getName()+"实现了"+a+"方法。。");
        }
    }

    public static void main(String[] args) {
        //4、局部内部类
        class MyClass3 implements MyFace{
            @Override
            public void myFunction(int a) {
                System.out.println("我是实现类局部内部类！！！"+getClass().getName()+"实现了"+a+"方法。。");
            }
        }

        MyClass1 myClass = new MyClass1();
        myClass.myFunction(1111);

        MyClass2 myClass1 = new MyClass2();
        myClass1.myFunction(2222);

        MyClass3 myClass2 = new MyClass3();
        myClass2.myFunction(3333);

        //5、匿名内部类
        MyFace myClass4 = new MyFace() {

            @Override
            public void myFunction(int a) {
                System.out.println("我是实现类匿名内部类！！！" + getClass().getName() + "实现了" + a + "方法。。");
            }
        };
        myClass4.myFunction(4444);

        //6、lambda表达式
        MyFace face= (int a)->{
            System.out.println("我是实现类lambda！！！"+"实现了"+a+"方法。。");
        };
        face.myFunction(5555);

        //6.1、lambda表达式 简化参数类型
        MyFace face1= a->{
            System.out.println("我是实现类lambda！！！"+"实现了"+a+"方法。。");
        };
        face1.myFunction(6666);

        //6.2、lambda表达式 简化大括号 注意：只有在一行代码时能去掉
        MyFace face3= a-> System.out.println("我是实现类lambda！！！"+"实现了"+a+"方法。。");
        face3.myFunction(7777);
    }

}

//1、函数式接口：接口只有一个方法
interface MyFace{
    void myFunction(int a);
}

//2、实现类
class MyClass1 implements MyFace{
    @Override
    public void myFunction(int a) {
        System.out.println("我是实现类！！！"+getClass().getName()+"实现了"+a+"方法。。");
    }
}