package com.mlf.jvm.gc;

import java.util.ArrayList;

/**
 * @program: Java基础
 * @description: 四种引用类型*(有五种) 强、软、弱、虚、终结器  生存时间：JVM 停止运行时终止
 * @author: mlf
 * @date: 2022-05-13 22:00
 **/
public class demo_1 {

    //演示强引用：
    // 强引用：root对象、当前线程直接引用的对象、对象的一般状态
    // 如果要对强引用进行垃圾回收，需要设置强引用对象为 null ,或者让其超出对象的生命周期范围，则认为改对象不存在引用。
    public static void main(String[] args) {

        ArrayList<Object> list = new ArrayList<>();
        list.add("fda");
        list.clear();
        // 可以看下 ArrayList 是如何进行内存释放的
        /*public void clear() {
            modCount++;
            // clear to let GC do its work
            for (int i = 0; i < size; i++)
                elementData[i] = null;
            size = 0;
        }*/


    }

}
