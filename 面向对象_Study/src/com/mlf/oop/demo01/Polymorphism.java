package com.mlf.oop.demo01;

/**
 * @program: Java基础
 * @description: 多态测试
 * @author: mlf
 * @date: 2022-05-25 15:49
 **/
public class Polymorphism {

    public static void main(String[] args) {
        Child c1 = new Child();
        Parents p2 = new Parents();
        c1.run("parent");
        c1.run(111);
        //c1.test();
        // 编译器认为本应该调用父类的run方法的
        //通过Parent类的引用变量来调用run方法。
        // 因为它引用了子类对象，并且子类方法覆盖了Parent类方法，所以在运行时将调用子类方法。
        // 由于方法调用是由JVM而不是编译器确定的，因此称为运行时多态。

        //p2.run();
    }

}

class Parents {

    public void run(String s) {
        System.out.println("parents=======run()方法" + s);
    }

    public void test() {
        System.out.println("parent test ");
    }

}

class Child extends Parents {

    public void run(int i) {
        System.out.println("我是子类Child的========run方法" + i);
    }

}
