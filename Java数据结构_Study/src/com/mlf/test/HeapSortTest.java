package com.mlf.test;

import com.mlf.datastructure.heap.HeapSort;

import java.util.Arrays;

/**
 * @program: Java基础
 * @description: 堆排序测试
 * @author: mlf
 * @date: 2022-05-21 19:25
 **/
public class HeapSortTest {
    public static void main(String[] args) {
        String[] source = {"A", "P", "B", "D", "C", "A", "c", "G", "E", "X", "Z"};
        HeapSort.sort(source);
        System.out.println(Arrays.toString(source));
    }
}
