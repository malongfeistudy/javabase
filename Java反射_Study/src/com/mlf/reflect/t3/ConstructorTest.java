package com.mlf.reflect.t3;

import com.mlf.reflect.t1.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * @program: Java基础
 * @description: Constructor类及其用法: Constructor类存在于反射包(java.lang.reflect)中，反映的是Class 对象所表示的类的构造方法。
 * @author: mlf
 * @date: 2022-05-25 18:04
 **/
public class ConstructorTest {

    public static void main(String[] args) throws Exception {

        Class<?> clazz = Class.forName("com.mlf.reflect.t1.User"); // 通过Class对象 获取Class对象的引用

        // 方式一：实例化默认构造方法，User必须声明无参构造函数,否则将抛异常
        // newInstance()	返回一个Object对象，是实现“虚拟构造器”的一种途径。使用该方法创建的类，必须带有无参的构造器。
        User user = (User) clazz.newInstance();
        user.setAge(18);
        user.setName("malongfei");
        System.out.println(user.toString());
        System.out.println("--------------------------------------------");

        // 方式二：getConstructors()方法，返回指定参数类型，具有public访问权限的构造函数对象
        Constructor<?> cs1 = clazz.getConstructor(Integer.class, String.class);
        User user1 = (User) cs1.newInstance(12, "mlf");
        //user1.setAge(17);
        System.out.println(user1.toString());
        System.out.println("--------------------------------------------");

        // 方式二：getDeclaredConstructor()方法，返回指定参数类型、所有声明的（包括private）构造函数对象
        Constructor<?> cs3 = clazz.getDeclaredConstructor(Integer.class);
        User user2 = (User) cs3.newInstance(2);
        user2.setName("mlf2");
        System.out.println(user2.toString());
        System.out.println("--------------------------------------------");

        // 返回所有具有public访问权限的构造函数的Constructor对象数组
        Constructor<?>[] cs2 = clazz.getConstructors();
        System.out.println(Arrays.toString(cs2));

        // 返回所有构造函数的Constructor对象数组（包裹private）
        Constructor<?>[] cs4 = clazz.getDeclaredConstructors();
        System.out.println(Arrays.toString(cs4));
        System.out.println(" 查看每个构造方法需要的参数");
        for (int i = 0; i < cs4.length; i++) {
            Class<?>[] parameterTypes = cs4[i].getParameterTypes();
            System.out.println("=====");
            System.out.print(cs4[i] + ": ");
            //获取构造函数参数类型
            for (int i1 = 0; i1 < parameterTypes.length; i1++) {
                System.out.print(parameterTypes[i1]);
            }
            System.out.println("=====");

        }
        System.out.println("--------------------------------------------");

        // Constructor本身的方法
        // 返回Class对象，返回真实类
        Constructor<?> declaredConstructor = clazz.getDeclaredConstructor(String.class);
        System.out.println(declaredConstructor.getDeclaringClass());


        System.out.println("-----getGenericParameterTypes-----");
        //对象表示此 Constructor 对象所表示的方法的形参类型
        Type[] tps = declaredConstructor.getGenericParameterTypes();
        for (Type tp : tps) {
            System.out.println("参数名称tp:" + tp);
        }

        System.out.println("-----getParameterTypes-----");
        //获取构造函数参数类型
        Class<?>[] clazzs = declaredConstructor.getParameterTypes();
        for (Class<?> claz : clazzs) {
            System.out.println("参数名称:" + claz.getName());
        }

        System.out.println("-----getName-----");
        //以字符串形式返回此构造方法的名称
        System.out.println(declaredConstructor.getName());

        System.out.println("-----getoGenericString-----");
        //返回描述此 Constructor 的字符串，其中包括类型参数。
        System.out.println(declaredConstructor.toGenericString());
    }

}







