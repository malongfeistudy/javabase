package com.mlf.datastructure.sort;

import java.util.Arrays;

import static com.mlf.datastructure.sort.Sort.CommonSortUtils.exch;
import static com.mlf.datastructure.sort.Sort.CommonSortUtils.greater;

class ttt {
    public static void main(String[] args) {
        Sort.BubbleSort.bubbleSort(new Integer[]{1, 2, 1, 4, 65, 3, 4, 5, 6, 7, 8, 9});
    }
}

public class Sort {

    //排序公用方法（交换和比较）
    public static class CommonSortUtils {

        /*
         数组元素i和j交换位置方法
         * */
        public static void exch(Comparable[] a, int i, int j) {
            Comparable temp;
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }

        /*
         * 比较是否a>b的方法
         * */
        public static boolean greater(Comparable a, Comparable b) {
            return a.compareTo(b) > 0;
        }

        /*
         * 比较是否a>b的方法
         * */
        public static boolean less(Comparable a, Comparable b) {
            return b.compareTo(a) > 0;
        }


    }

    /*
        本冒泡排序算法：升序 分为三个方法较之前更加 封装解耦
        最坏时间复杂度：逆序情况下O(n^2)
        适用场景：元素较少的情况下可选择适用
        优点：安全稳定排序，比较两个相同的数据时顺序不变
    * */
    public static class BubbleSort {

        /*
         * 对数组a中的元素排序，改良时间复杂度，空间换时间，冗余flag变量
         */
        public static void bubbleSort(Integer[] a) {
            for (int i = 0; i < a.length - 1; i++) {           // 外层循环，负责控制比较趟数
                boolean flag = false;                          // 标志位，记录本趟循环是否进行了交换（即是否整体已经有序）
                for (int j = 0; j < a.length - 1 - i; j++) {   // 内层循环，比较为冒泡的数
                    if (greater(a[j], a[j + 1])) {
                        exch(a, j, j + 1);
                        flag = true;                           // 记录已经交换过
                    }
                }
                if (!flag) {
                    break;                              // 本趟未交换，已经有序，退出外层循环
                }
                System.out.println("第" + (i + 1) + "趟：" + Arrays.toString(a)); //测试每趟下来的结果
            }
        }
    }

    /*
    *   选择排序算法：升序 分为三个方法较之前更加 封装解耦
        时间复杂度：O(n^2)
        适用场景：元素较少的情况下可选择适用
        优点：不稳定排序，比较两个相同的数据时顺序可能发生改变
    * */
    public static class SelectSort {

        //对数组a中的元素排序
        public static void SelectSort(Integer[] arr) {
            int minIndex;                           //最小元素下标
            for (int i = 0; i < arr.length - 1; i++) {       //进行N次交换
                minIndex = i;                         //默认最小元素下标为i
                for (int j = i + 1; j < arr.length; j++) {  //从i+1比较到最后一个元素
                    if (greater(arr[minIndex], arr[j])) {
                        minIndex = j;                 //被较小元素下标替换
                    }
                }
                exch(arr, i, minIndex);               //交换i与最小元素
            }
        }
    }

    /*
    直接插入排序  最坏时间复杂度：O(n^2)  最好：O(n)
    * */
    public static class Insertion {

        //升序
        public static void insertionSort(Integer[] wait) {
            for (int i = 1; i < wait.length; i++) {                    //插入次数为待排序数组wait[].length-1 从索引1开始，0不用比较
                for (int j = i; j > 0; j--) {                          //j>0 防止 j-1 导致 ArrayIndexOutOfBoundsException
                    if (greater(wait[j - 1], wait[j])) {            // 前 > 后时交换
                        exch(wait, j - 1, j);
                    } else {
                        break;
                    }
                }
            }
        }

    }

    /*希尔排序的排序思路是：
    把较大的数据集合分割成若干个小组（逻辑上分组），
    然后对每一个小组分别进行插入排序，
    此时，插入排序所作用的数据量比较小（每一个小组），插入的效率比较高
    */
    public static class ShellSort {

        public static void shellSort(Integer[] a) {
            int h = 1;
            //1、确定h初始值
            while (h < a.length / 2) {
                h = 2 * h + 1;
            }

            //2、排序
            while (h > 0) {
                //2.1、找到待插入的元素
                for (int i = h; i < a.length; i++) {
                    //2.3、把待插入元素放入有序序列中
                    for (int j = i; j >= h; j -= h) {
                        //2.2、待插入元素为a[j] 比较a[j]与a[j-h]
                        if (greater(a[j - h], a[j])) {
                            //2.3、若j-h较大则交换
                            exch(a, j - h, j);
                        } else {
                            break;
                        }
                    }
                }
                h = h / 2;
            }
        }
    }

    /*归并排序error：*/
    public static class Merge {
        //private static int[] tmp;
        //两路归并算法，两个排好序的子序列合并为一个子序列
        public static void merge(int[] a, int left, int mid, int right) {
            int p1 = left, p2 = mid + 1, k = left;//p1、p2是检测指针，k是存放指针
            int[] tmp = new int[a.length];//辅助数组
            while (p1 <= mid && p2 <= right) {
                if (a[p1] <= a[p2])   //稳定
                {
                    tmp[k++] = a[p1++];
                } else {
                    tmp[k++] = a[p2++];
                }
            }

            while (p1 <= mid) {
                tmp[k++] = a[p1++];//如果第一个序列未检测完，直接将后面所有元素加到合并的序列中
            }
            while (p2 <= right) {
                tmp[k++] = a[p2++];//同上
            }

            //复制回原素组
            for (int i = left; i <= right; i++) {
                a[i] = tmp[i];
            }
        }

        public static void mergeSort(int[] a) {
            int lo = 0;
            int hi = a.length - 1;
            mergeSort(a, lo, hi);
        }

        public static void mergeSort(int[] a, int start, int end) {
            if (start < end) {//当子序列中只有一个元素时结束递归
                int mid = (start + end) / 2;//划分子序列
                mergeSort(a, start, mid);//对左侧子序列进行递归排序
                mergeSort(a, mid + 1, end);//对右侧子序列进行递归排序
                merge(a, start, mid, end);//合并
            }
        }
    }

    //快速排序 最坏时间复杂度：O（n^2）  最好时间复杂度：O（nlgn）
    public static class QuickSort {

        //对数组内元素进行排序
        public static void sort(Comparable[] a) {
            int lo = 0;
            int hi = a.length - 1; //-1防止空指针异常
            sort(a, lo, hi);
        }

        //对数组a 中索引lo到hi之间的元素进行排序
        private static void sort(Comparable[] a, int lo, int hi) {
            //安全性校验
            if (lo >= hi) {
                return;
            }

            //对数组中lo到hi索引的元素进行分组（左子祖和右子组）
            int paritition = paritition(a, lo, hi);

            //左子组排序
            sort(a, lo, paritition - 1);
            //右子组排序
            sort(a, paritition + 1, hi);
        }

        private static int paritition(Comparable[] a, int lo, int hi) {

            //分组基准
            Comparable key = a[lo];
            //左右指针
            int left = lo;
            int right = hi + 1;
            //切分
            while (true) {
                //移动right
                while (greater(a[--right], key)) {//右指针元素比基准值大继续移动
                    if (right == lo) {
                        break;
                    }
                }
                //移动left
                while (greater(key, a[++left])) {//左指针元素比基准值小继续移动
                    if (left == right) {
                        break;
                    }
                }
                if (left >= right) {
                    break;
                } else {
                    exch(a, left, right);
                }
            }
            //交换基准
            exch(a, lo, right);
            //返回基准元素位置
            return right;
        }
    }

}


















