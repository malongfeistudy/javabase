package com.mlf.zidy.demo1;

import java.lang.reflect.Field;

/**
 * @program: Java基础
 * @description: 自定义注解
 * @author: mlf
 * @date: 2022-05-28 23:48
 **/

public class MyAnnotation {

    @MyFiled(description = "用户名", length = 10)
    public String userName;

    public static void main(String[] args) throws IllegalAccessException {
        new MyFieldTest("malongfei");
        Class<?> cls = MyAnnotation.class;

        //Field[] fields = cls.getFields();
        //for (Field field : fields) {
        //    System.out.println(field.getName());
        //}
        for (Field field : cls.getFields()) {
            if (field.isAnnotationPresent(MyFiled.class)) {
                MyFiled annotation = field.getAnnotation(MyFiled.class);
                System.out.println("字段：" + field.getName() + "描述：" + annotation.description() +
                        "长度：" + annotation.length());
            }
        }

        //System.gc();

    }

}

class MyFieldTest {

    //使用自定义注解
    @MyFiled(description = "用户名", length = 12)
    private String username;

    public MyFieldTest(String username) {
        this.username = username;
    }
}