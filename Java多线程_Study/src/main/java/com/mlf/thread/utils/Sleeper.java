package com.mlf.thread.utils;

import java.util.concurrent.TimeUnit;

/**
 * @program: Java基础
 * @description: 睡眠工具类
 * @author: mlf
 * @date: 2022-07-23 16:35
 **/
public class Sleeper {

    public static void sleep(int i) {
        try {
            TimeUnit.SECONDS.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(double i) {
        try {
            TimeUnit.MILLISECONDS.sleep((int) (i * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
