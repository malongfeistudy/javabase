package com.mlf.demo1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @program: Java基础
 * @description: 下限
 * @author: mlf
 * @date: 2022-05-22 10:06
 **/
class Info<T> {
    private T var;        // 定义泛型变量

    public void setVar(T var) {
        this.var = var;
    }

    public T getVar() {
        return this.var;
    }

    public String toString() {    // 直接打印
        return this.var.toString();
    }
}

public class Demo03 {
    public static void main(String args[]) {
        Info<String> i1 = new Info<String>();        // 声明String的泛型对象
        Info<Object> i2 = new Info<Object>();        // 声明Object的泛型对象
        i1.setVar("hello");
        i2.setVar(new Object());
        fun(i1);
        fun(i2);
    }

    @Test
    public void test() {
        ArrayList<B> list = new ArrayList<>();
        list.add(new B());
        list.add(new C());   //可添加子类和本类，因为规定了上限
        //C max = max(list); //不能向下转型，因为返回值规定了下限
        A max = max(list);  //可以向上转型
    }


    //要进行比较，所以 E 需要是可比较的类，因此需要 extends Comparable<…>（注意这里不要和继承的 extends 搞混了，不一样）
    // Comparable< ? super E> 要对 E 进行比较，即 E 的消费者，所以需要用 super
    // 而参数 List< ? extends E> 表示要操作的数据是 E 的子类的列表，指定上限，这样容器才够大

    // 返回类型E extends Comparable<>规定 E类型 上限必须是继承了Comparable的子类
    // Comparable<? super E>规定了比较器类型下限只能是 E类型及其父类  返回类型E可以向上转型
    // List<? extends E>规定List集合存储类型上限为 E 或者继承 E 的类型。
    private <E extends Comparable<? super E>> E max(List<? extends E> e1) {
        if (e1 == null) {
            return null;
        }
        // 迭代器返回的元素属于  E  的某个子类型
        Iterator<? extends E> iterator = e1.iterator();
        E result = iterator.next();
        while (iterator.hasNext()) {
            E next = iterator.next();
            if (next.compareTo(result) > 0) {
                result = next;
            }
        }
        return result;
    }


    // 只能接收String或Object类型的泛型，String类的父类只有Object类
    public static void fun(Info<? super String> temp) {
        System.out.print(temp + ", ");
    }

}

class A implements Comparable<A> {

    @Override
    public int compareTo(A o) {
        return 0;
    }
}

class B extends A {

}

class C extends B {

}