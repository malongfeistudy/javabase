package com.mlf.thread.demo1;

public class ThreadTest4 implements Runnable{

    @Override
    public void run() {
        int[] money = {1, 2, 3, 4, 5, 6};
        int i=money.length-1;
        while (i >= 0) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "--->抢到了" + money[i--] + "元");
        }
    }

    public static void main(String[] args) {
        ThreadTest4 threadTest4 = new ThreadTest4();
        new Thread(threadTest4,"张三").start();
        new Thread(threadTest4,"李四").start();
        new Thread(threadTest4,"王五").start();
    }
}

