package com.mlf.reflect.t2;

import com.mlf.reflect.t1.TestRe;
import com.mlf.reflect.t1.User;
import org.junit.Test;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @program: Java基础
 * @description: forName()
 * (1)获取Class对象的一个引用，但引用的类还没有加载(该类的第一个对象没有生成)就加载了这个类。
 * (2)为了产生Class引用，forName()立即就进行了初始化。
 * @author: mlf
 * @date: 2022-05-21 23:16
 **/
public class TestClassForName {


    @Test
    public void test() throws Exception, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/class.properties"));
        String classPath = properties.getProperty("classPath").toString();
        System.out.println(classPath);
        String method = properties.getProperty("method").toString();
        System.out.println(method);

        Class<?> cls = Class.forName(classPath);
        TestRe re = (TestRe) cls.newInstance();
        Method test = cls.getMethod(method);
        test.invoke(re);

        System.out.println("===================");
        Field name = cls.getField("name");
        System.out.println(name);  //返回的是类型
        System.out.println(name.get(re)); // get获取值 通过 字段.get对象  传统方法：对象.get字段

        //Field age = cls.getField("age");
        //System.out.println(age.get(re));  //NoSuchFieldException
        Field age = cls.getDeclaredField("age");
        age.setAccessible(true);
        System.out.println(age.get(re));


    }

    @Test
    public void test1() throws Exception {
        time1();
        time2();
        time3();
        //time1():
        //5ms
        //time2():
        //1195ms
    }

    public void time1() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 999999999; i++) {
            TestRe testRe = new TestRe();
            testRe.test();
        }
        long end = System.currentTimeMillis();
        System.out.println("time1():");
        System.out.println(end - start + "ms");
    }

    public void time2() throws Exception {
        Class<?> cls = Class.forName("com.mlf.reflect.t1.TestRe");
        TestRe re = (TestRe) cls.newInstance();
        Method test = cls.getMethod("test");
        long start = System.currentTimeMillis();
        for (int i = 0; i < 999999999; i++) {
            test.invoke(re);
        }
        long end = System.currentTimeMillis();
        System.out.println("time2():");
        System.out.println(end - start + "ms");
    }

    // 关闭访问检查，提高访问效率
    public void time3() throws Exception {
        Class<?> cls = Class.forName("com.mlf.reflect.t1.TestRe");
        TestRe re = (TestRe) cls.newInstance();
        Method test = cls.getMethod("test");
        test.setAccessible(true); // 关闭访问检查，提高访问效率
        long start = System.currentTimeMillis();
        for (int i = 0; i < 999999999; i++) {
            test.invoke(re);
        }
        long end = System.currentTimeMillis();
        System.out.println("time3():");
        System.out.println(end - start + "ms");
    }

    public static void main(String[] args) throws ClassNotFoundException {

        System.out.println(Class.forName("com.mlf.reflect.t1.User"));
        System.out.println(User.class);
        //System.out.println(Object.class.getClass());
        String[] s = new String[1];
    }

    /*@CallerSensitive
    public static Class<?> forName(String name, boolean initialize,
                                   ClassLoader loader)
            throws ClassNotFoundException
    {
        Class<?> caller = null;
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            // 先通过反射，获取调用进来的类信息，从而获取当前的 classLoader
            caller = Reflection.getCallerClass();
            if (sun.misc.VM.isSystemDomainLoader(loader)) {
                ClassLoader ccl = ClassLoader.getClassLoader(caller);
                if (!sun.misc.VM.isSystemDomainLoader(ccl)) {
                    sm.checkPermission(
                            SecurityConstants.GET_CLASSLOADER_PERMISSION);
                }
            }
        }
        // 调用native方法进行获取class信息
        return forName0(name, initialize, loader, caller);
    }

    static ClassLoader getClassLoader(Class<?> caller) {
        // This can be null if the VM is requesting it
        if (caller == null) {
            return null;
        }
        // Circumvent security check since this is package-private
        return caller.getClassLoader0();
    }

    private final ClassLoader classLoader;
    ClassLoader getClassLoader0() { return classLoader; }

    // Initialized in JVM not by private constructor
    // This field is filtered from reflection access, i.e. getDeclaredField
    // will throw NoSuchFieldException


    private static native Class<?> forName0(String name, boolean initialize,
                                            ClassLoader loader,
                                            Class<?> caller)
            throws ClassNotFoundException;*/

}
