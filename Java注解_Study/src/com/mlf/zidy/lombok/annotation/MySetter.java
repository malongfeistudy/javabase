package com.mlf.zidy.lombok.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-05 13:49
 **/

@Target(ElementType.TYPE)  // 类上
//@Documented
@Retention(RetentionPolicy.SOURCE)
        // 保留位置:源码
@interface MySetter {

}

