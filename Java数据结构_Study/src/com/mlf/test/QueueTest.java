package com.mlf.test;

import com.mlf.datastructure.lineartable.Queue;

/**
 * @program: Java基础
 * @description: 队列测试
 * @author: mlf
 * @date: 2022-05-26 18:47
 **/
public class QueueTest {
    public static void main(String[] args) {
        Queue<String> queue = new Queue<>();
        queue.enqueue("a");
        queue.enqueue("b");
        queue.enqueue("c");
        queue.enqueue("d");
        for (String str : queue) {
            System.out.print(str + " ");
        }
        System.out.println("-----------------------------");
        String result = queue.dequeue();
        System.out.println("出列了元素：" + result);
        System.out.println(queue.size());
    }
}
