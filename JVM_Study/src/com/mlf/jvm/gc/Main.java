package com.mlf.jvm.gc;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-13 22:55
 **/
public class Main {
    public static void main(String[] args) {
        method1();
        int a = 1;
        int b = a + 1;
    }

    private static void method1() {
        method2(1, 2);
    }

    private static int method2(int a, int b) {
        int c = a + b;
        return c;
    }
}
