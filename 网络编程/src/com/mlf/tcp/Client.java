package com.mlf.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @program: Java基础
 * @description: 客户端程序
 * @author: mlf
 * @date: 2022-05-11 23:00
 **/
public class Client {
    public static void main(String[] args) {
        new TCPClient().connect();
    }
}


class TCPClient {

    private static final int PORT = 7788; //定义一个端口号

    public void connect() {

        Socket client = null;

        try {
            client = new Socket(InetAddress.getLocalHost(), PORT);

            InputStream is = client.getInputStream(); //拿到接受的数据流

            byte[] data = new byte[1024];

            int len = is.read(data);

            System.out.println(new String(data, 0, len));  //输出数据
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}


