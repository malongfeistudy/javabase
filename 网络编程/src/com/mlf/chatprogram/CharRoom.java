package com.mlf.chatprogram;

import java.util.Scanner;

/**
 * @program: Java基础
 * @description: 聊天室
 * @author: mlf
 * @date: 2022-05-11 21:13
 **/
public class CharRoom {

    public static void main(String[] args) {

        System.out.println("欢饮来到聊天室：");

        Scanner sc = new Scanner(System.in);

        System.out.println("请输入本程序发送端口端口号：");
        int sendPort = sc.nextInt();

        System.out.println("请输入本程序接收端口端口号：");
        int receviePort = sc.nextInt();

        System.out.println("聊天程序启动");
        new Thread(new SendTask(sendPort), "发送端任务").start();
        new Thread(new ReceiveTask(receviePort), "接受端任务").start();

    }

}
