package com.mlf.proxy.realfun;

/**
 * @program: Java基础
 * @description: 接口类
 * @author: mlf
 * @date: 2022-04-23 17:02
 **/
public interface UserDao {

     void add(String s);
     void del();
     void update();
     void query();

}
