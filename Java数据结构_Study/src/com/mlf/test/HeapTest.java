package com.mlf.test;

import com.mlf.datastructure.heap.Heap;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-21 17:30
 **/
public class HeapTest {

    public static void main(String[] args) {

        Heap<String> tHeap = new Heap<>(10);

        tHeap.insert("A");
        tHeap.insert("B");
        tHeap.insert("C");
        tHeap.insert("D");
        tHeap.insert("E");
        tHeap.insert("F");
        tHeap.insert("G");

        String res = null;
        while ((res = tHeap.delMax()) != null) {
            System.out.print(res + " ");
        }

    }

}
