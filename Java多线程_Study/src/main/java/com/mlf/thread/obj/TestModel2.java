package com.mlf.thread.obj;

import com.mlf.thread.utils.Sleeper;
import lombok.extern.java.Log;

import java.util.LinkedList;

/**
 * @program: Java基础
 * @description: 测试 生产者--消费者模型   消息队列实现
 * @author: mlf
 * @date: 2022-07-26 12:10
 **/
public class TestModel2 {

    public static void main(String[] args) {

        MessageQueue queue = new MessageQueue(2);

        for (int i = 0; i < 3; i++) {
            int id = i + 1;
            new Thread(() -> {
                queue.put(new Message(id, "消息值来源" + id));
            }, "生产者" + (i + 1)).start();
        }

        new Thread(() -> {
            while (true) {
                Sleeper.sleep(1);
                Message message = queue.take();
            }
        }, "消费者").start();

    }

}


// 消息队列类
@Log
class MessageQueue {

    // 采用双向队列（Java里面用 LinkedList实现 一边存、一边取）
    // 消息队列集合
    private LinkedList<Message> linkedList = new LinkedList<>();
    // 队列容量
    private int capacity;

    // 容量加入构造方法可以传入
    public MessageQueue(int capacity) {
        this.capacity = capacity;
    }

    // 获取消息  消费者
    public Message take() {
        // wait需要配合 synchronized 同步代码块使用
        synchronized (linkedList) {
            // 检查队列为空 需要等待
            while (linkedList.isEmpty()) {
                try {
                    log.info("队列为空 消费者消费需要等待生产者生产");
                    linkedList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 若有消息则返回 队列头部
            Message message = linkedList.removeFirst();
            log.info(" 消费者消费 " + message.toString());
            // 存储完通知生产线程 生产
            linkedList.notifyAll();
            return message;
        }

    }

    // 存入消息  生产者
    public void put(Message message) {
        // 检查消息队列是否存满
        synchronized (linkedList) {
            // 存储消息 存满即等待消费者进行消费
            while (linkedList.size() == this.capacity) {
                try {
                    log.info("队列已满 等待消费者进行消费：");
                    linkedList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 若没存满 则进行生产存储
            linkedList.add(message);
            log.info(" 生产者生产消息：" + message.toString());
            // 存储完通知消费线程 消费
            linkedList.notifyAll();
        }
    }
}


// 消息资源类   为保证线程安全  final不可继承覆盖、无set方法不可修改
final class Message {
    // 消息id
    private Integer id;
    // 消息内容
    private Object value;

    public Message(Integer id, Object value) {
        this.id = id;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", value=" + value +
                '}';
    }
}
