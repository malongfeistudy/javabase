package com.mlf.thread.votatiles;

import lombok.extern.java.Log;

/**
 * @program: Java基础
 * @description: 两阶段终止--volatile  加入犹豫模式 balking
 * @author: mlf
 * @date: 2022-07-27 17:34
 **/

@Log
public class TwoPhaseStop2 {

    // 监控线程
    private Thread monitorThread;

    // 多线程共享变量 单线程写入（停止线程） 多线程读取 使用 volatile
    private volatile boolean stop = false;

    // 判断当前线程是否已经启动(类似单例模式 创建对象时判断对象是否存在)
    private boolean starting = false;

    // 启动监控线程
    public void start() {
        synchronized (this) {
            if (starting) return;
            starting = true;
        }
        monitorThread = new Thread(() -> {
            log.info("开始监控");
        }, "monitor");
        monitorThread.start();
    }

    // 停止监控线程
    public void stop() {
        stop = true;
        monitorThread.interrupt();
    }

}

/**
 * @author 马龙飞
 * @description TODO 测试线程重复启动
 * @date 2022/7/27 18:38
 */
@Log
class Test2 {
    public static void main(String[] args) {
        TwoPhaseStop2 twoPhaseStop = new TwoPhaseStop2();
        for (int i = 0; i < 1000; i++) {
            new Thread(twoPhaseStop::start).start();
        }
    }
}
