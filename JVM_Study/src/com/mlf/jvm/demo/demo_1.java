package com.mlf.jvm.demo;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-10 12:20
 **/
class StackOverflowErrorTest {

    private static int count;

    public static void main(String[] args) {
        try {
            method();
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(count);
        }
    }

    public static void method() {
        count++;
        method();
    }
}
