package com.mlf.datastructure.uf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @program: Java基础
 * @description: 畅通工程
 * @author: mlf
 * @date: 2022-06-01 16:08
 **/
public class Traffic_Project {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(Traffic_Project.class.getClassLoader().getResourceAsStream("traffic_project.txt")));

        //BufferedReader reader1 = new BufferedReader(
        //        new InputStreamReader(new FileInputStream("A:\\Java开发学习\\Learning\\Java基础\\Java数据结构_Study\\src\\traffic_project1.txt")));
        int number = Integer.parseInt(reader.readLine());
        UF_Tree_Weight uf_tree_weight = new UF_Tree_Weight(number);
        int roadNumber = Integer.parseInt(reader.readLine());

        for (int i = 0; i < roadNumber; i++) {
            String s = reader.readLine();
            int p = Integer.parseInt(s.split(" ")[0]);
            int q = Integer.parseInt(s.split(" ")[1]);
            uf_tree_weight.union(p, q);
        }

        System.out.println("还需修路的条数为：" + (uf_tree_weight.getCount() - 1));
    }

}
