package com.mlf.test;

import com.mlf.datastructure.sort.Sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SortCompare {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> list=new ArrayList<>();
        //1、读取文件
        BufferedReader reader=new BufferedReader(
                new InputStreamReader(
                        SortCompare.class.getClassLoader().getResourceAsStream("reverse_shell_insertion.txt")));
        String line=null;
        while ((line= reader.readLine())!=null){
            //2、装入集合
            list.add(Integer.parseInt(line));
        }
        Integer[] arr = new Integer[list.size()];
        list.toArray(arr);
        //testInsertion(arr);         //insertionSort：16383ms
        testShellSort(arr);         //ShellSort：25ms
    }

    //测试插入排序时间函数
    public static void testInsertion(Integer[] a){
        long start = System.currentTimeMillis();
        Sort.Insertion.insertionSort(a);
        long end = System.currentTimeMillis();
        System.out.println("insertionSort："+(end-start)+"ms");
    }

    //测试插入排序时间函数
    public static void testShellSort(Integer[] a){
        long start = System.currentTimeMillis();
        Sort.ShellSort.shellSort(a);
        long end = System.currentTimeMillis();
        System.out.println("ShellSort："+(end-start)+"ms");
    }
}

