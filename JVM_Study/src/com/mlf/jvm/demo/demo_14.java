package com.mlf.jvm.demo;

import sun.misc.Unsafe;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * @program: Java基础
 * @description: unsafe释放内存
 * @author: mlf
 * @date: 2022-05-13 10:35
 **/
public class demo_14 {

    static int _1GB = 1024 * 1024 * 1024;

    public static void main(String[] args) throws IOException {
        Unsafe unsafe = getUnsafe();

        //分配内存
        long base = unsafe.allocateMemory(_1GB);
        unsafe.setMemory(base, _1GB, (byte) 0);
        System.out.println("分配完毕···");
        System.in.read();

        //释放内存
        unsafe.freeMemory(base);
        System.out.println("释放完毕···");
        System.in.read();
    }

    public static Unsafe getUnsafe() {

        try {
            Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            Unsafe unsafe = (Unsafe) f.get(null);
            return unsafe;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

}
