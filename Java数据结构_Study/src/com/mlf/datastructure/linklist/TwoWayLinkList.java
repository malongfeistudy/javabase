package com.mlf.datastructure.linklist;

import java.util.Iterator;

public class TwoWayLinkList<T> implements Iterable<T> {

    //头结点
    private Node head;
    //最后一个结点
    private Node last;
    //链表的长度
    private int N;

    //结点类
    private class Node {
        //存储数据
        public T item;
        //指向前一个
        public Node pre;
        //指向后一个
        public Node next;

        public Node(T item, Node pre, Node next) {
            this.item = item;
            this.pre = pre;
            this.next = next;
        }
    }

    public TwoWayLinkList() {
        this.head = new Node(null, null, null);
        this.last = null;
        this.N = 0;
    }

    //清空链表
    public void clear() {
        this.head.next = null;
        this.head.pre = null;
        this.head.item = null;
        this.last = null;
        this.N = 0;
    }

    //链表长度
    public int length() {
        return N;
    }

    //是否为空
    public boolean isEmpty() {
        return N == 0;
    }

    //获取第一个元素
    public T getFirst() {
        if (isEmpty()) {
            return null;
        }
        return head.next.item;
    }

    //获取最后一个元素
    public T getLast() {
        if (isEmpty()) {
            return null;
        }
        return last.item;
    }

    //插入元素t
    public void insert(T t) {
        if (isEmpty()) {
            //链表为空  创建新结点 成为尾结点
            last = new Node(t, head, null);
            //让头结点指向尾结点
            head.next = last;
        } else {
            //结点不为空
            Node newNode = new Node(t, last, null);
            //让last指向 newNode
            last.next=newNode;
            //新结点成为尾结点
            last=newNode;
        }
        N++;
    }

    //向指定位置i处插入元素
    public void insert(int i, T t) {
        //i-1处结点
        Node pre = head;
        for (int index = 0; index < i; index++) {
            pre = pre.next;
        }
        //i处结点
        Node curr = pre.next;
        //让i-1指向新创建结点，让新结点指向原来的i位置
        Node newNode = new Node(t, pre, curr);
        pre.next = newNode;
        curr.pre = newNode;
        //元素个数+1
        N++;
    }

    //获取i位置结点元素
    public T get(int i) {
        if (this.length()==0){
            return null;
        }else {
            Node n = head.next;
            for (int index = 0; index < i; index++) {
                n = n.next;
            }
            return n.item;
        }
    }

    //找到t在链表第一次出现的位置
    public int indexOf(T t) {
        if (this.length()==0) {
            return -1;
        }else {
            Node n = this.head;
            for (int i = 0; n.next != null; i++) {
                n=n.next;
                if (n.item.equals(t)) {
                    return i;
                }
            }
            return -1;
        }
    }

    //删除指定i处结点 并返回元素
    public T remove(int i) {
        if (this.length()==0){
            return null;
        }else if (i==this.length()-1){ //删除尾结点 要更新last
            T item = last.item;
            last=last.pre;
            N--;
            return item;
        } else {
            //i-1处结点
            Node pre = head;
            for (int index = 0; index <= i-1; index++) {
                pre = pre.next;
            }
            //i处结点
            Node curr = pre.next;
            //i+1处结点
            Node nnext = curr.next;
            //i-1和i+1处结点 相互指向
            pre.next=nnext;
            nnext.pre=pre;
            //元素个数-1
            N--;
            return curr.item;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new TIterator();

    }

    private class TIterator implements Iterator{
        private Node n;
        public TIterator(){
            this.n=head;
        }
        @Override
        public boolean hasNext() {
            return n.next!=null;
        }

        @Override
        public T next() {
            n = n.next;
            return n.item;
        }
    }
}












