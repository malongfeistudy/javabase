package com.mlf.jvm.demo;

/**
 * @program: Java基础
 * @description: 字符串延迟加载  遇见一行常量 放入一行 不是一次性放入串池
 * @author: mlf
 * @date: 2022-05-10 17:06
 **/
public class demo_6 {

    public static void main(String[] args) {

        int x = args.length;
        System.out.println(); //字符串个数

        System.out.println("1");   //执行完 String Count 2148
        System.out.println("2");
        System.out.println("3");   //执行完 String Count 2149
        System.out.println("4");
        System.out.println("5");
        System.out.println("6");
        System.out.println("7");
        System.out.println("8");
        System.out.println("9");
        System.out.println("10");   //String Count 2157


        System.out.println("1");    //String Count 2157
        System.out.println("2");
        System.out.println("3");
        System.out.println("4");
        System.out.println("5");
        System.out.println("6");
        System.out.println("7");
        System.out.println("8");
        System.out.println("9");
        System.out.println("10");   //String Count 2157


    }

}
