package com.mlf.thread.obj;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * @program: Java基础
 * @description: 设计模式 保护性暂停 实现
 * @author: mlf
 * @date: 2022-07-25 16:07
 **/

@Slf4j
public class TestModel1 {
    public static void main(String[] args) {
        GuardObject1 guardObject = new GuardObject1();

        // 线程一：
        new Thread(() -> {
            log.info("进入线程一 执行");
            int i = 0;
            try {
                i = new Random().nextInt(5);
                Thread.sleep(i * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            guardObject.setResp(i);
            log.info("执行时间为: " + i + "s");
            // 采用保护性暂停模式 ....
            // 优点一：执行完后还 可以做一些别的事情 并且不阻塞其他线程 将阻塞与唤醒阶段提前了
            // 优点二：标志位不用设置为全局变量，可以是线程局部变量
            // 若使用 join 则线程二必须等待全部执行完才能够执行
            log.info("采用保护性暂停模式 .... 可以做一些别的事情");
            try {
                // 使用休眠模拟其他业务时间
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "t1").start();
        // 二：
        new Thread(() -> {
            Object resp = null;
            log.info("进入线程二 等待执行结果并且返回");
            try {
                resp = guardObject.getResp(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("返回结果: " + resp);
        }, "t2").start();


    }
}

class GuardObject1 {
    // 结果
    private Object response;

    // 获取结果
    public Object getResp(long millis) throws InterruptedException {
        synchronized (this) {
            long base = System.currentTimeMillis(); // 开始时间
            long passedTime = 0; // 经历时间
            while (isAlive()) {
                long delay = millis - passedTime; // 更新 实时剩余延迟时间
                if (delay <= 0) {
                    break;
                }
                this.wait(delay);
                passedTime = System.currentTimeMillis() - base; // 经历时间=求差值
            }
        }
        return response;
    }

    // 产生结果
    public void setResp(Object response) {
        synchronized (this) {
            this.response = response;
            this.notifyAll();
        }
    }

    // 判断是否满足条件
    public boolean isAlive() {
        return response == null;
    }
}