package com.mlf.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @program: Java基础
 * @description: 发送端程序
 * @author: mlf
 * @date: 2022-05-11 20:46
 **/
public class Sender {

    public static void main(String[] args) {

        DatagramSocket datagramSocket = null;
        DatagramPacket datagramPacket = null;

        try {
            //创建一个DatagramSocket对象 用于发送数据包
            datagramSocket = new DatagramSocket(300);

            String data = "hello udp!"; //需要发送的数据

            byte[] dataBytes = data.getBytes(); //将数据转化为数组

            //创建一个发送的数据包，里边封装了dataBytes数据，长度，接收端的IP及端口号
            datagramPacket = new DatagramPacket(dataBytes, 0, dataBytes.length,
                    InetAddress.getByName("localhost"), 8954);

            System.out.println("发送信息");

            try {
                Thread.sleep(2000); //等待延迟两秒发送
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            datagramSocket.send(datagramPacket); //发送数据

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            datagramSocket.close(); //关闭资源
        }


    }


}
