package com.mlf.thread.demo1;

//注意，线程开启不一定立即执行，由CPU调度执行
public class ThreadTest1 extends Thread {
    @Override
    public void run() {
        //run方法线程体
        for (int i=0;i<10;i++){
            System.out.println("我正在学习run----"+i);
        }
    }

    public static void main(String[] args) {

        //创建一个线程对象
        ThreadTest1 threadTest1 = new ThreadTest1();

        //调用start()开启线程
        threadTest1.start();

        //main线程，主线程
        for (int i=0;i<10;i++){
            System.out.println("我在看main代码----"+i);
        }
    }
}
