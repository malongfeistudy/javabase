package com.mlf.demo1;

/**
 * @program: Java基础
 * @description: 数组与泛型测试，不允许创建泛型数组
 * @author: mlf
 * @date: 2022-05-25 14:24
 **/
public class Demo_05 {
    public static void main(String[] args) {
        //String[] strings = new String[10];
        //Object[] objects = strings;
        //objects[0] = 123; // 数组不会报错，向上转型，是可协同变化类型，会警告，编译通过，运行会报数组存储异常
        //List<String>[] lists = new List<String>[2];
        // 直接编译报错，创建数组错误，因为数组类型是确定的，规定不允许创建
        // 假如泛型数组成功创建，那么下面的操作就不合理了
        //List<Integer> integerList = Arrays.asList(24); // 新建一个只有一个Integer元素的集合
        //Object[] objects1= lists; // 将泛型数组转为Object[]类型
        //objects1[0] = integerList; //添加Integer类型的集合 ，
        // 在运行时期泛型的类型会被擦除的，那么List<Integer>赋值给List<String>就没有问题，
        // 都变成了List这个原生态类型
        //String s = lists[0].get(0); // 必然报错，这也就是为什么不允许创建泛型数组的原因


    }
}
