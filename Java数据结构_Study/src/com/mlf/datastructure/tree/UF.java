package com.mlf.datastructure.tree;

/**
 * @program: Java基础
 * @description: 并查集
 * @author: mlf
 * @date: 2022-06-01 15:03
 **/
public class UF {

    //记录结点元素和该元素所在分组的标识
    private int[] eleAndGroup;
    //记录并查集中数据的分组个数
    private int count;

    //初始化并查集
    public UF(int N) {
        eleAndGroup = new int[N];
        for (int i = 0; i < N; i++) {
            eleAndGroup[i] = i;
        }
        this.count = N;
    }

    // 获取当前并查集中的数据有多少个分组
    public int getCount() {
        return count;
    }

    // 元素p所在分组的标识符
    public int find(int p) {
        return eleAndGroup[p];
    }

    // 判断连个元素是否在同一分组中
    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    // 把元素q、p所在分组合并
    public void union(int p, int q) {
        if (connected(p, q)) return;

        int pGroup = eleAndGroup[p];
        int qGroup = eleAndGroup[q];
        for (int i = 0; i < eleAndGroup.length; i++) {
            if (eleAndGroup[i] == pGroup) eleAndGroup[i] = qGroup;
        }
        // 分组数-1
        count--;
    }

}













