package com.mlf.letcode;

import org.junit.Test;

import java.util.*;

public class LetCode {

    private final LetCode LetCode = new LetCode();

    @Test
    public void test704(){

    }

    @Test
    public void test2(){
        //l1 = [2,4,3], l2 = [5,6,4]
        ListNode l1 = new ListNode(2);
        l1.next=new ListNode(4);
        l1.next.next=new ListNode(3);
        ListNode l2 = new ListNode(5);
        l2.next=new ListNode(6);
        l2.next.next=new ListNode(4);
        addTwoNumbers(l1,l2);
    }

    @Test
    public void test4() throws InterruptedException {

        int[] a=new int[]{1,3};
        int[] b=new int[]{2};
        double arrays = LetCode.findMedianSortedArrays(a, b);
        System.out.println(arrays);
        a.wait();
        a.notify();
    }

    @Test
    public void test1() {
        //[2,7,11,15]
        //9
        int[] nums = new int[]{2, 3, 2, 3};
        int target = 5;
        int[] ints = twoSum(nums, target);
        System.out.println("111:" + Arrays.toString(ints));
        int[] ints1 = twoSum2(nums, target);
        System.out.println("222:" + Arrays.toString(ints1));

    }

    //151. 颠倒字符串中的单词
    public String reverseWords1(String s) {
        String[] words = s.trim().split(" +");
        Collections.reverse(Arrays.asList(words));
        return String.join(" ", words);
    }
    static class Reverse{
        public static void main(String[] args) {
            String s = "124";
            System.out.println(s.substring(1, 2));
            //s.length()
            char[] ch = s.substring(0,1).toCharArray();
        }
       /**
        * 不使用Java内置方法实现
        * <p>
        * 1.去除首尾以及中间多余空格
        * 2.反转整个字符串
        * 3.反转各个单词
        */
       public String reverseWords2(String s) {
           for (int i = 0; i < s.length(); i++) {
               char[] array = s.toCharArray();
               int index = array[i] - 'a';
           }



           // System.out.println("ReverseWords.reverseWords2() called with: s = [" + s + "]");
           // 1.去除首尾以及中间多余空格
           StringBuilder sb = removeSpace(s);
           // 2.反转整个字符串
           reverseString(sb, 0, sb.length() - 1);
           // 3.反转各个单词
           reverseEachWord(sb);
           return sb.toString();
       }

       private StringBuilder removeSpace(String s) {
           // System.out.println("ReverseWords.removeSpace() called with: s = [" + s + "]");
           int start = 0;
           int end = s.length() - 1;
           while (s.charAt(start) == ' ') start++;
           while (s.charAt(end) == ' ') end--;
           StringBuilder sb = new StringBuilder();
           while (start <= end) {
               char c = s.charAt(start);
               if (c != ' ' || sb.charAt(sb.length() - 1) != ' ') {
                   sb.append(c);
               }
               start++;
           }
           // System.out.println("ReverseWords.removeSpace returned: sb = [" + sb + "]");
           return sb;
       }

       /**
        * 反转字符串指定区间[start, end]的字符
        */
       public void reverseString(StringBuilder sb, int start, int end) {
           // System.out.println("ReverseWords.reverseString() called with: sb = [" + sb + "], start = [" + start + "], end = [" + end + "]");
           while (start < end) {
               char temp = sb.charAt(start);
               sb.setCharAt(start, sb.charAt(end));
               sb.setCharAt(end, temp);
               start++;
               end--;
           }
           // System.out.println("ReverseWords.reverseString returned: sb = [" + sb + "]");
       }

       private void reverseEachWord(StringBuilder sb) {
           int start = 0;
           int end = 1;
           int n = sb.length();
           while (start < n) {
               while (end < n && sb.charAt(end) != ' ') {
                   end++;
               }
               reverseString(sb, start, end - 1);
               start = end + 1;
               end = start + 1;
           }
       }
   }
    static class Solution {
        public String reverseWords(String s) {
            char[] charArray=s.toCharArray();
            int left=0,right=s.length()-1;
            // 清除字符串两边的空格
            while(charArray[left]==' '){// 清除左边
                left++;
            }

            while(charArray[right]==' '){// 清除右边
                right--;
            }
            StringBuilder sb=new StringBuilder();
            // 开始添加单词
            while(left<=right){
                int index=right;
                // index 向左遍历找到第一个空格
                while(index>=left&&charArray[index]!=' '){
                    index--;
                }
                // 现在 index 已经找到第一个空格，i=index+1 后移到字符串出现的位置
                // 添加字符串
                for(int i=index+1;i<=right;i++){
                    sb.append(charArray[i]);
                }
                // 如果不是最后一个单词，就添加空格
                if(index>left) sb.append(' ');

                // 使用 index 指针 跳过中间可能出现的空格
                while(index>=left&&charArray[index]==' '){
                    index--;
                }
                // 把 right 放到下一个单词出现的位置，继续循环
                right=index;
            }
            return sb.toString();

        }
    }

    //剑指 Offer 05. 替换空格
    public String replaceSpace(String s) {

        if (s==null) return null;

        char[] sChar = s.toCharArray();

        StringBuilder sb = new StringBuilder();
        for (char c : sChar) {
            if (c == ' ') sb.append("%20");
            else sb.append(c);
        }
        return sb.toString();

    }

    //344. 反转字符串
    public void reverseString(char[] s) {
        int fast=s.length-1;
        int slow=0;
        while(slow<fast){
            char temp=s[slow];
            s[slow++]=s[fast];
            s[fast--]=temp;
        }
    }

     //704、二分查找
     public int search1(int[] nums, int target) {
         // 避免当 target 小于nums[0] 大于nums[nums.length - 1]时多次循环运算
         if (target < nums[0] || target > nums[nums.length - 1]) {
             return -1;
         }
         int l=0;
         int r=nums.length;  //左闭右开[l,r)
         // 因为left == right的时候，在[left, right)是无效的空间，所以使用 <
         while(l<r){
             int mid=l+((r-l)>>1);
             //target在左区间 在[left，right)中
             if(nums[mid]>target) r=mid;
             // target在右区间，在[mid+1，right)中
             else if(nums[mid]<target) l=mid+1;
             // nums[mid]=target，返回下标
             else return mid;
         }
         return -1;
     }
     //704、二分查找
    public int search2(int[] nums, int target) {
        // 避免当 target 小于nums[0] 大于nums[nums.length - 1]时多次循环运算
        if (target < nums[0] || target > nums[nums.length - 1]) {
            return -1;
        }
        int l=0;
        int r=nums.length-1; //左闭右闭[l,r]
        while(l<=r){//有可能取值到l=r
            // 中间值
            int mid = l + ((r-l)>>1);
            // target==nums[mid] 刚好找到
            if(target==nums[mid]) return mid;
                // target在右半区间 闭区间跳过边界值 所以l=mid+1
            else if(target>nums[mid]) l=mid+1;
                // target在左半区间 同理
            else r=mid-1;
        }
        return -1;
    }

    //35. 搜索插入位置
    public int searchInsert(int[] nums, int target) {
        int l=0,r=nums.length;  //左闭合 右开 [l,r)
        // 用于更新数组长度 为不在数组序列 返回的索引（左半区间更新）
        int len=nums.length;
        while(l<r){  //l不可能等于r
            int mid=l+((r-l)>>1);
            int res=nums[mid];
            if(res==target) return mid;
                // target在左半区 右开
            else if(res>target) {
                r=mid;
                len=mid;
            }
            // target在右半区间
            else l=mid+1;
        }
        return len;
    }

    //leetcode  4.寻找两个正序数组的中位数
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int p1 = 0;
        int p2 = 0;
        int k = 0;
        int N = nums1.length + nums2.length;
        int[] num = new int[N];
        for (int i = 0; i < N; i++) {
            if (p1 == nums1.length || p2 == nums2.length) {
                break;
            }
            if (nums1[p1] <= nums2[p2]) {
                num[k++] = nums1[p1++];
            } else {
                num[k++] = nums2[p2++];
            }
            //System.out.println(num[i]);
        }
        //System.out.println(Arrays.toString(num));
        while (p1 < nums1.length) {
            num[k++] = nums1[p1++];
        }
        while (p2 < nums2.length) {
            num[k++] = nums2[p2++];
        }
        //System.out.println(Arrays.toString(num));
        if (N % 2 == 0) {
            return (double) (num[N / 2] + num[(N / 2) - 1]) / 2;
        } else {
            return num[N / 2];
        }

    }

    //leetcode  2. 两数相加
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    //leetcode  2. 两数相加
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //创建一个新list
        ListNode root = new ListNode(0);
        //游标指针
        ListNode cursor = root;
        //进位
        int carry = 0;
        //当l1、l2、进位都有其一存在时 便满足
        while(l1 != null || l2 != null || carry != 0) {
            //从l1、l2中取出val、 为空时取出的则为0
            int l1Val = l1 != null ? l1.val : 0;
            int l2Val = l2 != null ? l2.val : 0;
            //l1+l2+进位=总和
            int sumVal = l1Val + l2Val + carry;
            //每次计算进位 下次循环＋上
            carry = sumVal / 10;
            //将个位存入 sumNode
            ListNode sumNode = new ListNode(sumVal % 10);
            //让cursor指向sumNode
            cursor.next = sumNode;
            //更新cursor
            cursor = sumNode;
            //如果不为空时 游标指针将指向下一结点
            if(l1 != null) l1 = l1.next;
            if(l2 != null) l2 = l2.next;
        }
        //返回结果链表的头结点
        return root.next;
    }

    //leetcode  1. 两数之和
    /*    有问题的方法  提前全部放进去 导致 map.containsKey  取出来的值为   map.containsKey方法底层为getNode() 他返回的是最后找的key
               getNode()函数代码：if (e.hash == hash &&
                                ((k = e.key) == key || (key != null && key.equals(k))))
                                return e;
                                */
    public static int[] twoSum2(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; ++i) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{map.get(target - nums[i]), i};
            }
        }
        return new int[0];
    }

    //leetcode  1. 两数之和
    //正确的方法
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> hashtable = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            if (hashtable.containsKey(target - nums[i])) {
                return new int[]{hashtable.get(target - nums[i]), i};//第一次找到答案就返回下标值
            }
            hashtable.put(nums[i], i); //key(数组值):value(数组下标)
        }
        return new int[0];//没结果返回空数组
    }

        // 347. 前 K 个高频元素
        public int[] topKFrequent(int[] nums, int k) {

            //使用哈希存储映射 元素:频率
            Map<Integer,Integer> occurrences = new HashMap<>();
            for(int num : nums){
                // 存在就+1 否则存1
                occurrences.put(num, occurrences.getOrDefault(num, 0) + 1);
            }

            // 升序采用大顶堆，降序采用小顶堆
            // 采用数据结构：java 小顶堆  使用小顶堆筛选前k个高频的元素，不能使用自然排序，需要重写比较的类函数
            // 队列采用int[] 数据类型 int[0] 代表数组的值、int[1] 代表该值出现的次数
            // 比较入队的元素int[1] 根据出现次数排序
            // 大顶堆 ：(o1, o2) -> o2[1] - o1[1]  后面的比前面大 需要交换  较大元素上浮形成大顶堆
            // 小顶堆 ：(o1, o2) -> o1[1] - o2[1] ==> Comparator.comparingInt(o -> o[1]) 前面比后面大 需要交换 较大元素下沉形成小顶堆

            // 比较器原理：这里o1表示位于前面的对象，o2表示后面的对象
            // 返回-1（或负数），表示不需要交换01和02的位置，o1排在o2前面，asc
            // 返回1（或正数），表示需要交换01和02的位置，o1排在o2后面，desc

            PriorityQueue<int[]> queue = new PriorityQueue<>(k,Comparator.comparingInt(o -> o[1])); //寻找前k个最小数，因此将小顶堆大小定义为k

            // 思路：遍历map，用一个大小为k的小顶堆保存当前找到的前k个最大数，如果下一个数组元素比堆顶大，那堆顶元素必然不是前k大，将堆顶元素出堆，此数组元素入堆
            for (Map.Entry<Integer,Integer> entry : occurrences.entrySet()){
                int num = entry.getKey();
                int count = entry.getValue();
                // 入队操作：判断队列大小是否等于 k
                if(queue.size() == k){
                    // 当堆顶元素值的频率 小于 插入新元素 时  否则不用入队
                    // 需要先弹出堆顶元素 再将新元素插入
                    if (queue.peek() != null && queue.peek()[1] < count){
                        queue.poll();
                        queue.offer(new int[]{num, count});
                    }
                }else {
                    queue.offer(new int[]{num, count});
                }
            }

            // 将队列中元素全部弹出 存入结果集并返回
            int[] res = new int[k];
            for (int i = 0; i < k; i++) {
                res[i] = Objects.requireNonNull(queue.poll())[0];
            }

            return res;
        }

}
