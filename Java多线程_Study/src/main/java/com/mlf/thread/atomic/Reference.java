package com.mlf.thread.atomic;

import java.util.concurrent.atomic.AtomicMarkableReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-07-28 18:46
 **/
public class Reference {

    public static void main(String[] args) {
        // 記錄每次的版本号
        AtomicStampedReference<Integer> balance = new AtomicStampedReference<>(0, 0);
        // 记录布尔值，
        AtomicMarkableReference<Integer> reference = new AtomicMarkableReference<>(0, false);
    }

}
