package t1;

import org.junit.Test;
import t2.Test2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-08-14 15:36
 **/
public class Test1 {
    public static void main(String[] args) {
        Test2 test2 = new Test2();
        //Test3 test3 = new Test3();  // 报错 default 不同包不能访问
        Integer i = 100;
        Double d = 100.00;
        System.out.println(i.intValue() == d.intValue());
        System.out.println(i.doubleValue() == d.doubleValue());

    }

    @Test
    public void test11() {
        int[][] ints = new int[1][];
        Arrays.sort(ints, (a, b) -> {
            if (a[1] == b[1]) return a[0] - b[0];
            return a[1] - b[1];
        });
        Comparable comparable;
        LinkedList<int[]> objects = new LinkedList<>();
        objects.toArray(new int[1][]);
    }

    @Test
    public void test() {
        int n = 127;
        System.out.println(++n);
        Object o = new Object();
        o.hashCode();
        HashSet<Object> set = new HashSet<>();
        set.add(1);
        set.add("1");
        String s = "";
        s.concat("a");
        new StringBuilder();
        boolean equals = o.equals(o); // 比较内存地址是否相等
    }
}
