package com.mlf.jvm.demo;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: Java基础
 * @description: 测试ByteBuffer流内存 用list集合
 * 方法区是JVM规范，jdk6中对方法的实现称为永久代
 * 方法区是JVM规范，jdk8中对方法的实现称为元空间
 * @author: mlf
 * @date: 2022-05-12 21:59
 **/
public class Demo_12 {

    static int _100Mb = 1024 * 1024 * 100;

    public static void main(String[] args) {

        List<ByteBuffer> list = new ArrayList<>();

        int i = 0;
        try {
            while (true) {
                ByteBuffer byteBuffer = ByteBuffer.allocateDirect(_100Mb);
                list.add(byteBuffer);
                i++;
            }
        } finally {
            System.out.println(i);
        }

    }

}
