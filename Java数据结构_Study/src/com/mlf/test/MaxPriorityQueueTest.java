package com.mlf.test;

import com.mlf.datastructure.priority.MaxPriorityQueue;

/**
 * @program: Java基础
 * @description: 最大优先队列
 * @author: mlf
 * @date: 2022-05-23 17:22
 **/
public class MaxPriorityQueueTest<T extends Comparable<T>> {

    public static void main(String[] args) {

        MaxPriorityQueue<String> queue = new MaxPriorityQueue<>(10);

        queue.insert("A");
        queue.insert("B");
        queue.insert("D");
        queue.insert("C");
        queue.insert("H");
        queue.insert("T");
        queue.insert("H");

        while (!queue.isEmpty()) {
            String s = queue.delMax();
            System.out.print(s + " ");
        }

    }

}









