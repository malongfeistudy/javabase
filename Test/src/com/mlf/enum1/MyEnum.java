package com.mlf.enum1;

public enum MyEnum implements myInter {

    A("a", 1) {
        @Override
        public void info() {
            System.out.println("我是A");
        }
    }, B("b", 2) {
        @Override
        public void info() {
            System.out.println("我是B");
        }
    };

    private final String name;
    private final int age;

    MyEnum(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "MyEnum{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

interface myInter {
    void info();
}
