package com.mlf.datastructure.linklist;

import java.util.Iterator;

public class LinkList<T> implements Iterable<T> {

    //记录头结点
    private final Node head;
    //记录链表的长度
    private int N;

    //结点类
    private class Node {
        //存储数据
        T item;
        //下一个结点
        Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

    public LinkList() {
        //初始化头结点
        this.head = new Node(null, null);
        //初始化元素个数
        this.N = 0;
    }

    //清空鏈表
    public void clear() {
        head.next = null;
        this.N = 0;
    }

    //链表长度
    public int length() {
        return N;
    }

    //判断是否为空
    public boolean isEmpty() {
        return N == 0;
    }

    //获取指定位置圆元素
    public T get(int i) {
        //从头结点开始
        Node n = head.next;
        //通过for循环来遍历 直到第i个元素
        for (int index = 0; index < i; index++) {
            n = n.next;
        }
        //返回i处的元素
        return n.item;
    }

    //向链表插入一个元素
    public void insert(T t) {
        //找出当前最后一个结点
        Node n = head;
        while (n.next != null) {
            n = n.next;
        }
        //让最后一个结点指向新结点
        Node node = new Node(t, null);
        n.next = node;
        //元素个数+1
        N++;
    }

    //在单链表i位置插入t元素
    public void insert(int i, T t) {
        //找到i-1位置结点
        Node pre = head;
        for (int index = 0; index <= i - 1; index++) {
            pre = pre.next;
        }
        //找到i位置结点
        Node curr = pre.next;
        //插入新结点元素  new一个新结点赋值给 i-1.next 让 newNode指向原来 i
        pre.next = new Node(t, curr);
        //元素个数+1
        N++;
    }

    //删除i处结点并返回元素
    public T remove(int i) {
        if (this.length()==0){
            return null;
        }else {
            //找到i-1位置结点
            Node pre = head;
            for (int index = 0; index <= i - 1; index++) {
                pre = pre.next;
            }
            //i位置结点
            Node curr = pre.next;
            //让 i-1 指向 i+1
            pre.next = curr.next;
            //元素个数-1
            N--;
            return curr.item;
        }
    }

    //查找元素t在链表中第一次出现的位置
    public int indexOf(T t) {
        //从头结点开始依次找到每一个结点的item与t比较
        Node n = head;
        for (int i = 0; n.next != null; i++) {
            n = n.next;
            if (n.item == t) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Iterator<T> iterator() {
        return new LiIterator();
    }
    private class LiIterator implements Iterator{
        private Node n;
        public LiIterator(){
            this.n=head;
        }
        @Override
        public boolean hasNext() {
            return n.next!=null;
        }

        @Override
        public Object next() {
            n=n.next;
            return n.next;
        }
    }

    //用来反转整个链表
    public void reverse(){
        //判断当前链表是否为空链表，如果是空链表，则结束运行，如果不是，则请调用重载的reverse方法完成反转
        if(isEmpty()) return;
        reverse(head.next);
    }

    //用来反转指定结点
    private Node reverse(Node curr) {

        if (curr.next==null){
            head.next=curr;
            return curr;//
        }

        //1 递归的反转curr的下一个结点；返回值就是链表反转后当前节点的上一个结点
        Node pre = reverse(curr.next);
        //2 让返回的结点的下一个结点变为当前结点curr
        pre.next=curr;
        //3 把当前结点的下一个结点变为null
        curr.next=null;

        return curr;
    }

}






