package com.mlf.face;

/**
 * @program: Java基础
 * @description: 传值和引用
 * @author: mlf
 * @date: 2022-05-12 23:58
 **/
public class ValueAndCite {

    public static void main(String[] args) {

        int x = 10;
        modify(x);
        System.out.println("main: " + x);

    }

    static void modify(int num) {
        num += 10;
        System.out.println("modify: " + num);
    }

}