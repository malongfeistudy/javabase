package com.mlf.thread;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-11 18:27
 **/
public class Demo_Test {

    public static void main(String[] args) {

        //一个账户
        Customs c = new Customs();

        //两个人同时存储
        new Thread(c).start();
        new Thread(c).start();

    }

}

//银行类：实现存款功能
class Bank {
    private int sum;

    public void add(int num) {
        synchronized (this) {
            sum += num;
            System.out.println("sum= " + sum);
        }
    }

}

//账户类  实现Runnable接口可一只创建一个对象，从而再去多线程运行，达到共享的效果
class Customs implements Runnable {

    private final Bank b = new Bank();

    @Override
    public void run() {

        for (int i = 0; i < 3; i++) {
            b.add(100);
        }

    }
}

//继承Thread类：会在需要资源共享的情况下会造成数据不同步
class Customs2 extends Thread {

    public void run() {

    }

}








