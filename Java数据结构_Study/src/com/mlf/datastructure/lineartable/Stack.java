package com.mlf.datastructure.lineartable;

import java.util.Iterator;

/**
 * @program: Java基础
 * @description: 栈数据结构
 * @author: mlf
 * @date: 2022-05-25 22:00
 **/
public class Stack<T> implements Iterable<T> {
    //记录首结点
    private Node head;
    //栈中元素的个数
    private int N;

    public Stack() {
        head = new Node(null, null);
        N = 0;
    }

    //判断当前栈中元素个数是否为0
    public boolean isEmpty() {
        return N == 0;
    }

    public void push(T t) {
        Node oldNext = head.next;
        Node node = new Node(t, oldNext);
        head.next = node; //个数+1
        N++;
    }

    public T pop() {
        Node oldNext = head.next;
        if (oldNext == null) {
            return null;
        }
        head.next = head.next.next;
        N--;
        return oldNext.item;
    }

    public int size() {
        return N;
    }

    @Override
    public Iterator<T> iterator() {
        return new SIterator();
    }

    private class SIterator implements Iterator<T> {
        private Node n = head;

        @Override
        public boolean hasNext() {
            return n.next != null;
        }

        @Override
        public T next() {
            Node node = n.next;
            n = n.next;
            return node.item;
        }
    }

    private class Node {
        public T item;
        public Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
