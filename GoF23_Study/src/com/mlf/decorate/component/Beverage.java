package com.mlf.decorate.component;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-01 18:08
 **/
public interface Beverage {
    double cost();
}


