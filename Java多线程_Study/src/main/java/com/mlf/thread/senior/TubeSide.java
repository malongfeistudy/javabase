package com.mlf.thread.senior;

/**
 * @program: 数据结构与算法
 * @description: 管程法模拟
 * @author: mlf
 * @date: 2022-04-17 23:27
 **/
public class TubeSide {
    public static void main(String[] args) {
        SynContainer container = new SynContainer();

        new Thread(new Productor(container)).start();
        new Thread(new Consumer(container)).start();

    }
}


//生产者
class Productor extends Thread{

    SynContainer container;
    public Productor(SynContainer container){
        this.container=container;
    }

    //生产
    @Override
    public void run() {
        for (int i = 1; i < 101; i++) {
            container.push(new Chicken(i));
            System.out.println("生产了第"+i+"只鸡");
        }
    }
}


//消费者
class Consumer extends Thread{

    SynContainer container;
    public Consumer(SynContainer container){
        this.container=container;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("消费了第"+container.pop().id+"只鸡");
        }
    }
}


//产品 面向对象封装：私有属性 构造方法 get set方法 set内可加条件限制
class Chicken {
    int id;
    public Chicken(int id) {
        this.id = id;
    }
}


//缓冲区 生产者放 消费者取
class SynContainer{

    //容器大小
    Chicken[] chickens=new Chicken[100];
    //计数器
    int count=0;

    //生产者把产品放入容器  满了就通知消费者消费
    public synchronized void push(Chicken chicken){

        if (count==chickens.length-1){
            //生产者等待 消费者消费
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //容器没满就生产
        chickens[count++]=chicken;
        //void notify()  唤醒在该对象上等待的某个线程
        //void notifyAll()  唤醒在该对象上等待的所有线程
        //通知消费者消费
        this.notifyAll();
        //this.notify();
    }

    public synchronized Chicken pop(){

        //没有产品时等待生产者生产
        if (count==0){
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Chicken chicken = chickens[--count];
        //消费过后通知生产者生产
        this.notifyAll();
        //返回当前产品
        return chicken;
    }

}
