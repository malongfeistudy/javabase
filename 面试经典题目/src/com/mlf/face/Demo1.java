package com.mlf.face;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-10-02 19:30
 **/
public class Demo1 {

    public static void main(String[] args) throws InterruptedException {
        Container container = new Container("马斯克一号", 10000);
        Monitoring monitoring = new Monitoring(container);
        new Thread(monitoring).start();
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    try {
                        Thread.sleep(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    container.add(j);
                }
            }).start();
        }
    }

}

class Monitoring implements Runnable {

    private final Container container;

    public Monitoring(Container container) {
        this.container = container;
    }


    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (container.getCount() == 1000) {
                System.out.println("===容器 " + this.container.getName() + "===已经添加了" + container.getCount() + "个元素");
                break;
            } else {
                System.out.println("容器" + this.container.getName() + "计数器count=" + container.getCount());
            }
        }
    }
}

class Container {

    private Object[] array; // 容器采用object数组存储
    private int count; // 计数器，可表示当前数组中已存储元素数量
    private int size; // 初始化数组大小
    private String name;

    public Container(String name, int size) {
        this.name = name;
        this.array = new Object[size];
        this.size = size;
        this.count = 0;
    }

    // 添加方法
    public void add(Object obj) {
        array[this.count] = obj;
        this.count++; // 存储完成之后加一
    }

    // 获取当前元素数量
    public int getCount() {
        return this.count;
    }

    public void setArray(Object[] array) {
        this.array = array;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object[] getArray() {
        return array;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}















