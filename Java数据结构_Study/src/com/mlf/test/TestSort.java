package com.mlf.test;

import java.util.Arrays;

import static com.mlf.datastructure.sort.Sort.*;
import static com.mlf.test.TestSort.Utils.*;

public class TestSort {

    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;

        long start = System.currentTimeMillis();
        // 把方法a和方法b比对很多次来验证方法a是否正确
        for (int i = 0; i < testTime; i++) {
            Integer[] arr1 = generateRandomArray(maxSize, maxValue);
            Integer[] arr2 = copyArray(arr1);

            //SelectSort.SelectSort(arr1);//3082ms
            BubbleSort.bubbleSort(arr1);//4967ms
            //Insertion.insertionSort(arr1);//2662ms
            //ShellSort.shellSort(arr1);//2425ms
            //QuickSort.sort(arr1);//2545ms

            comparator(arr2);//Arrays.sort(arr)
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
        long end = System.currentTimeMillis();
        System.out.println(end-start+"ms");

    }


public static class Utils{
    //1、实现一个绝对正确但是复杂度不好的方法B，使用的是JDK提供的Arrays类的排序方法
    public static void comparator(Integer[] arr) {
        Arrays.sort(arr);
    }

    //2、实现一个随机样本产生器
    public static Integer[] generateRandomArray(int maxSize, int maxValue) {
        //产生随机数范围为[0,maxSize]
        int length=(int) ((maxSize + 1) * Math.random());
        Integer[] arr = new Integer[length];
        for (int i = 0; i < arr.length; i++) {
            //产生[-maxValue,maxValue]的元素
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }
    //拷贝数组
    public static Integer[] copyArray(Integer[] arr) {
        if (arr == null) {
            return null;
        }
        Integer[] res = new Integer[arr.length];
        System.arraycopy(arr, 0, res, 0, arr.length);
        return res;
    }

    //3、实现比对的方法
    public static boolean isEqual(Integer[] arr1, Integer[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (!arr1[i].equals(arr2[i])) {
                return false;
            }
        }
        return true;
    }

    //5、如果有一个样本使得比对出错，打印样本分析是哪个方法出错
    public static void printArray(Integer[] arr) {
        if (arr == null) {
            return;
        }
        for (int j : arr) {
            System.out.print(j + " ");
        }
        System.out.println();
    }
}


}