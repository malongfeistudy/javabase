package com.mlf.jvm.demo;

import org.junit.Test;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-10 17:24
 **/
public class demo_7 {

    //StringTable["a", "b", "ab" ]
    public static void main(String[] args) {

        //vm调用StringBuild.append("a").append("b").toString() 最终相当于变成 new String("ab")
        String s = new String("a") + new String("b");  //简化等同于  String s = new String("ab");

        //*****线程此时串池中对象StringTable["a", "b"]

        //堆内存： new String("a")   new String("b")   new String("ab")

        System.out.println("ab" == s);  //false  串池中没有"ab",此次会将 "ab"常量放入串池 因为 s在堆内存中，"ab"在串池中内存地址不同

        //****此时串池中对象StringTable["a", "b", "ab"]

        //s.intern尝试将字符串对象s放入串池中 ：如果串池中有，则不会放入；若没有，放入, 最终会返回串池中的对象
        String s1 = s.intern();  //不会放入s，因为串池中已经存在 "ab"

        System.out.println("ab" == s1);  //true   都是串池中的常量对象 ab

        System.out.println("ab" == s);  //false   s为堆内存地址 "ab"为串池中

    }


    //StringTable["a", "b", "ab" ]
    @Test
    public void test() {

        //vm调用StringBuild.append("a").append("b").toString() 最终相当于变成 new String("ab")
        String s = new String("a") + new String("b");  //简化等同于  String s = new String("ab");

        //*****线程此时串池中对象StringTable["a", "b"]

        //堆内存： new String("a")   new String("b")   new String("ab")

        //s.intern尝试将字符串对象s放入串池中 ：如果串池中有，则不会放入；若没有，放入, 最终会返回串池中的对象
        String s1 = s.intern();  //放入s，因为串池中不存在 "ab" 之前代码行中未出现常量 "ab"

        System.out.println("ab" == s1);  //true   都是串池中的常量对象 ab

        //****此时串池中对象StringTable["a", "b", "ab"]

        System.out.println("ab" == s);   //true   s、"ab"为串池中同一个常量

    }

}
