package com.mlf.demo1.finaltest;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-06-14 11:44
 **/
public class FinalTest {

    public static void main(String[] args) {
        B b = new B();
        A a = b;
        a.test();
        a.test();
        b.test(1);
    }

}


class A {
    public final int test() {
        return 1;
    }

    // final方法不可重写，但是可以重载
    public final int test(int s) {
        return s;
    }

}

class B extends A {
    // final方法不可重写，但是可以重载
    // public final int test(int i){
    //    i=2;
    //    return i;
    //}
}