package com.mlf.datastructure.priority;

/**
 * @program: Java基础
 * @description: 索引最小优先队列
 * @author: mlf
 * @date: 2022-05-23 17:22
 **/

public class IndexMinPriorityQueue<T extends Comparable<T>> {

    private T[] items; // 存储元素
    private int[] pq;  // 存储items里的索引（堆调整后的顺序） 保证item有序，以下操作的是pq[]数组
    private int[] qp;  // pq数组的反转（索引和值）存储pq索引，它的索引是元素索引
    private int N;

    public IndexMinPriorityQueue(int capacity) {
        this.items = (T[]) new Comparable[capacity + 1]; // 初始化
        this.pq = new int[capacity + 1];
        this.qp = new int[capacity + 1];
        this.N = 0;

        //Arrays.fill(qp, -1); // 填充qp数组全为 -1 初始的时候没有数据/
        for (int i = 0; i < qp.length; i++) {
            qp[i] = -1;
        }
    }

    // 判断堆中索引处元素 i<j
    private boolean less(int i, int j) {
        return items[pq[i]].compareTo(items[pq[j]]) < 0;
    }

    public int size() {
        return N;
    }

    public boolean isEmpty() {
        return N == 0;
    }

    // 交换pq[] 中 i和 j处存储的items索引
    private void exch(int i, int j) {
        // 交换pq中元素
        int temp = pq[i];
        pq[i] = pq[j];
        pq[j] = temp;

        // 更新qp中元素
        qp[pq[i]] = i;
        qp[pq[j]] = j;
    }

    // 判断k对应的元素是否存在
    public boolean contains(int k) {
        return qp[k] != -1;
    }


    // 返回最小元素的索引
    public int minIndex() {
        return pq[1];
    }

    // 往队列插入一个元素t，并关联索引i
    public void insert(int i, T t) {
        if (contains(i)) return;
        N++;
        items[i] = t;
        pq[N] = i; // 记录items中元素索引
        qp[i] = N; //记录pq反转
        swim(N);   // 元素上浮，堆调整为有序
    }


    // 删除最小元素索引并且返回，相当于出队
    public int delMin() {
        // 获取最小元素索引
        int minIndex = pq[1];

        // 交换pq中索引 1 处 和 N 处索引
        exch(1, N);

        // 删除qp中对应内容
        qp[pq[N]] = -1;

        //删除pq中最大索引处的值（交换后的最小元素索引）
        pq[N] = -1;

        // 删除items中元素内容
        items[minIndex] = null;

        N--; //元素个数--

        sink(1); //对 pq[] 进行下沉调整，实际是items[pq[k]]

        return minIndex;
    }


    // 删除索引i关联的元素
    public void delete(int i) {

        // 找到i在pq中的索引k， qp[i]表示存储在pq中items[i]元素的索引
        int k = qp[i];

        // 交换pq中k和N
        exch(k, N);

        // 删除qp中的内容，  删除pq中的内容，数量-1 ， 删除items中i处元素
        qp[pq[N]] = -1;
        pq[N] = -1;
        items[i] = null;
        N--;

        // 调整堆，先上浮，再下沉
        sink(k);
        swim(k);
    }

    // 把与索引i关联的元素修改为t
    public void changeItem(int i, T t) {
        // 修改元素值
        items[i] = t;
        // 找出pq[]中索引
        int k = qp[i];
        // 对pq进行堆调整
        sink(k);
        swim(k);
    }

    // 上浮算法
    private void swim(int k) {
        while (k > 1) {
            if (less(k, k / 2)) exch(k, k / 2);
            k /= 2;
        }
    }

    // 下沉算法
    public void sink(int k) {
        while (2 * k <= N) {
            int min;
            if (2 * k + 1 <= N) {
                if (less(2 * k, 2 * k + 1)) min = 2 * k;
                else min = 2 * k + 1;
            } else min = 2 * k;

            if (less(k, min)) break;

            exch(k, min);

            k = min;
        }
    }

}









