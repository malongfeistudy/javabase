package com.mlf.test;

import com.mlf.datastructure.linklist.LinkList;

import java.util.LinkedList;

public class LinkListTest {
    public static void main(String[] args) {
        LinkList<String> s1=new LinkList<>();
        s1.insert("我是谁");
        s1.insert("我在哪");
        s1.insert("咋办呀");
        s1.insert("干什么");
        System.out.println("---------------------");
        for (int i=0;i<s1.length();i++){
            System.out.println(s1.get(i));
        }
        System.out.println("---------------------");
        int indexOf = s1.indexOf("咋办呀");
        System.out.println(indexOf);//2

        System.out.println("---------------------");
        s1.insert(2,"找不到工作");
        System.out.println(s1.get(2));
        System.out.println(s1.get(3));

        System.out.println("---------------------");
        System.out.println(s1.length());
        String remove = s1.remove(s1.length() - 1);
        System.out.println(remove);
        System.out.println(s1.length());

        System.out.println("---------------------");
        s1.clear();
        System.out.println(s1.length());
        System.out.println(s1.isEmpty());

    }

    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null || nums.length < 2) return nums;
        // 双向队列 保存当前窗口最大值的数组位置 保证队列中数组位置的数按从大到小排序
        LinkedList<Integer> list = new LinkedList<>();
        // 结果数组
        int[] result = new int[nums.length - k + 1];
        for (int i = 0; i < nums.length; i++) {
            // 保证从大到小 如果前面数小 弹出
            while (!list.isEmpty() && nums[list.peekLast()] <= nums[i]) {
                list.pollLast();
            }
            // 添加当前值对应的数组下标
            list.addLast(i);
            // 初始化窗口 等到窗口长度为k时 下次移动再删除过期数值
            if (list.peek() <= i - k) {
                list.poll();
            }
            // 窗口长度为k时 再保存当前窗口中最大值
            if (i - k + 1 >= 0) {
                result[i - k + 1] = nums[list.peek()];
            }
        }
        return result;
    }
}
