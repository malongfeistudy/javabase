package com.mlf.test;

import com.mlf.pojo.Person;

import java.io.*;

/**
 * @program: Java基础
 * @description: 通过ObjectOutputStream 的writeObject()方法把这个类的对象写到一个地方（文件），
 *               再通过ObjectInputStream 的readObject()方法把这个pojo(person)对象读出来
 * @author: mlf
 * @date: 2022-04-21 18:06
 **/
public class ReadWritePojoTest {

    public static void main(String[] args) throws IOException {

        File file = new File("out.txt");

        if (!file.exists()) {

            //if (file.mkdir()){
            //    System.out.println("文件目录directory"+file+"创建成功！！");
            //}

            if (file.createNewFile()) {
                System.out.println("文件file" + file + "创建成功！！");
            }

        } else System.out.println("文件" + file + "已存在！！！");
        //删除文件 该文件夹目录下不能包含文件夹目录 只能包含文件file
        //if (file.delete()) {
        //    System.out.println("文件"+file+"删除成功！！");
        //}


        //输出流 写入txt
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(fos);

                Person person = new Person("tom", 22,10);
                System.out.println(person);
                oos.writeObject(person);            //写入对象
                oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    oos.close();
                } catch (IOException e) {
                    System.out.println("oos关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                System.out.println("fos关闭失败：" + e.getMessage());
            }
        }

        //输入流 读取txt
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(fis);
                try {
                    Person person = (Person) ois.readObject();   //读出对象
                    System.out.println(person);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    ois.close();
                } catch (IOException e) {
                    System.out.println("ois关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                System.out.println("fis关闭失败：" + e.getMessage());
            }
        }
    }
}
