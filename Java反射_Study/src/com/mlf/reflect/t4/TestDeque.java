package com.mlf.reflect.t4;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-09-21 08:13
 **/
public class TestDeque {
    public static void main(String[] args) {
        Deque<Character> deque = new LinkedList<>();
        deque.push('a');
        deque.push('b');
        deque.push('c');
        // System.out.println(deque.peek());  // peek返回队头
        //System.out.println(deque.pop()); // pop() 返回队头 并且删除

        //System.out.println(removeDuplicates("ababab"));

        //System.out.println(removeDuplicate1s("abbaca"));

        //String  s1 = "1_2";
        //System.out.println(s1.split("_").length);
        //String  s2 = "_2";
        //String  s3 = "2_";
        //System.out.println(s3.split("_").length);
        //String[] s = s3.split("_");
        ////System.out.println(s.length);
        //for (String s1 : s) {
        //    System.out.println(s1);
        //}
        System.out.println();

    }

    public static String removeDuplicate1s(String S) {
        char[] s = S.toCharArray();
        int top = -1;
        for (int i = 0; i < S.length(); i++) {
            if (top == -1 || s[top] != s[i]) {
                s[++top] = s[i];
            } else {
                top--;
            }
        }
        return String.valueOf(s, 0, ++top);
    }

    public static String removeDuplicates(String s) {   // abbaca
        Deque<Character> deque = new LinkedList<>();
        char ch;
        for (int i = 0; i < s.length(); i++) {
            ch = s.charAt(i);
            if (deque.isEmpty() || deque.peek() != ch) {
                deque.push(ch);
            } else {
                deque.pop();
            }
        }

        StringBuilder sb = new StringBuilder();
        //剩余的元素即为不重复的元素
        while (!deque.isEmpty()) {
            sb.append(deque.removeLast());
        }
        return sb.toString();
    }
}
