package com.mlf.zidy.demo2;

import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @program: Java基础
 * @description:
 * @author: mlf
 * @date: 2022-05-30 17:00
 **/

public class TestMethodAnnotation {

    @Override
    @MyMethodAnnotation(description = "toStringMethod", title = "override toString method")
    public String toString() {
        return "重写toString方法···";
    }

    @Deprecated
    @MyMethodAnnotation(description = "old static method", title = "old static method")
    public static void oldMethod() {
        System.out.println("old static method, don't use it");
    }

    @SuppressWarnings({"unchecked"})
    @MyMethodAnnotation(title = "test method", description = "suppress warning static method")
    public static void genericsTest() throws FileNotFoundException {
        ArrayList l = new ArrayList();
        l.add("test");
        oldMethod();
    }

    public static void main(String[] args) {

        Method[] methods = TestMethodAnnotation.class.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyMethodAnnotation.class)) {
                Annotation[] annotations = method.getDeclaredAnnotations();
                System.out.println("--------------------");
                // 获取并遍历方法上的所有注解
                for (Annotation annotation : annotations) {
                    System.out.println("method：" + method + " annotation: " + annotation);
                }

                // 获取MyMethodAnnotation对象信息
                MyMethodAnnotation annotation = method.getAnnotation(MyMethodAnnotation.class);
                System.out.println(annotation.title() + ": " + annotation.description());
            }
        }

    }

}
