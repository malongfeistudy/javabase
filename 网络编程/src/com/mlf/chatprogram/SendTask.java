package com.mlf.chatprogram;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * @program: Java??
 * @description: ????
 * @author: mlf
 * @date: 2022-05-11 21:14
 **/
public class SendTask implements Runnable {

    private int sendPort; //????????

    public SendTask(int sendPort) {
        this.sendPort = sendPort;
    }

    @Override
    public void run() {

        DatagramSocket datagramSocket = null;

        try {
            datagramSocket = new DatagramSocket();

            Scanner scanner = new Scanner(System.in);

            while (true) {
                String data = scanner.nextLine();
                byte[] dataBytes = data.getBytes();
                DatagramPacket datagramPacket = new DatagramPacket(dataBytes, dataBytes.length, InetAddress.getByName("192.168.0.255"), sendPort);
                datagramSocket.send(datagramPacket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            datagramSocket.close();
        }

    }


}











