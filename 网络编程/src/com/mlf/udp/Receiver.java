package com.mlf.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @program: Java基础
 * @description: UDP网络程序接收端
 * @author: mlf
 * @date: 2022-05-11 20:31
 **/
public class Receiver {

    public static void main(String[] args) {

        //字节数组，用于接收数据
        byte[] buf = new byte[1024];
        DatagramSocket datagramSocket = null;

        try {
            //新建一个DatagramSocket对象用于接收数据包，监听端口号为8954
            datagramSocket = new DatagramSocket(8954);

            //接收到数据并封装在byte数组中
            DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length);

            System.out.println("等待接收数据");

            datagramSocket.receive(datagramPacket); //接收数据并填充到datagramPacket中，没有数据将会一直阻塞

            //获取接收到的数据，包括数据内容、长度、发送端的IP地址和端口号
            String info = new String(datagramPacket.getData(), 0, datagramPacket.getLength()) + "from "
                    + datagramPacket.getAddress().getHostAddress() + ":" + datagramPacket.getPort();

            System.out.println(info); //打印数据
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            datagramSocket.close(); //关闭资源
        }

    }

}












