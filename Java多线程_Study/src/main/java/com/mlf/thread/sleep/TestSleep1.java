package com.mlf.thread.sleep;

public class TestSleep1 implements Runnable {

    private boolean flag = true;

    @Override
    public void run() {
        int i = 0;
        while (flag) {
            System.out.println("run.....Thread" + (i++));
        }
    }

    public void stop() {
        this.flag = false;
    }

    public static void main(String[] args) {
        TestSleep1 sleep1 = new TestSleep1();
        new Thread(sleep1).start();
        for (int i = 0; i < 1000; i++) {
            System.out.println("main" + i);
            if (i == 900) {
                sleep1.stop();
            }
        }
    }
}
