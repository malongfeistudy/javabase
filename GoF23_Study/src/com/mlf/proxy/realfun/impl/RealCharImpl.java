package com.mlf.proxy.realfun.impl;

import com.mlf.proxy.realfun.RealCharInter;

/**
 * @program: Java基础
 * @description: 真实角色
 * @author: mlf
 * @date: 2022-06-27 11:22
 **/
public class RealCharImpl implements RealCharInter {


    public final void RealCharFunComm(String name) {
        System.out.println("RealCharFunComm实现类" + name);
    }

    public void RealCharFunComm1() {
        System.out.println("RealCharFunComm1");
    }

    static void testStatic() {
        System.out.println("testStatic");
    }

}
