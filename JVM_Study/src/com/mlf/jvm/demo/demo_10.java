package com.mlf.jvm.demo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * @program: Java基础
 * @description: list.add(line.intern ()) 和 list.add(line) 之间区别在于 后者会有垃圾回收来回收不用存入串池中的串对象起到优化作用
 * @author: mlf
 * @date: 2022-05-10 23:12
 **/
public class demo_10 {

    public static void main(String[] args) throws IOException {

        ArrayList<String> list = new ArrayList<>();
        System.in.read();

        //多次循环输入流读取文件file.txt
        for (int i = 0; i < 10; i++) {

            try (BufferedReader reader =
                         new BufferedReader(
                                 new InputStreamReader(
                                         new FileInputStream("JVM_Study/file.txt"),
                                         StandardCharsets.UTF_8))) {

                String line = null;
                long start = System.nanoTime();
                while (true) {
                    line = reader.readLine();  //读取每一行内容
                    if (line == null) {
                        break;
                    }
                    //line.intern();  //写入串池
                    list.add(line.intern());   //加入数组
                }
                System.out.println("cost:" + (System.nanoTime() - start) / 1000000 + "ms");
            }

            System.in.read();

        }

    }

}
