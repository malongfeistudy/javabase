package com.mlf.datastructure.tree;

import java.util.Arrays;

/**
 * @program: Java基础
 * @description: 并查集
 * @author: mlf
 * @date: 2022-06-01 15:03
 **/
public class UF_Tree_Weight {

    //记录结点元素和该元素所在分组的标识
    private int[] eleAndGroup;
    //记录并查集中数据的分组个数
    private int count;
    //存储每个根结点对应的树中元素的个数
    private int[] sz;

    //初始化并查集
    public UF_Tree_Weight(int N) {
        eleAndGroup = new int[N];
        sz = new int[N];
        Arrays.fill(sz, 1);
        for (int i = 0; i < N; i++) {
            eleAndGroup[i] = i;
        }
        this.count = N;
    }

    // 获取当前并查集中的数据有多少个分组
    public int getCount() {
        return count;
    }

    // 判断连个元素是否在同一分组中
    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    // 元素p所在分组的标识符
    public int find(int p) {
        while (true) {
            if (eleAndGroup[p] == p)
                return eleAndGroup[p];

            p = eleAndGroup[p];
        }
    }


    // 把元素q、p所在分组合并
    public void union(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (connected(pRoot, qRoot)) return;

        if (sz[pRoot] > sz[qRoot]) {
            eleAndGroup[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else {
            eleAndGroup[pRoot] = qRoot;
            sz[qRoot] += pRoot;
        }

        count--;

    }


}













