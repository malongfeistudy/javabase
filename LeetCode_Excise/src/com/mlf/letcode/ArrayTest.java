package com.mlf.letcode;

import org.junit.Test;

import java.util.*;

public class ArrayTest {

    @org.junit.Test
    public void test18(){
        int[] ints = {1, 2, 3, 4, -1, -9, -10, 10, 9, 9, -9, -1, -2, -3, -4};
        List<List<Integer>> list = threeSum(ints);
        for (List<Integer> integers : list) {
            System.out.println(integers);
        }
    }


    @org.junit.Test
    public void test206(){

        HashSet<Integer> hashSet = new HashSet<>();
        //hashSet.contains()
//hashSet.add()
        System.out.println(Arrays.toString(intersection(new int[]{1, 2}, new int[]{1, 2})));

    }
    @org.junit.Test
    public void test209() {
        int[] nums = {1, 1 ,1,1 ,2};
        int i = minSubArrayLen1(nums, 3);
        System.out.println(i);

        int i2 = minSubArrayLen2(nums, 3);
        System.out.println(i2);
    }

    public int[] intersection(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>(nums1.length);
        // 将 nums1 出现的数值及频次放入映射中
        for (int num : nums1) {
            Integer count = map.get(num);
            if (count == null) {
                map.put(num, 1);
            } else {
                map.put(num, ++count);
            }
        }
        List<Integer> list = new ArrayList<>();
        for (int num : nums2) {
            // 获取映射中该数值出现的频次
            Integer count = map.get(num);
            if (count != null && count != 0) {
                list.add(num);
                // 注意每次匹配后，该数值的频次需要减 1（nums1 和 nums2 匹配的数值的频次要相同）
                map.put(num, --count);
            }
        }
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }

    @Test
    public void testJzOffer03(){
        int[] nums = new int[]{2, 3, 1, 0, 2, 5, 3};
        findRepeatNumber(nums);
        findRepeatNumber2(nums);
    }

    //剑指 Offer 03. 数组中重复的数字
    public int findRepeatNumber(int[] nums) {
        int[] arr = new int[nums.length];
        for(int i = 0;i < nums.length;i++){
            arr[nums[i]]++;
            if(arr[nums[i]] > 1) {
                System.out.println(nums[i]);
                //return nums[i];
            }
        }
        return -1;
    }

    //剑指 Offer 03. 数组中重复的数字
    public int findRepeatNumber2(int[] nums) {

        HashSet<Integer> hashSet = new HashSet<>();

        for (int i : nums) {
            if (hashSet.contains(i)) System.out.println(i);
            hashSet.add(i);
        }

        return -1;
    }

    //209、长度最小的子数组 滑动窗口O(n)
    public static int minSubArrayLen2(int[] nums,int s) {
        int length = 0;//最终结果
        int sum = 0; //子序列之和
        int k = 0; //滑动窗指针
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            //更新k指针
            while (sum >= s) {
                length = length == 0 ? i - k + 1 : Math.min(length, i - k + 1);
                sum -= nums[k++]; //体现出来滑动窗口的精髓之处
            }
        }
        return length;
    }

    //209、长度最小的子数组 暴力O(n^2)
    public static int minSubArrayLen1(int[] nums,int s) {
        int length = 0;//最终结果
        for (int i = 0; i < nums.length; i++) {
            int sum = 0;//子序列和
            for (int j=i; j<nums.length;j++) {
                sum+=nums[j];
                if (sum >= s) {
                    length = length == 0 ? j - i + 1 : Math.min(length, j - i + 1);
                    break;
                }
            }
        }
        return length;
    }


    //142. 环形链表 II
    /**
     * Definition for singly-linked list.
     * class ListNode {
     *     int val;
     *     ListNode next;
     *     ListNode(int x) {
     *         val = x;
     *         next = null;
     *     }
     * }
     */
    public class Solution {
        public ListNode detectCycle(ListNode head) {

            ListNode fast=head;
            ListNode slow=head;
            ListNode temp=null;
            // 如果链表无环，则返回 null
            if(head==null) return null;

            while(fast!=null&&fast.next!=null){
                fast=fast.next.next;
                slow=slow.next;
                if(fast==slow){
                    temp=head;
                    while(true){
                        if(temp==slow) break; //先判断 有可能是头结点
                        temp=temp.next;
                        slow=slow.next;
                    }
                    break;
                }
            }
            return temp;
        }
    }

    //206. 反转链表
    //1、递归
    public ListNode reverseList1(ListNode head){
        return reverse(null,head);
    }

    private ListNode reverse(ListNode pre, ListNode curr) {
        if (curr==null){
            return pre;
        }
        ListNode next = curr.next;
        curr.next=pre;
        pre=curr;
        curr=next;
        return reverse(pre,curr);
    }

    //2、迭代
    public ListNode reverseList2(ListNode head){
        ListNode curr=head;
        ListNode pre=null;
        while (head!=null){
            ListNode next = curr.next;
            curr.next=pre;
            pre=curr;
            curr=next;
        }
        return pre;
    }

    static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }


    //18、四数之和
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> list=new ArrayList<>();
        int length=nums.length;
        if(length < 4) return list;
        Arrays.sort(nums);
        for(int i=0;i<length-3;i++){  //不用判断最后三个数
            if(i > 0 && nums[i] == nums[i-1])    continue;
            if ((long) nums[i] + nums[i + 1] + nums[i + 2] + nums[i + 3] > target) {
                break;
            }
            if ((long) nums[i] + nums[length - 3] + nums[length - 2] + nums[length - 1] < target) {
                continue;
            }
            for(int k=i+1;k<length-2;k++){
                // 这种剪枝是错误的，这道题目target 是任意值
                // if(nums[i] + nums[k] > target) return list;
                if(k>i+1 && nums[k] == nums[k-1]) continue;
                if ((long) nums[i] + nums[k] + nums[k + 1] + nums[k + 2] > target) {
                    break;
                }
                if ((long) nums[i] + nums[k] + nums[length - 2] + nums[length - 1] < target) {
                    continue;
                }
                for(int left=k+1,right=length-1;left<right;){
                    int sum = nums[i]+nums[k]+nums[left]+nums[right];
                    if(sum==target) {
                        list.add(Arrays.asList(nums[i],nums[k],nums[left],nums[right]));
                        while(left<right && nums[left]==nums[left+1]) left++;
                        while(left<right && nums[right]==nums[right-1]) right--;
                        left--;right--;
                    }else if(sum<target) left++;
                    else right--;
                }
            }
        }
        return list;
    }

    //15、三数之和
    public static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        if (nums.length < 3) return list;  //若不够三个数 则直接返回
        Arrays.sort(nums);   //让nums数组有序
        for (int i = 0; i < nums.length; i++) { //首先让nums[i]成为第一个不变量
            if (nums[i] > 0) break;  //若nums[i]开始大于0 则不用往后找了（因为后边都大于0）
            if (i > 0 && nums[i] == nums[i - 1]) continue;  //当nums[i]和前一个数相等时进行下一轮循环
            for (int j=i+1,k=nums.length-1;j<k;) {  //内存循环 找第二个和第三个数 j为左指针 k为右指针 双指针从两头同时找
                int sum = nums[i] + nums[j] + nums[k];  //定义和
                if (sum == 0) { //等于0时
                    //加入list
                    list.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    //继续找下一组 j++ k-- 直到j=k停止
                    while (j < k && nums[j] == nums[j + 1]) j++; //两个数相等时左重置
                    while (j < k && nums[k] == nums[k - 1]) k--; //右重置
                    j++; //右移动
                    k--; //左移动
                }else if (sum<0) j++;  //小了左指针往右移动 j++
                 else k--;  //大了右指针往左移动 k--
            }
        }
        return list;
    }

    //27、移除元素 快慢指针
    public static int removeElement1(int[] nums, int val) {
        int slowIndex=0;
        for (int fastIndex = 0; fastIndex < nums.length; fastIndex++) {
            if (val!=nums[fastIndex]){
                nums[slowIndex++]=nums[fastIndex];
            }
            //System.out.println(slowIndex);
        }
        return slowIndex;
    }
    //27、移除元素 暴力
    public static int removeElement(int[] nums, int val) {
        int size = nums.length;

        for (int i = 0; i < size; i++) {
            if (nums[i] == val) {
                if (size - (i + 1) >= 0)
                    System.arraycopy(nums, i + 1, nums, i + 1 - 1, size - (i + 1));
                i--;
                size--;
            }
        }
        return size;
    }

    @org.junit.Test
    public void test() {
        int[] nums = {1, 1, 1, 1, 2, 3, 4};
        int i = removeElement(nums, 1);
        System.out.println(i);
        int[] nums1 = {1, 1, 1, 1, 2, 3, 4};
        int i1 = removeElement1(nums1, 1);
        System.out.println(i1);

    }

    public static int yiHuo(int[] nums, int n) {
        //异或：相同为0 不同为1 而且满足交换律和结合率
        // 先用变量temp异或nums数组所有元素 再去异或1~n  最终temp为nums数组缺的那个元素
        int temp = 0;
        //异或nums数组元素
        for (int num : nums) {
            temp ^= num;
        }
        //异或1~n
        for (int i = 1; i <= n; i++) {
            temp ^= i;
        }
        return temp;
    }


    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 9, 10};
        int i = yiHuo(nums, 10);
        System.out.println(i);
    }

}
