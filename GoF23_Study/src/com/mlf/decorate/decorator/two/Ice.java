package com.mlf.decorate.decorator.two;

import com.mlf.decorate.component.Beverage;

/**
 * @program: Java基础
 * @description: 配料：冰
 * @author: mlf
 * @date: 2022-06-01 18:29
 **/
public class Ice extends CondimentDecorator {
    public Ice(Beverage beverage) {
        super.beverage = beverage;
    }

    public double cost() {
        return beverage.cost() + 1;
    }
}
