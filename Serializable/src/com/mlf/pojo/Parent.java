package com.mlf.pojo;

import java.io.Serializable;

/**
 * @program: Java基础
 * @description: 父类继承的方法
 * @author: mlf
 * @date: 2022-04-21 20:45
 **/
public class Parent implements Serializable {

    int num;

    public Parent() {
    }

    public Parent(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
